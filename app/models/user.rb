class User < ActiveRecord::Base
  # include PublicActivity::Model
  # include SEED::Commentable
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  attr_accessor :login
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable, :confirmable, :lockable
  after_create :send_welcome_email

  acts_as_follower
  acts_as_followable
  acts_as_voter

  acts_as_liker
  acts_as_likeable
  acts_as_mentionable

  # validates_presence_of :username
  # validates_uniqueness_of :username
  validates_with EmailValidator

  validates :username, presence: true, length: {maximum: 255}, uniqueness: { case_sensitive: false }, format: { with: /\A[a-zA-Z0-9]*\z/, message: "may only contain letters and numbers." }

  has_attached_file :avatar, styles: { medium: "300x300#", thumb: "100x100#" }, default_url: "sys/unknown_profile.png"
  validates_attachment_content_type :avatar, content_type: /\Aimage\/.*\Z/
  has_attached_file :background, styles: { medium: "800x460#", thumb: "450x320#" }, default_url: "sys/cover_background.jpg"
  validates_attachment_content_type :background, content_type: /\Aimage\/.*\Z/

  has_many :friendships, dependent: :destroy
  has_many :inverse_friendships, class_name: "Friendship", foreign_key: "friend_id", dependent: :destroy

  # has_many :posts, as: :postable, dependent: :destroy
  has_many :posts
  has_many :media, through: :posts ,dependent: :destroy

  has_one :detailbook

  has_many :job_applications
  has_many :folders
  has_many :classifieds

  has_many :roles, dependent: :destroy
  has_many :events, dependent: :destroy

  has_many :polls, dependent: :destroy
  has_many :poll_votes, dependent: :destroy
  has_many :vote_options, through: :poll_votes

  has_many :teaching_availabilities, dependent: :destroy
  accepts_nested_attributes_for :teaching_availabilities, reject_if: :all_blank, allow_destroy: true

  has_many :conversations, :foreign_key => :sender_id

  has_many :todo_tasks, dependent: :destroy

  # after_save :load_into_soulmate
  # before_destroy :remove_from_soulmate

  validates_uniqueness_of :username

  # Detailbook Profile
  has_many :educations, dependent: :destroy
  has_many :job_experiences, dependent: :destroy
  has_and_belongs_to_many :skills
  accepts_nested_attributes_for :educations, reject_if: :all_blank, allow_destroy: true
  accepts_nested_attributes_for :job_experiences, reject_if: :all_blank, allow_destroy: true
  accepts_nested_attributes_for :skills, reject_if: :all_blank, allow_destroy: true

  has_many :application_forms, dependent: :destroy
  has_many :benchmark_reports, dependent: :destroy
  # has_many :comments, as: :commentable, dependent: :destroy


  # CV Builder 
  has_attached_file :cv_doc
  # validates_attachment_content_type :cv_doc, content_type: /\Aapplication\/.*\Z/
  validates_attachment_content_type :cv_doc, content_type: ['application/pdf'], :if => :pdf_attached?
  has_attached_file :cl_doc
  validates_attachment_content_type :cl_doc, content_type: /\Aapplication\/.*\Z/

  # validates_attachment :document, :content_type => { :content_type => %w(application/pdf application/msword application/vnd.openxmlformats-officedocument.wordprocessingml.document) }

  # Exchange Files
  has_many :ex_files
  has_many :categories, through: :ex_files

  # Mailboxer Stuff
  acts_as_messageable

  # YOLO
  has_many :yolos

  # Vita
  has_many :vita

  # Interests and Activities
  has_many :interest_activities, dependent: :destroy

  # has_many :memberships
  # has_many :groups, through: :memberships
  # has_many :group_ownerships
  # has_many :owned_groups, through: :group_owernships, class_name: "Group"
  # attr_accessible :name, :description, :owner_id

  groupify :group_member

  def mailboxer_name
    self.full_name
  end

  def mailboxer_email(object)
    self.email
  end

  def pdf_attached?
    self.cv_doc.file?
  end  

  def request_friendship(user_2)
	  self.friendships.create(friend: user_2)
  end

  def pending_friend_requests_from
    self.inverse_friendships.where(state: "pending")
  end

  def pending_friend_requests_to
    self.friendships.where(state: "pending")
  end
  
  def active_friends
    self.friendships.where(state: "active").map(&:friend) + self.inverse_friendships.where(state: "active").map(&:user)
  end  

  def friendship_status(user_2)
    friendship = Friendship.where(user_id: [self.id, user_2.id], friend_id: [self.id, user_2.id])
    unless friendship.any? 
      return "not_friends"
    else 
      if friendship.first.state == "active"
        return "friends"
      else
        if friendship.first.user == self
          return "pending"
        else
          return "requested"
        end
      end
    end
  end

  def friendship_relation(user_2)
    Friendship.where(user_id: [self.id, user_2.id], friend_id: [self.id, user_2.id]).first 
  end

  # for Roles

  def admin?
    Role.is_admin?(self)
  end

  def student?
    Role.is_student?(self)
  end

  def tutor?
    Role.is_tutor?(self)
  end

  def university?
    Role.is_university?(self)
  end

  def business?
    Role.is_business?(self)
  end

  def human?
    self.student? || self.tutor?
  end

  def building?
    self.university? || self.business?
  end

  # for Votes
  def voted_for?(poll)
    # vote_options.any? {|v| v.poll == poll }
    vote_options(true).any? {|v| v.poll == poll }
    # poll_votes.any? {|v| v.vote_option.poll == poll}
  end

  # For Login with Username or Email
  # def self.find_first_by_auth_conditions(warden_conditions)
  #   conditions = warden_conditions.dup
  #   if login = conditions.delete(:login)
  #     where(conditions).where(["username = :value OR lower(email) = lower(:value)", { :value => login }]).first
  #   else
  #     where(conditions).first
  #   end
  # end

  def self.find_for_database_authentication(warden_conditions)
    conditions = warden_conditions.dup
    if login = conditions.delete(:login)
      where(conditions.to_h).where(["lower(username) = :value OR lower(email) = :value", { :value => login.downcase }]).first
    else
      where(conditions.to_h).first
    end
  end

  def has_groups?
    return true
  end

  def has_events?
    return true
  end

  def has_exchanged_files?
    return true
  end

  # def follow(user)
  #   Follow.create(follower_type: "User", followable_type: "User", follower_id: self.id, followable_id: user.id)
  # end

  # def unfollow(user)
  #   Follow.destroy_all(follower_id: self.id, followable_id: user.id)
  # end

  def send_welcome_email
    UserMailer.welcome_email(self).deliver_later
  end

  private 

  # def load_into_soulmate
  #   loader = Soulmate::Loader.new("users")
  #   loader.add("term" => full_name, "id" => self.id, "data" => {
  #     "link" => Rails.application.routes.url_helpers.user_path(self.username)
  #   })
  # end

  # def remove_from_soulmate
  #   loader = Soulmate::Loader.new("users")
  #   loader.remove("id" => self.id)
  # end


end
