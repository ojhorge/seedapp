class Event < ActiveRecord::Base
	extend FriendlyId
	# include SEED::Commentable

	friendly_id :title, use: :slugged
	belongs_to :user

	after_save :load_into_soulmate
	before_destroy :remove_from_soulmate

	validates_uniqueness_of :title

	# has_many :posts, as: :postable, dependent: :destroy

	# acts_as_commentable

	private 

	def load_into_soulmate
		loader = Soulmate::Loader.new("Events")
		loader.add("term" => title, "id" => self.id, "data" => {
			"link" => Rails.application.routes.url_helpers.event_path(self.slug)
		})
	end

	def remove_from_soulmate
		loader = Soulmate::Loader.new("Events")
		loader.remove("id" => self.id)
	end
end
