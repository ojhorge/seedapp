class Category < ActiveRecord::Base
	extend FriendlyId
	
	friendly_id :title, use: :slugged
	has_many :videos
	has_many :ex_files
end
