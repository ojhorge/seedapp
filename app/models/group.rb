class Group < ActiveRecord::Base
  extend FriendlyId

  friendly_id :title, use: :slugged
  belongs_to :user

  validates_uniqueness_of :title

  groupify :group, members: [:users], default_members: :users

  # has_many :posts, as: :postable, dependent: :destroy

  has_attached_file :avatar, styles: { medium: "300x300#", thumb: "100x100#" }, default_url: "sys/unknown_profile.png"
  validates_attachment_content_type :avatar, content_type: /\Aimage\/.*\Z/
  has_attached_file :background, styles: { medium: "800x460#", thumb: "450x320#" }, default_url: "sys/cover_background.jpg"
  validates_attachment_content_type :background, content_type: /\Aimage\/.*\Z/
end
