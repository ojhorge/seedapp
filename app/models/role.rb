# class Role < ActiveRecord::Base
#   belongs_to :user
# end


# #NOTE add the user object you want to attach role to...

class Role < ActiveRecord::Base
  belongs_to :user
  validates :user, presence: true
  validates :name, uniqueness: {scope: :user_id}
  validates :name, inclusion: {in: %w(admin spotlight student tutor university business)}

  scope :admins, -> { where(name: "admin") }
  
  # Changed in 22/06/2015
  scope :students, -> { where(name: "student") }
  scope :tutors, -> { where(name: "tutor") }
  scope :universities, -> { where(name: "university") }
  scope :businesses, -> { where(name: "business") }

  def self.is_admin?(user)
    exists?(user_id: user.id, name: "admin")
  end

  def self.add_admin(user)
    find_or_create_by(user_id: user.id, name: "admin")
  end

  def self.add_spotlight(user)
    find_or_create_by(user_id: user.id, name: "spotlight")
  end

  # Changed in 22/05/2015
  def self.add_student(user)
    find_or_create_by(user_id: user.id, name: "student")
  end

  def self.add_tutor(user)
    find_or_create_by(user_id: user.id, name: "tutor")
  end

  def self.add_university(user)
    find_or_create_by(user_id: user.id, name: "university")
  end

  def self.add_business(user)
    find_or_create_by(user_id: user.id, name: "business")
  end

  # Changed in 14/07/2015
  def self.is_student?(user)
    exists?(user_id: user.id, name: "student")
  end

  def self.is_tutor?(user)
    exists?(user_id: user.id, name: "tutor")
  end

  def self.is_university?(user)
    exists?(user_id: user.id, name: "university")
  end

  def self.is_business?(user)
    exists?(user_id: user.id, name: "business")
  end

end
