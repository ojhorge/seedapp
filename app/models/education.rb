class Education < ActiveRecord::Base
  belongs_to :vitum
  belongs_to :user

  has_attached_file :media
	# validates_attachment_presence :media_attachment
	validates_attachment_content_type :media,
		:content_type => ['video/mp4'],
		:message => "Sorry, right now we only support MP4 video",
		:if => :is_type_of_video?
	validates_attachment_content_type :media,
		:content_type => ['image/png', 'image/jpeg', 'image/jpg', 'image/gif'],
		:message => "Different error message",
		:if => :is_type_of_image?

	protected

	def is_type_of_video?
		media.content_type =~ %r(video)
	end

	def is_type_of_image?
		media.content_type =~ %r(image)
	end
end
