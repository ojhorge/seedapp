class Skill < ActiveRecord::Base
  # has_and_belongs_to_many :detailbooks
  has_and_belongs_to_many :users
  belongs_to :vitum

  has_attached_file :skill_format
  # , :styles => {
		# 	:video => { :geometry => "1280x720", :format => 'mp4' },
		# 	:thumb => { :geometry => "800x600", :format => 'jpg', :time => 30 }
		# }, :processors => [:transcoder]
	# validates_attachment_presence :media_attachment
	validates_attachment_content_type :skill_format,
		:content_type => ['video/mp4'],
		:message => "Sorry, right now we only support MP4 video",
		:if => :is_type_of_video?
	validates_attachment_content_type :skill_format,
		:content_type => ['image/png', 'image/jpeg', 'image/jpg', 'image/gif'],
		:message => "Different error message",
		:if => :is_type_of_image?

	protected

	def is_type_of_video?
		skill_format.content_type =~ %r(video)
	end

	def is_type_of_image?
		skill_format.content_type =~ %r(image)
	end
end
