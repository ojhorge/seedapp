class Yolo < ActiveRecord::Base
	include SimpleHashtag::Hashtaggable
	# include SEED::Commentable

  	belongs_to :user

  	validates_presence_of :content
	hashtaggable_attribute :content

	acts_as_votable
	acts_as_likeable

	auto_html_for :content do
		html_escape
		image
		youtube(width: "100%", height: 250, autoplay: false)
		link target: "_blank", rel: "nofollow"
		simple_format
	end

  has_attached_file :media, styles: { medium: "300x300#", thumb: "100x100#" }, default_url: "sys/nophoto.jpg"
	# validates_attachment_presence :media_attachment
	validates_attachment_content_type :media,
		:content_type => ['video/mp4'],
		:message => "Sorry, right now we only support MP4 video",
		:if => :is_type_of_video?
	validates_attachment_content_type :media,
		:content_type => ['image/png', 'image/jpeg', 'image/jpg', 'image/gif'],
		:message => "Different error message",
		:if => :is_type_of_image?

	has_many :comments, as: :commentable, dependent: :destroy

	# acts_as_commentable

	protected

	def is_type_of_video?
		media.content_type =~ %r(video)
	end

	def is_type_of_image?
		media.content_type =~ %r(image)
	end

end
