class InterestActivity < ActiveRecord::Base
  belongs_to :user
  belongs_to :vitum

  has_attached_file :media
  # , :styles => {
		# 	:video => { :geometry => "1280x720", :format => 'mp4' },
		# 	:thumb => { :geometry => "800x600", :format => 'jpg', :time => 30 }
		# }, :processors => [:transcoder]
	# validates_attachment_presence :media
	validates_attachment_content_type :media,
		:content_type => ['video/mp4'],
		:message => "Sorry, right now we only support MP4 video",
		:if => :is_type_of_video?
	validates_attachment_content_type :media,
		:content_type => ['image/png', 'image/jpeg', 'image/jpg', 'image/gif'],
		:message => "Different error message",
		:if => :is_type_of_image?

	protected

	def is_type_of_video?
		media.content_type =~ %r(video)
	end

	def is_type_of_image?
		media.content_type =~ %r(image)
	end
end
