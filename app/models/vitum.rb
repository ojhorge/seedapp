class Vitum < ActiveRecord::Base
  # unloadable
  belongs_to :user
  
  has_one :exec_summary, dependent: :destroy
  accepts_nested_attributes_for :exec_summary, allow_destroy: true

  has_many :educations, dependent: :destroy
  accepts_nested_attributes_for :educations, reject_if: :all_blank, allow_destroy: true

  has_many :job_experiences, dependent: :destroy
  accepts_nested_attributes_for :job_experiences, reject_if: :all_blank, allow_destroy: true

  has_many :skills, dependent: :destroy
  accepts_nested_attributes_for :skills, reject_if: :all_blank, allow_destroy: true

  has_many :interest_activities, dependent: :destroy
  accepts_nested_attributes_for :interest_activities, reject_if: :all_blank, allow_destroy: true
end
