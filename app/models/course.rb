class Course < ActiveRecord::Base
	extend FriendlyId
	friendly_id :title, use: :slugged
	
	attr_accessor :title
	has_many :coursesyllabuses
	has_many :syllabuses, through: :coursesyllabuses

    accepts_nested_attributes_for :syllabuses, reject_if: :all_blank, allow_destroy: true
end
