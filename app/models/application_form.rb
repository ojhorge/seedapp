class ApplicationForm < ActiveRecord::Base
  belongs_to :job_application
  belongs_to :user

  has_attached_file :cv_doc
  # validates_attachment_content_type :cv_doc, content_type: /\Aapplication\/.*\Z/
  validates_attachment_content_type :cv_doc, content_type: ['application/pdf'], :if => :pdf_attached?
  has_attached_file :cl_doc
  validates_attachment_content_type :cl_doc, content_type: /\Aapplication\/.*\Z/

  # validates_attachment :document, :content_type => { :content_type => %w(application/pdf application/msword application/vnd.openxmlformats-officedocument.wordprocessingml.document) }


  def pdf_attached?
  	self.cv_doc.file?
  end

end
