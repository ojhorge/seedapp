class ExFile < ActiveRecord::Base
	belongs_to :user
	belongs_to :category

	has_attached_file :attachment
	# validates_attachment_presence :attachment
	validates_attachment_content_type :attachment,
		:content_type => ['video/mp4', 'video/flv', 'video/webm'],
		:message => "Sorry, right now we only support MP4, FLV and WEBm videos",
		:if => :is_type_of_video?
	validates_attachment_content_type :attachment,
		:content_type => ['image/png', 'image/jpeg', 'image/jpg', 'image/gif'],
		:message => "Image error message",
		:if => :is_type_of_image?
	validates_attachment_content_type :attachment,
		:content_type => ['application/txt', 'application/pdf', 'application/msword',
			'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
			'application/vnd.oasis.opendocument.text', 'application/x-vnd.oasis.opendocument.text',
			'application/rtf', 'application/x-rtf', 'application/doc', 
			'application/docx', 'application/x-soffice', 'application/octet-stream'],
		:message => "Document error message",
		:if => :is_type_of_document?
	validates_attachment_content_type :attachment,
		:content_type => ['text/richtext', 'text/rtf', 'text/plain'],
		:message => "Document error message",
		:if => :is_type_of_text?

	protected

	def is_type_of_video?
		attachment.content_type =~ %r(video)
	end

	def is_type_of_image?
		attachment.content_type =~ %r(image)
	end

	def is_type_of_document?
		attachment.content_type =~ %r(application)
	end

	def is_type_of_text?
		attachment.content_type =~ %r(text)
	end

end
