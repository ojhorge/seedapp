class Detailbook < ActiveRecord::Base
  belongs_to :user
  has_attached_file :avatar, styles: { medium: "300x300>", thumb: "100x100>" }, default_url: "sys/unknown_profile.png"
  validates_attachment_content_type :avatar, content_type: /\Aimage\/.*\Z/
  has_attached_file :background, styles: { medium: "800x460>", thumb: "450x320>" }, default_url: "sys/unknown_background.jpg"
  validates_attachment_content_type :background, content_type: /\Aimage\/.*\Z/

  has_many :educations, dependent: :destroy
  has_many :job_experiences, dependent: :destroy
  has_and_belongs_to_many :skills
  accepts_nested_attributes_for :educations, reject_if: :all_blank, allow_destroy: true
  accepts_nested_attributes_for :job_experiences, reject_if: :all_blank, allow_destroy: true
  accepts_nested_attributes_for :skills, reject_if: :all_blank, allow_destroy: true

  validates :user, presence: true, uniqueness:  { case_sensitive: false}

end
