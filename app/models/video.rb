class Video < ActiveRecord::Base
	belongs_to :lecture
	belongs_to :category

	has_attached_file :content, :styles => {
			:medium => { :geometry => "1280x720#", :format => 'mp4' },
			:thumb => { :geometry => "800x600#", :format => 'jpg', :time => 30 }
		}, :processors => [:transcoder]

	validates_attachment_presence :content
	validates_attachment_content_type :content,
		:content_type => ['video/mp4'],
		:message => "Sorry, right now we only support MP4 video",
		:if => :is_type_of_video?
	validates_attachment_content_type :content,
		:content_type => ['image/png', 'image/jpeg', 'image/jpg', 'image/gif'],
		:message => "Different error message",
		:if => :is_type_of_image?

	has_many :comments, as: :commentable, dependent: :destroy
	# acts_as_commentable

	protected

	def is_type_of_video?
		content.content_type =~ %r(video)
	end

	def is_type_of_image?
		content.content_type =~ %r(image)
	end


end
