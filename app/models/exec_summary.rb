class ExecSummary < ActiveRecord::Base
	# unloadable
  belongs_to :vitum

	has_attached_file :media_attachment
	# , :styles => {
 #        	:mp4 => {
	# 		      :format => 'mp4',
	# 		      :geometry => "1200x675#",
	# 		      :convert_options => {
	# 		        :input => {},
	# 		        :output => {
	# 		          :vcodec => 'libx264',
	# 		          :movflags => '+faststart',
	# 		          :strict => :experimental
	# 		        }
	# 		      }
	# 		    },
	# 		    :webm => {
	# 		      :format => 'webm',
	# 		      :geometry => "1200x675#",
	# 		      :convert_options => {
	# 		        :input => {},
	# 		        :output => {
	# 		          :vcodec => 'libvpx',
	# 		          :acodec => 'libvorbis',
	# 		          'cpu-used' => -10,
	# 		          :deadline => :realtime,
	# 		          :strict => :experimental
	# 		        }
	# 		      }
	# 		    },

	# 		    # I couldn't get the preview to work with
	# 		    # the method outlined in the docs,
	# 		    # so I just passed the options
	# 		    # to avconv specifically.

	# 		    :preview => {
	# 		      :format => :jpg,
	# 		      :geometry => "1200x675#",
	# 		      :convert_options => {
	# 		        :output => {
	# 		          :vframes => 1,
	# 		          :s => "1200x675",
	# 		          :ss => '00:00:20'
	# 		        }
	# 		      }
	# 		    },
 #        	:thumbnail => {
	# 		      :format => :jpg,
	# 		      :geometry => "300x169#",
	# 		      :convert_options => {
	# 		        :output => {
	# 		          :vframes => 1,
	# 		          :s => '300x169',
	# 		          :ss => '00:00:20'
	# 		        }
	# 		      }
	# 	      },
 #        }, :processors => [:transcoder]

	# , :styles => {
	# 		:video => { :geometry => "1280x720", :format => 'mp4' },
	# 		:thumb => { :geometry => "800x600", :format => 'jpg', :time => 30 }
	# 	}, :processors => [:transcoder]
	# validates_attachment_presence :media_attachment
	validates_attachment_content_type :media_attachment,
		:content_type => ['video/mp4'],
		:message => "Sorry, right now we only support MP4 video",
		:if => :is_type_of_video?
	validates_attachment_content_type :media_attachment,
		:content_type => ['image/png', 'image/jpeg', 'image/jpg', 'image/gif'],
		:message => "Different error message",
		:if => :is_type_of_image?

	protected

	def is_type_of_video?
		# media_attachment.content_type =~ %r(video)
		media_attachment.content_type =~ %r(video)
    # ['video', 'image/pjpeg', 'image/gif', 'image/png', 'image/x-png', 'image/jpg'].include?(asset.content_type)
	end

	def is_type_of_image?
		media_attachment.content_type =~ %r(image)
    # ['image/jpeg', 'image/pjpeg', 'image/gif', 'image/png', 'image/x-png', 'image/jpg'].include?(asset.content_type)
		# media_attachment.content_type =~ %r(image)
	end

end


