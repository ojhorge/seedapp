class JobApplication < ActiveRecord::Base
  extend FriendlyId

  friendly_id :job_title, use: :slugged

  belongs_to :user

  has_attached_file :picture, styles: { medium: "800x460#", thumb: "350x350#" }, default_url: "sys/nophoto.jpg"
  validates_attachment_content_type :picture, content_type: /\Aimage\/.*\Z/

  after_save :load_into_soulmate
  before_destroy :remove_from_soulmate

  validates_uniqueness_of :job_title

  has_many :application_forms, dependent: :destroy

  has_one :sale, as: :sellable, dependent: :destroy

  private

	def load_into_soulmate
		loader = Soulmate::Loader.new("Opportunities")
		loader.add("term" => job_title, "id" => self.id, "data" => {
			"link" => Rails.application.routes.url_helpers.job_application_path(self.guid)
		})
	end

	def remove_from_soulmate
		loader = Soulmate::Loader.new("Opportunities")
		loader.remove("id" => self.id)
	end

end
