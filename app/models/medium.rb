class Medium < ActiveRecord::Base
  belongs_to :user
  belongs_to :post

	has_attached_file :media_attachment
	# validates_attachment_presence :media_attachment
	validates_attachment_content_type :media_attachment,
		:content_type => ['video/mp4'],
		:message => "Sorry, right now we only support MP4 video",
		:if => :is_type_of_video?
	validates_attachment_content_type :media_attachment,
		:content_type => ['image/png', 'image/jpeg', 'image/jpg', 'image/gif'],
		:message => "Different error message",
		:if => :is_type_of_image?

	protected

	def is_type_of_video?
		media_attachment.content_type =~ %r(video)
	end

	def is_type_of_image?
		media_attachment.content_type =~ %r(image)
	end


end
