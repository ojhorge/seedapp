class Classified < ActiveRecord::Base
  extend FriendlyId

  friendly_id :title, use: :slugged

  belongs_to :user

  validates :title, :description, :price, presence: true

  has_attached_file :pic, styles: { medium: "800x460#", thumb: "450x320#" }, default_url: "sys/nophoto.jpg"
  validates_attachment_content_type :pic, content_type: /\Aimage\/.*\Z/

  after_save :load_into_soulmate
  before_destroy :remove_from_soulmate

  validates_uniqueness_of :title
  has_one :sale, as: :sellable, dependent: :destroy

  # serialize :classified_params, Hash
  def paypal_url(return_path)
  	values = {
		business: "admin@seed.ac",
        cmd: "_xclick",
        upload: 1,
        return: "#{Rails.application.secrets.app_host}#{return_path}",
        invoice: id,
        amount: 1.49,
        item_name: "Advertising Placement",
        item_number: 3,
        quantity: '1',
        currency_code: 'GBP',
        notify_url: "#{Rails.application.secrets.app_host}/hook"
  	}
  	"#{Rails.application.secrets.paypal_host}/cgi-bin/webscr?" + values.to_query
  end

  private

  def load_into_soulmate
    loader = Soulmate::Loader.new("Classifieds")
    loader.add("term" => title, "id" => self.id, "data" => {
      "link" => Rails.application.routes.url_helpers.classified_path(self.slug)
      })
  end

  def remove_from_soulmate
    loader = Soulmate::Loader.new("Classifieds")
      loader.remove("id" => self.id)
  end

end
