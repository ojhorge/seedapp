class Syllabus < ActiveRecord::Base
	extend FriendlyId
	friendly_id :title, use: :slugged
	
	attr_accessor :title
	has_many :coursesyllabuses
	has_many :courses, through: :coursesyllabuses
	has_many :lectures, dependent: :destroy
	accepts_nested_attributes_for :lectures, reject_if: :all_blank, allow_destroy: true
end
