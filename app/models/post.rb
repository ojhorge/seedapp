class Post < ActiveRecord::Base
	include PublicActivity::Model
	include SimpleHashtag::Hashtaggable
	# include SEED::Commentable

	belongs_to :user
	validates_presence_of :user_id
	# validates_presence_of :content

	# belongs_to :postable, polymorphic: true

	hashtaggable_attribute :content

	auto_html_for :content do
		html_escape
		image
		youtube(width: "100%", height: 250, autoplay: false)
		link target: "_blank", rel: "nofollow"
		simple_format
	end

	has_many :media, dependent: :destroy
	accepts_nested_attributes_for :media, reject_if: :all_blank, allow_destroy: true
	
	has_many :comments, as: :commentable, dependent: :destroy
	belongs_to :post_type
	
	acts_as_votable
	acts_as_likeable

	# acts_as_commentable
end
