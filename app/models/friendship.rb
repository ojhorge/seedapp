class Friendship < ActiveRecord::Base
	include PublicActivity::Model
	# include SEED::Commentable
	# tracked recipient: :friend 

  	belongs_to :user
 	belongs_to :friend, class_name: "User"

 	has_many :comments, as: :commentable

  	def accept_friendship
		self.update_attributes(state: "active", friended_at: Time.now)
	end

	def deny_friendship
		self.destroy
	end

	def cancel_friendship
		self.destroy
	end

	acts_as_votable
	acts_as_likeable

end
