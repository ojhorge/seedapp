module UsersHelper
	def action_buttons(user)
		if current_user.human? && user.human?
			case current_user.friendship_status(user) when "friends"
				link_to "<i class='fa fa-user-times'></i> Cancel Friendship".html_safe, friendship_path(current_user.friendship_relation(user)), method: :delete, class: "btn btn-danger-outline pull-right"
			when "pending"
				link_to "<i class='fa fa-user-times'></i> Cancel Request".html_safe, friendship_path(current_user.friendship_relation(user)), method: :delete, class: "btn btn-danger-outline pull-right" 
			when "requested"
				#"Accept or Deny"
				link_to "#{ }<i class='fa fa-user-plus'></i>Accept".html_safe, accept_friendship_path(current_user.friendship_relation(user)), method: :put, class: "btn btn-success-outline pull-right"		
			when "not_friends"
				link_to "<i class='fa fa-user-plus'></i> Add as Friend".html_safe, friendships_path(user_id: user.id), method: :post, class: "btn btn-primary-outline pull-right"
			end
		# elsif current_user.building? | user.building?
		# 	case current_user.friendship_status(user) when "friends"
		# 		link_to "<i class='fa fa-user-times'></i> Unfollow".html_safe, friendship_path(current_user.friendship_relation(user)), method: :delete, class: "btn btn-danger-outline"
		# 	when "pending"
		# 		link_to "<i class='fa fa-user-times'></i> Unfollow".html_safe, friendship_path(current_user.friendship_relation(user)), method: :delete, class: "btn btn-danger-outline"
		# 	when "requested"
		# 		#"Accept or Deny"
		# 		link_to "<i class='fa fa-user-plus'></i> Follow".html_safe, accept_friendship_path(current_user.friendship_relation(user)), method: :put, class: "btn btn-primary-outline"			
		# 	when "not_friends"
		# 		link_to "<i class='fa fa-user-plus'></i> Follow".html_safe, friendships_path(user_id: user.id), method: :post, class: "btn btn-primary-outline"
		# 	end
		# end
		elsif current_user.building? | user.building?
			if current_user.follows?(user)
				link_to "<i class='fa fa-user-times'></i> Unfollow".html_safe, unfollow_follow_path(user.username), method: :get, class: "btn btn-danger-outline pull-right"
			else	
				link_to "<i class='fa fa-user-plus'></i> Follow".html_safe, follow_follow_path(user.username), method: :get, class: "btn btn-primary-outline pull-right"
			end
		end
	end
end
