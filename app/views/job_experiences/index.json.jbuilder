json.array!(@job_experiences) do |job_experience|
  json.extract! job_experience, :id, :guid, :title, :company_name, :city, :country, :date_start, :date_finish, :current_working, :description, :done, :detailbook_id
  json.url job_experience_url(job_experience, format: :json)
end
