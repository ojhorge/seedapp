json.array!(@lectures) do |lecture|
  json.extract! lecture, :id, :title, :description, :syllabus_id
  json.url lecture_url(lecture, format: :json)
end
