json.array!(@online_learnings) do |online_learning|
  json.extract! online_learning, :id
  json.url online_learning_url(online_learning, format: :json)
end
