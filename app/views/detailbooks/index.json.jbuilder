json.array!(@detailbooks) do |detailbook|
  json.extract! detailbook, :id, :user_id, :name, :last_name, :dob, :gender, :city, :country, :benchmark
  json.url detailbook_url(detailbook, format: :json)
end
