json.array!(@todo_tasks) do |todo_task|
  json.extract! todo_task, :id, :title, :completed, :row_order, :priority, :comment, :archived
  json.url todo_task_url(todo_task, format: :json)
end
