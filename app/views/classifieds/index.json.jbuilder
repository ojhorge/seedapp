json.array!(@classifieds) do |classified|
  json.extract! classified, :id, :title, :user_id, :description, :price, :availability, :notification_status, :status, :purchased_at
  json.url classified_url(classified, format: :json)
end
