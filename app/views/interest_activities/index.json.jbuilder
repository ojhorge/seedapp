json.array!(@interest_activities) do |interest_activity|
  json.extract! interest_activity, :id, :name, :description, :user_id
  json.url interest_activity_url(interest_activity, format: :json)
end
