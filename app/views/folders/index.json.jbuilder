json.array!(@folders) do |folder|
  json.extract! folder, :id, :guid, :user_id, :name, :description, :sharable_to, :category
  json.url folder_url(folder, format: :json)
end
