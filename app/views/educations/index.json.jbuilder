json.array!(@educations) do |education|
  json.extract! education, :id, :guid, :school_institution, :date_start, :date_finish, :city, :country, :degree, :field_study, :grade, :activities_societies, :description, :done, :detailbook_id
  json.url education_url(education, format: :json)
end
