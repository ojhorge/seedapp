json.array!(@job_applications) do |job_application|
  json.extract! job_application, :id, :guid, :company_name, :industry, :job_title, :experience, :job_function, :employment_type, :job_description, :city, :country, :public, :user_id
  json.url job_application_url(job_application, format: :json)
end
