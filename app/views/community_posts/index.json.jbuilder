json.array!(@community_posts) do |community_post|
  json.extract! community_post, :id, :name, :content
  json.url community_post_url(community_post, format: :json)
end
