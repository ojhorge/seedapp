json.array!(@posts) do |post|
  json.extract! post, :id, :user_id, :content, :content_html
  json.url post_url(post, format: :json)
end
