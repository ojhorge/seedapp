json.array!(@syllabuses) do |syllabus|
  json.extract! syllabus, :id, :title, :description
  json.url syllabus_url(syllabus, format: :json)
end
