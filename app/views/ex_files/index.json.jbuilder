json.array!(@ex_files) do |ex_file|
  json.extract! ex_file, :id, :user_id, :name, :description, :category_id, :guid
  json.url ex_file_url(ex_file, format: :json)
end
