json.array!(@benchmark_reports) do |benchmark_report|
  json.extract! benchmark_report, :id, :user_id, :points, :month, :year
  json.url benchmark_report_url(benchmark_report, format: :json)
end
