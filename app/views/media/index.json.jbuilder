json.array!(@media) do |medium|
  json.extract! medium, :id, :user_id, :description, :guid, :post_id
  json.url medium_url(medium, format: :json)
end
