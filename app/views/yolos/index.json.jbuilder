json.array!(@yolos) do |yolo|
  json.extract! yolo, :id, :user_id, :content, :content_html
  json.url yolo_url(yolo, format: :json)
end
