json.array!(@videos) do |video|
  json.extract! video, :id, :title, :description, :lecture_id
  json.url video_url(video, format: :json)
end
