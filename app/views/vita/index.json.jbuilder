json.array!(@vita) do |vitum|
  json.extract! vitum, :id, :user_id, :title, :guid
  json.url vitum_url(vitum, format: :json)
end
