class ApplicationMailer < ActionMailer::Base
  default from: "donotreply@seed.ac"
  layout 'mailer'
end
