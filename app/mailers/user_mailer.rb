class UserMailer < ApplicationMailer
	default from: "donotreply@seed.ac"

	def welcome_email(user)
	  	@user = user
	  	mail(to: @user.email, subject: 'Welcome to SEED.ac')
	end
end
