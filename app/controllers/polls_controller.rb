class PollsController < ApplicationController
  before_action :set_poll, only: [:show, :edit, :update, :destroy]

  # GET /polls
  # GET /polls.json
  def index
    @polls = Poll.all
  end

  # GET /polls/1
  # GET /polls/1.json
  def show
    @poll = Poll.includes(:vote_options).find_by_id(params[:id])
  end

  # GET /polls/new
  def new
    @poll = current_user.polls.new
  end

  # GET /polls/1/edit
  def edit
    @poll = Poll.find_by_id(params[:id])
  end

  # POST /polls
  # POST /polls.json
  def create
    @poll = current_user.polls.new(poll_params)
    @poll.guid = SecureRandom.hex[1..11]
    respond_to do |format|
      if @poll.save
        format.html { redirect_to @poll, notice: 'Poll was successfully created.' }
        format.json { render :show, status: :created, location: @poll }
      else
        format.html { render :new }
        format.json { render json: @poll.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /polls/1
  # PATCH/PUT /polls/1.json
  def update
    # respond_to do |format|
    #   if @poll.update(poll_params)
    #     format.html { redirect_to @poll, notice: 'Poll was successfully updated.' }
    #     format.json { render :show, status: :ok, location: @poll }
    #   else
    #     format.html { render :edit }
    #     format.json { render json: @poll.errors, status: :unprocessable_entity }
    #   end
    # end
    @poll = Poll.find_by_id(params[:id])
    if @poll.update_attributes(poll_params)
      flash[:success] = 'Poll was updated!'
      redirect_to polls_path
    else
      render 'edit'
    end
  end

  # DELETE /polls/1
  # DELETE /polls/1.json
  def destroy
    # @poll.destroy
    # respond_to do |format|
    #   format.html { redirect_to polls_url, notice: 'Poll was successfully destroyed.' }
    #   format.json { head :no_content }
    # end
    @poll = Poll.find_by_id(params[:id])
    if @poll.destroy
      flash[:success] = 'Poll was destroyed!'
    else
      flash[:warning] = 'Error destroying poll...'
    end
    redirect_to polls_path
  end

  # def empty?
  #   if Poll.none
  # end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_poll
      @poll = Poll.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def poll_params
      params.require(:poll).permit(
        :topic, 
        :user_id,
        :guid,
        vote_options_attributes: [:id, :title, :_destroy])
    end
end
