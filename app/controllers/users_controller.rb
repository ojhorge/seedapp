class UsersController < ApplicationController
  before_action :authenticate_user!, only: [:index, :job_applications, :show]
  before_action :set_user, only: [:show, :job_applications]
  before_action :get_counts, only: [:index]

  def index
    case params[:people]
    when "friends"
      @members = current_user.active_friends
      @businesses = []
      User.where(role: "b").each do |business|
        @businesses.push(business) if current_user.follows?(business)
      end
    when "requests"
      @members = current_user.pending_friend_requests_from.map(&:user)
    when "pending"
      @members = current_user.pending_friend_requests_to.map(&:friend)
    when "students"
      @members = User.where(role: "s").where.not(id: current_user.id)
    when "tutors"
      @members = User.where(role: "t").where.not(id: current_user.id)
    when "universities"
      @members = User.where(role: "u").where.not(id: current_user.id)
    when "businesses"
      @members = User.where(role: "b").where.not(id: current_user.id)
    else
      @members = User.where.not(role: "b").where.not(id: current_user.id)
      @businesses = User.where(role: "b").where.not(id: current_user.id)
    end
  end

  def show
    @friends = Friendship.where(user_id: current_user, friend_id: @user, state: "active")
    @friends += Friendship.where(user_id: @user, friend_id: current_user, state: "active")
    @post = Post.new
    @posts = @user.posts.order('created_at DESC')
    @activities = PublicActivity::Activity.where(owner_id: @user.id).order('created_at DESC')
    @quote = get_quote
    # @comment = current_user.comments.new
  end

  private

  def get_counts
    @friend_count = current_user.active_friends.size
    @pending_count = current_user.pending_friend_requests_to.map(&:friend).size
  end

  def set_user
  	@user = User.find_by(username: params[:id])
  end

  def get_quote
    quotes = [
        "Education is not preparation for life; education <i>is</i> life itself.<br> – John Dewey",
        "We have all much to learn: Let us teach one another as <i>kindly</i> as we can.<br> – Reade",
        "Few of us know what we are capable of doing . . .; <i>We have never pushed ourselves</i> hard enough to find out.<br> – Alfred A. Montapert",
        "Success seems to be largely a matter of <i>hanging on</i> after others have let go.<br> – William Feather",
        "If you want learning, you must <i>work</i> for it.<br> – Josiah Gilbert Holland",
        "Achievements are the accomplishment of <i>persistent</i> individuals.<br> – James Jones",
        "Failing to prepare is preparing to fail.<br> – Anonymous"
      ] 
    index = Random.rand(quotes.size - 1)

    return quotes[index]
  end

  def follow
    user = User.find(params[:id])
    current_user.follow!(user) # => This assumes you have a variable current_user who is authenticated
    respond_to do |format|
      format.html {redirect_to users_path, notice: "User Followed"}
    end
  end

  def unfollow
    user = User.find(params[:id])
    current_user.unfollow!(user) # => This assumes you have a variable current_user who is authenticated
    respond_to do |format|
      format.html {redirect_to users_path, notice: "User Unfollowed"}
    end
  end

end
