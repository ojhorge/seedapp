class JobApplicationsController < ApplicationController
  before_action :set_job_application, only: [:show, :edit, :update, :destroy]
  before_action :set_user, only: [:job_applications]
  # GET /job_applications
  # GET /job_applications.json
  def index
    # if current_user.student?
      @job_applications = JobApplication.all    
    # else
    #   @job_applications = current_user.job_applications.all
    # end
  end

  # GET /job_applications/1
  # GET /job_applications/1.json
  def show
    if current_user.student?
      @application_form = @job_application.application_forms.new(params[:user] = { user_id: current_user.id})
    end
  end

  # GET /job_applications/new
  def new
    @job_application = current_user.job_applications.new
  end

  # GET /job_applications/1/edit
  def edit
  end

  # POST /job_applications
  # POST /job_applications.json
  def create
    @job_application = current_user.job_applications.new(job_application_params)
    @job_application.guid = SecureRandom.hex[1..11]
    respond_to do |format|
      if @job_application.save
        format.html { redirect_to @job_application, notice: 'Job application was successfully created.' }
        format.json { render :show, status: :created, location: @job_application }
      else
        format.html { render :new }
        format.json { render json: @job_application.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /job_applications/1
  # PATCH/PUT /job_applications/1.json
  def update
    respond_to do |format|
      if @job_application.update(job_application_params)
        format.html { redirect_to @job_application, notice: 'Job application was successfully updated.' }
        format.json { render :show, status: :ok, location: @job_application }
      else
        format.html { render :edit }
        format.json { render json: @job_application.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /job_applications/1
  # DELETE /job_applications/1.json
  def destroy
    @job_application.destroy
    respond_to do |format|
      format.html { redirect_to job_applications_url, notice: 'Job application was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def job_applications
    @job_applications = JobApplication.all.where(user_id: current_user.id).order('created_at DESC')
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_job_application
      @job_application = JobApplication.friendly.find(params[:id])
    end

    def set_user
      @user = User.find_by_username(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def job_application_params
      params.require(:job_application).permit(
        :guid, 
        :company_name, 
        :industry, 
        :job_title, 
        :experience,
        :job_function, 
        :employment_type, 
        :job_description, 
        :deadline,
        :cat,
        :city, 
        :country, 
        :public, 
        :user_id,
        :picture,
        :slug,
        :payment_price
      )
    end
end
