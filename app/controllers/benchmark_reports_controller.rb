class BenchmarkReportsController < ApplicationController
  before_action :authenticate_user!
  before_action :set_user
  before_action :set_benchmark_report, only: [:show, :edit, :update, :destroy]

  # GET /benchmark_reports
  # GET /benchmark_reports.json
  def index
    @benchmark_reports = @user.benchmark_reports.all
    # gon.rabl "app/views/benchmark_reports/index.json.rabl", as: "benchmark"

    points = []
    period = []
    avg = []
    max = []

    @benchmark_reports.each do |benchmark|
      points.push(benchmark.points)
      period.push(benchmark.month.to_s + '/' + benchmark.year.to_s)
      avg.push(benchmark.average)
      max.push(benchmark.maximum)
    end
    
    @data = { labels: period, 
              datasets: [
                {
                  label: "Highest Benchmark",
                  fillColor: "rgba(189, 80, 43, 0.0)",
                  strokeColor: "rgba(189, 80, 43, 1)",
                  pointColor: "rgba(189, 80, 43, 1)",
                  pointStrokeColor: "#fff",
                  pointHighlightFill: "#fff",
                  pointHighlightStroke: "rgba(189, 80, 43, 1)",
                  data: max
                },
                {
                  label: "Your Benchmark",
                  fillColor: "rgba(80, 179, 110, 0.0)",
                  strokeColor: "rgba(80, 179, 110, 1)",
                  pointColor: "rgba(80, 179, 110, 1)",
                  pointStrokeColor: "#fff",
                  pointHighlightFill: "#fff",
                  pointHighlightStroke: "rgba(80, 179, 110, 1)",
                  data: points
                },
                {
                  label: "Average Benchmark",
                  fillColor: "rgba(60, 155, 207, 0.0)",
                  strokeColor: "rgba(60, 155, 207, 1)",
                  pointColor: "rgba(60, 155, 207, 1)",
                  pointStrokeColor: "#fff",
                  pointHighlightFill: "#fff",
                  pointHighlightStroke: "rgba(60, 155, 207, 1)",
                  data: avg
                }
              ]
            }

      @options = {
        generateLegend: true,
        width: 560,

        # Boolean - Whether grid lines are shown across the chart
        scaleShowGridLines: true,

        # String - Colour of the grid lines
        scaleGridLineColor: "rgba(0,0,0,.05)",

        # Number - Width of the grid lines
        scaleGridLineWidth: 1,

        # Boolean - Whether to show horizontal lines (except X axis)
        scaleShowHorizontalLines: true,

        # Boolean - Whether to show vertical lines (except Y axis)
        scaleShowVerticalLines: true,

        # Boolean - Whether the line is curved between points
        bezierCurve: true,

        # Number - Tension of the bezier curve between points
        bezierCurveTension: 0.4,

        # Boolean - Whether to show a dot for each point
        pointDot: true,

        # Number - Radius of each point dot in pixels
        pointDotRadius: 4,

        # Number - Pixel width of point dot stroke
        pointDotStrokeWidth: 1,

        # Number - amount extra to add to the radius to cater for hit detection outside the drawn point
        pointHitDetectionRadius: 20,

        # Boolean - Whether to show a stroke for datasets
        datasetStroke: true,

        # Number - Pixel width of dataset stroke
        datasetStrokeWidth: 2,

        # Boolean - Whether to fill the dataset with a colour
        datasetFill: true,

        # String - A legend template
        legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"background-color:<%=datasets[i].strokeColor%>\">&nbsp;&nbsp;&nbsp;&nbsp;</span><%if(datasets[i].label){%>&nbsp;<%=datasets[i].label%><%}%></li><%}%></ul>",
        
        tooltipTemplate: "<%if (label){%><%=label%>: <%}%><%= value %>"
      }
  end

  # GET /benchmark_reports/1
  # GET /benchmark_reports/1.json
  def show
  end

  # GET /benchmark_reports/new
  def new
    @benchmark_report = @user.benchmark_reports.new
  end

  # GET /benchmark_reports/1/edit
  def edit
  end

  # POST /benchmark_reports
  # POST /benchmark_reports.json
  def create
    @benchmark_report = @user.benchmark_reports.new(benchmark_report_params)

    respond_to do |format|
      if @benchmark_report.save
        format.html { redirect_to @benchmark_report, notice: 'Benchmark report was successfully created.' }
        format.json { render :show, status: :created, location: @benchmark_report }
      else
        format.html { render :new }
        format.json { render json: @benchmark_report.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /benchmark_reports/1
  # PATCH/PUT /benchmark_reports/1.json
  def update
    respond_to do |format|
      if @benchmark_report.update(benchmark_report_params)
        format.html { redirect_to @benchmark_report, notice: 'Benchmark report was successfully updated.' }
        format.json { render :show, status: :ok, location: @benchmark_report }
      else
        format.html { render :edit }
        format.json { render json: @benchmark_report.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /benchmark_reports/1
  # DELETE /benchmark_reports/1.json
  def destroy
    @benchmark_report.destroy
    respond_to do |format|
      format.html { redirect_to benchmark_reports_url, notice: 'Benchmark report was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_benchmark_report
      @benchmark_report = BenchmarkReport.find(params[:id])
    end

    # Use to call the user
    def set_user
      @user = User.find_by(username: params[:user_id])
    end


    # Never trust parameters from the scary internet, only allow the white list through.
    def benchmark_report_params
      params.require(:benchmark_report).permit(:user_id, :points, :month, :year, :average, :maximum)
    end
end

# class cona
#   before_action :authenticate_user!, only: [:index, :job_applications]
#   before_action :set_user, only: [:show, :job_applications]
#   before_action :get_counts, only: [:index]

#   def index
#     case params[:people]
#     when "friends"
#       @users = current_user.active_friends
#     when "requests"
#       @users = current_user.pending_friend_requests_from.map(&:user)
#     when "pending"
#       @users = current_user.pending_friend_requests_to.map(&:friend)
#     when "students"
#       @users = User.where(role: "s").where.not(id: current_user.id)
#     when "tutors"
#       @users = User.where(role: "t").where.not(id: current_user.id)
#     when "universities"
#       @users = User.where(role: "u").where.not(id: current_user.id)
#     when "businesses"
#       @users = User.where(role: "b").where.not(id: current_user.id)
#     else
#       @users = User.where.not(id: current_user.id)
#     end
#   end

#   def show
#     @post = Post.new
#     @posts = @user.posts.order('created_at DESC')
#     @activities = PublicActivity::Activity.where(owner_id: @user.id).order('created_at DESC')
#     @quote = get_quote
#   end

#   private

#   def get_counts
#     @friend_count = current_user.active_friends.size
#     @pending_count = current_user.pending_friend_requests_to.map(&:friend).size
#   end

#   def set_user
#     @user = User.find_by(username: params[:id])
#   end

#   def get_quote
#     quotes = [
#         "Education is not preparation for life; education <i>is</i> life itself.<br> – John Dewey",
#         "We have all much to learn: Let us teach one another as <i>kindly</i> as we can.<br> – Reade",
#         "Few of us know what we are capable of doing . . .; <i>We have never pushed ourselves</i> hard enough to find out.<br> – Alfred A. Montapert",
#         "Success seems to be largely a matter of <i>hanging on</i> after others have let go.<br> – William Feather",
#         "If you want learning, you must <i>work</i> for it.<br> – Josiah Gilbert Holland",
#         "Achievements are the accomplishment of <i>persistent</i> individuals.<br> – James Jones",
#         "Failing to prepare is preparing to fail.<br> – Anonymous"
#       ] 
#     index = Random.rand(quotes.size - 1)

#     return quotes[index]
#   end
# end
