class DetailbooksController < ApplicationController
  before_action :set_detailbook, only: [:show, :edit, :update, :destroy]

  # GET /detailbooks
  # GET /detailbooks.json
  def index
    if current_user.detailbook
      redirect_to detailbook_path(current_user.detailbook.username)
    end 
  end

  # GET /detailbooks/1
  # GET /detailbooks/1.json
  def show
    @activities = PublicActivity::Activity.where(owner_id: @detailbook.user.id).order('created_at DESC')
    @quote = get_quote
  end

  # GET /detailbooks/new
  def new
    if !current_user.detailbook
      @detailbook = Detailbook.new
      # respond_with(@detailbook)
    else
      redirect_to detailbook_path(current_user.detailbook), notice: "You already created a Detailbook profile!"
    end
  end

  # GET /detailbooks/1/edit
  def edit
  end

  # POST /detailbooks
  # POST /detailbooks.json
  def create
    @detailbook = Detailbook.new(detailbook_params)

    respond_to do |format|
      if @detailbook.save
        format.html { redirect_to detailbook_path(@detailbook.username), notice: 'Detailbook was successfully created.' }
        format.json { render :show, status: :created, location: detailbook_path(@detailbook.username) }
      else
        format.html { render :new }
        format.json { render json: @detailbook.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /detailbooks/1
  # PATCH/PUT /detailbooks/1.json
  def update
    respond_to do |format|
      if @detailbook.update(detailbook_params)
        format.html { redirect_to detailbook_path(@detailbook.username), notice: 'Detailbook was successfully updated.' }
        format.json { render :show, status: :ok, location: detailbook_path(@detailbook.username) }
      else
        format.html { render :edit }
        format.json { render json: @detailbook.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /detailbooks/1
  # DELETE /detailbooks/1.json
  def destroy
    @detailbook.destroy
    respond_to do |format|
      format.html { redirect_to detailbooks_url, notice: 'Detailbook was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_detailbook
      @detailbook = Detailbook.find_by_username(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def detailbook_params
      params.require(:detailbook).permit(
        :user_id,
        :username, 
        :name, 
        :last_name, 
        :dob, 
        :gender, 
        :city, 
        :country, 
        :benchmark,
        :avatar,
        :background,
        educations_attributes: [:id, :_destroy, :done, :guid, :school_instituition, :date_start, :date_finish, :city, :country, :degree, :field_study, :grade, :activities_societies, :description],
        job_experiences_attributes: [:id, :_destroy, :done , :guid, :title, :company_name, :city, :country, :date_start, :date_finish, :current_working, :description],
        skills_attributes: [:id, :_destroy, :done, :name]
      )
    end

    def get_quote
      quotes = [
          "Education is not preparation for life; education <i>is</i> life itself.<br> – John Dewey",
          "We have all much to learn: Let us teach one another as <i>kindly</i> as we can.<br> – Reade",
          "Few of us know what we are capable of doing . . .; <i>We have never pushed ourselves</i> hard enough to find out.<br> – Alfred A. Montapert",
          "Success seems to be largely a matter of <i>hanging on</i> after others have let go.<br> – William Feather",
          "If you want learning, you must <i>work</i> for it.<br> – Josiah Gilbert Holland",
          "Achievements are the accomplishment of <i>persistent</i> individuals.<br> – James Jones",
          "Failing to prepare is preparing to fail.<br> – Anonymous"
        ] 
      index = Random.rand(quotes.size - 1)

      return quotes[index]
    end
end
