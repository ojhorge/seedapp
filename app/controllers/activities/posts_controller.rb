class Activities::PostsController < PostsController	
	before_action :set_postable

	private

	def set_postable
		@postable = PublicActivity::Activity.find(params[:activity_id])
	end

end