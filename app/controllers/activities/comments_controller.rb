class Activities::CommentsController < CommentsController
	before_action :set_commentable

	private

	def set_commentable
		@commentable = PublicActivity::Activity.find(params[:activity_id])
	end

end