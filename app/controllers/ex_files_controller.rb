class ExFilesController < ApplicationController
  before_action :authenticate_user!
  before_action :set_user
  before_action :set_ex_file, only: [:show, :edit, :update, :destroy]

  # GET /ex_files
  # GET /ex_files.json
  def index
    if params[:cat]
      @ex_files = current_user.ex_files.all.where(category_id: Category.where(slug: params[:cat]))
    else
      @ex_files = current_user.ex_files.all
    end
  end

  # GET /ex_files/1
  # GET /ex_files/1.json
  def show
    redirect_to @ex_file.attachment.url
  end

  # GET /ex_files/new
  def new
    @ex_file = current_user.ex_files.new
  end

  # GET /ex_files/1/edit
  def edit
  end

  # POST /ex_files
  # POST /ex_files.json
  def create
    @ex_file = current_user.ex_files.new(ex_file_params)
    @ex_file.guid = SecureRandom.base64[1..11]

    respond_to do |format|
      if @ex_file.save
        format.html { redirect_to user_ex_files_url(current_user.username), notice: 'File was successfully created.' }
        format.json { render :show, status: :created, location: @ex_file }
      else
        format.html { render :new }
        format.json { render json: @ex_file.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /ex_files/1
  # PATCH/PUT /ex_files/1.json
  def update
    @user = User.find(params[:user_id])
    @ex_file = @user.ex_files.find(params[:id])

    respond_to do |format|
      if @ex_file.update(ex_file_params)
        format.html { redirect_to user_ex_file_url(@user.username, @ex_file.guid), notice: 'File was successfully updated.' }
        format.json { render :show, status: :ok, location: @ex_file }
      else
        format.html { render :edit }
        format.json { render json: @ex_file.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /ex_files/1
  # DELETE /ex_files/1.json
  def destroy
    @ex_file.destroy
    respond_to do |format|
      format.html { redirect_to user_ex_files_url(@user.username), notice: 'File was successfully removed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ex_file
      @ex_file = ExFile.find_by_guid(params[:id])
    end

    def set_user
      @user = User.find_by(username: params[:user_id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def ex_file_params
      params.require(:ex_file).permit(:user_id, :name, :description, :category_id, :guid, :attachment)
    end
end
