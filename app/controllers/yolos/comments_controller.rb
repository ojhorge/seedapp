class Yolos::CommentsController < CommentsController
	before_action :set_commentable

	private

	def set_commentable
		@commentable = Yolo.find(params[:yolo_id])
	end

end