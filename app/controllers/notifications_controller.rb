class NotificationsController < ApplicationController
  def index
  	@user = current_user
		@users = @user.active_friends
		@users.push(current_user)
		case params[:content] when 'posts'
			@notifications = PublicActivity::Activity.where(owner_id: @users, trackable_type: "Post").order('created_at DESC')
		else
			@notifications = PublicActivity::Activity.where(owner_id: @users).order('created_at DESC')
		end
		PublicActivity::Activity.where(recipient_id: current_user.id).update_all(:read => true)
		PublicActivity::Activity.where(owner_id: current_user.id).update_all(:read => true)
		@notifications.update_all(:read => true)
		# @notifications = PublicActivity::Activity.where(owner_id: @user.id).order('created_at DESC')
  end

  # def read_all_notification
  	
  # end 
end
