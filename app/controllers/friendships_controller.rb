class FriendshipsController < ApplicationController
  before_action :authenticate_user!
  before_action :set_user, only: [:create]
  before_action :set_friendship, only: [:destroy, :accept, :upvote, :downvote]

  # Friendship Requests
  def create
    @friendship = current_user.request_friendship(@user)
    @friendship.create_activity key: 'friendship.requested', owner: @friendship.user, recipient: @friendship.friend
    respond_to do |format|
      format.html {redirect_to users_path, notice: "Friendship Requested"}
    end
  end

  def destroy
    @friendship.destroy
    respond_to do |format|
      format.html {redirect_to users_path, notice: "Friendship Deleted"}
    end
  end

  def accept
    @friendship.accept_friendship
    @friendship.create_activity key: 'friendship.accepted', owner: @friendship.user, recipient: @friendship.friend
    # @friendship.create_activity key: 'friendship.accepted', owner: @friendship.friend, recipient: @friendship.user
    respond_to do |format|
      format.html {redirect_to users_path, notice: "Friendship Accepted"}
    end
  end

  # Likes
  def upvote 
    current_user.like!(@friendship)
    @friendship.likees_count += 1
    @friendship.save
    redirect_to :back
  end  

  # Unlikes
  def downvote
    current_user.unlike!(@friendship)
    @friendship.likees_count -= 1
    @friendship.save
    redirect_to :back
  end

  private

  def set_user
    @user = User.find(params[:user_id])
  end

  def set_friendship
    @friendship = Friendship.find(params[:id])
  end
end
