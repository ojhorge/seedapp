class TransactionsController < ApplicationController
  	# before_filter :load_sellable

  	def create
		
		# sale = @sellable.create_sale(
		# 	amount: @sellable.payment_price,
		# 	buyer_email: current_user.email,
		# 	seller_email: Rails.configuration.stripe[:stripe_seller_email],
		# 	stripe_token: params[:stripeToken])

		# sale.process!
		# if sale.finished?
		# 	redirect_to @sellable, notice: "Transaction Successful"
		# else
		# 	redirect_to :back, notice: "Something went wrong."
		# end

		token = params[:stripeToken]

		begin 
    
		    charge = Stripe::Charge.create(
				amount: 149,
				currency: "gbp",
				card: token,
				description: current_user.email)
     
     		@sale = @sellable.create_sale!(buyer_email: current_user.email, amount: @sellable.payment_price, seller_email: Rails.configuration.stripe[:stripe_seller_email])
     		# redirect_to pickup_url(guid: @sale.guid)
     		redirect_to @sellable, notice: "Transaction Successful"

     		rescue Stripe::CardError => e
     
  			@error = e
      
  			redirect_to :back, notice: @error
      
 		end


	end

	def pickup
		@sale = Sale.find_by!(guid: params[:guid])
		@sellable = @sale.sellable
	end

	private
	# def find_sellable
 #      	params.each do |name, value|
 #        	if name =~ /(.+)_id$/
 #          		return $1.classify.constantize.find(value)
 #        	end
 #    	end
 #  	end
 # 	def load_sellable
	#     resource, id = request.path.split('/')[1, 2]
	#     @sellable = resource.singularize.classify.constantize.find(id)
	# end

	# def load_sellable
	#     klass = [Classified, JobApplication].detect { |c| params["#{c.name.underscore}_slug"] }
	#     @sellable = klass.find(params["slug"])
	# end

end
