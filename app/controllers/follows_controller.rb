class FollowsController < ApplicationController
  # before_filter :load_socializable
  before_action :authenticate_user!
  before_action :set_user

  def follow
  	current_user.follow!(@user)
  	respond_to do |format|
      format.html {redirect_to users_path, notice: "You are now following #{@user.full_name}."}
    end
  end

  def unfollow
  	current_user.unfollow!(@user)
  	respond_to do |format|
      format.html {redirect_to users_path, notice: "You are not following #{@user.full_name} anymore."}
    end
  end

  private

  def set_user
  	@user = User.find_by_username(params[:id])
  end
end
