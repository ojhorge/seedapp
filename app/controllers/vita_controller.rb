class VitaController < ApplicationController
  before_action :authenticate_user!
  before_action :set_user
  before_action :set_vitum, only: [:show, :edit, :destroy]

  # GET /vita
  # GET /vita.json
  def index
    if current_user.business?
      @vita = Vitum.all
    elsif current_user.student?
      if current_user.vita.any?
        redirect_to vitum_path(current_user.vita.first.guid)
      else
        redirect_to new_vitum_path
        # @vita = current_user.vita.all
      end
    end
  end

  # GET /vita/1
  # GET /vita/1.json
  def show
    @benchmark_reports = @vitum.user.benchmark_reports.all
    # gon.rabl "app/views/benchmark_reports/index.json.rabl", as: "benchmark"

    points = []
    period = []
    avg = []
    max = []

    @benchmark_reports.each do |benchmark|
      points.push(benchmark.points)
      period.push(benchmark.month.to_s + '/' + benchmark.year.to_s)
      avg.push(benchmark.average)
      max.push(benchmark.maximum)
    end
    
    @data = { labels: period, 
              datasets: [
                {
                  label: "Highest Benchmark",
                  fillColor: "rgba(189, 80, 43, 0.0)",
                  strokeColor: "rgba(189, 80, 43, 1)",
                  pointColor: "rgba(189, 80, 43, 1)",
                  pointStrokeColor: "#fff",
                  pointHighlightFill: "#fff",
                  pointHighlightStroke: "rgba(189, 80, 43, 1)",
                  data: max
                },
                {
                  label: "#{@vitum.user.first_name}'s' Benchmark",
                  fillColor: "rgba(80, 179, 110, 0.0)",
                  strokeColor: "rgba(80, 179, 110, 1)",
                  pointColor: "rgba(80, 179, 110, 1)",
                  pointStrokeColor: "#fff",
                  pointHighlightFill: "#fff",
                  pointHighlightStroke: "rgba(80, 179, 110, 1)",
                  data: points
                },
                {
                  label: "Average Benchmark",
                  fillColor: "rgba(60, 155, 207, 0.0)",
                  strokeColor: "rgba(60, 155, 207, 1)",
                  pointColor: "rgba(60, 155, 207, 1)",
                  pointStrokeColor: "#fff",
                  pointHighlightFill: "#fff",
                  pointHighlightStroke: "rgba(60, 155, 207, 1)",
                  data: avg
                }
              ]
            }

    @options = {
            generateLegend: true,
            width: 1100,

            # Boolean - Whether grid lines are shown across the chart
            scaleShowGridLines: true,

            # String - Colour of the grid lines
            scaleGridLineColor: "rgba(0,0,0,.05)",

            # Number - Width of the grid lines
            scaleGridLineWidth: 1,

            # Boolean - Whether to show horizontal lines (except X axis)
            scaleShowHorizontalLines: true,

            # Boolean - Whether to show vertical lines (except Y axis)
            scaleShowVerticalLines: true,

            # Boolean - Whether the line is curved between points
            bezierCurve: true,

            # Number - Tension of the bezier curve between points
            bezierCurveTension: 0.4,

            # Boolean - Whether to show a dot for each point
            pointDot: true,

            # Number - Radius of each point dot in pixels
            pointDotRadius: 4,

            # Number - Pixel width of point dot stroke
            pointDotStrokeWidth: 1,

            # Number - amount extra to add to the radius to cater for hit detection outside the drawn point
            pointHitDetectionRadius: 20,

            # Boolean - Whether to show a stroke for datasets
            datasetStroke: true,

            # Number - Pixel width of dataset stroke
            datasetStrokeWidth: 2,

            # Boolean - Whether to fill the dataset with a colour
            datasetFill: true,

            # String - A legend template
            legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"background-color:<%=datasets[i].strokeColor%>\">&nbsp;&nbsp;&nbsp;&nbsp;</span><%if(datasets[i].label){%>&nbsp;<%=datasets[i].label%><%}%></li><%}%></ul>",
            
            tooltipTemplate: "<%if (label){%><%=label%>: <%}%><%= value %>"
    }
  end

  # GET /vita/new
  def new
    @jobs = current_user.job_experiences.all
    @edu = current_user.educations.all
    @skills = current_user.skills.all
    @interest = current_user.interest_activities.all
    @vitum = current_user.vita.new
    @vitum.build_exec_summary
    # @vitum = current_user.vita.build(:educations => @edu, :jobs => @jobs, :skills => @skills, :interest_activities => @interest)
    @exec_summary = @vitum.exec_summary
  end

  # GET /vita/1/edit
  def edit
  end

  # POST /vita
  # POST /vita.json
  def create
    @vitum = current_user.vita.new(vitum_params)
    @vitum.guid = SecureRandom.base64[1..11]
    respond_to do |format|
      if @vitum.save
        format.html { redirect_to vita_path, notice: 'Vita was successfully created.' }
        format.json { render :show, status: :created, location: @vitum.guid }
      else
        format.html { render :new }
        format.json { render json: @vitum.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /vita/1
  # PATCH/PUT /vita/1.json
  def update
    @vitum = Vitum.find(params[:id])
    respond_to do |format|
      if @vitum.update_attributes(vitum_params)
        format.html { redirect_to vitum_path(@vitum.guid), notice: 'Vitum was successfully updated.' }
        format.json { render :show, status: :ok, location: @vitum }
      else
        format.html { render :edit }
        format.json { render json: @vitum.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /vita/1
  # DELETE /vita/1.json
  def destroy
    @vitum.destroy
    respond_to do |format|
      format.html { redirect_to vita_url, notice: 'Vitum was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_user
      @user = User.find_by(username: params[:user_id])
    end

    def set_vitum
      @vitum = Vitum.find_by(guid: params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def vitum_params
      params.require(:vitum).permit(
        :user_id, 
        :title, 
        :guid,
        exec_summary_attributes: [:id, :guid, :vitum_id, :description, :media_attachment, :_destroy],
        educations_attributes: [:id, :guid, :school_institution, :date_start, :date_finish, :city, :country, :degree, :field_study, :grade, :activities_societies, :description, :done, :media, :user_id, :vitum_id, :_destroy],
        job_experiences_attributes: [:id, :guid, :title, :company_name, :city, :country, :date_start, :date_finish, :current_working, :description, :done, :user_id, :vitum_id, :media, :_destroy],
        skills_attributes: [:id, :name, :done, :user_id, :vitum_id, :skill_format, :_destroy],
        interest_activities_atributes: [:id, :name, :description, :user_id, :media, :done, :vitum_id, :_destroy])
    end
end
