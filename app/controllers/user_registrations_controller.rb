class UserRegistrationsController < Devise::RegistrationsController

	def create
		@user = User.new(sign_up_params)
		if @user.role != "b"
			params[:user][:full_name] = params[:user][:username]
		end
		respond_to do |format|
			if @user.save
				case @user.role when 's'
					Role.add_student(@user)
				when 't'
					Role.add_tutor(@user)
				when 'u'
					Role.add_university(@user)
				when 'b'
					Role.add_business(@user)
				else
					render :back
				end
				# sign_in(@user)
				format.html { redirect_to new_user_session_path, notice: "User created successfully! Please check your email for further instructions." }
				format.json { render :edit, status: :created, location: @user }
			else
				format.html { render :action => 'back', :user => params[:user] }
				format.json { render json: @user.errors, status: :unprocessable_entity }
			end
		end
	end

	def update
		if @user.student? || @user.tutor?
			params[:user][:full_name] = params[:user][:first_name] + ' ' + params[:user][:last_name]				
		elsif @user.university?
			params[:user][:full_name] = params[:user][:university]
		end
		super
		# respond_to do |format|

		# 	if @user.update(account_update_params)
		# 		format.html { redirect_to root_path, notice: 'User was successfully updated.' }
		# 		format.json { render :show, status: :ok, location: @user }
		# 	else
		# 		format.html { render :edit }
		# 		format.json { render json: @user.errors, status: :unprocessable_entity }
		# 	end
		# end
	end


	private

	def sign_up_params
		params.require(:user).permit(
			:email, 
			:password, 
			:password_confirmation, 
			:username,
			:role,
			:full_name)
			# :toc)
	end

	def account_update_params
		params.require(:user).permit(
			:email, 
			:password, 
			:password_confirmation, 
			:username, 
			:current_password, 
			:dob, 
			:bio, 
			:gender,
			:role,
			:first_name,
			:last_name,
			:bio,
			:dob,
			:gender,
			:full_name,
			:university,
			:country,
			:city,
			:ucas_code,
			:address1,
			:address2,
			:address3,
			:post_code,
			# :vat,
			:number_of_employees,
			:company_number,
			:industry,
			:info_email,
			:contact_email,
			:phone_number,
			:website_link,
			:years_experience,
			:uni_graduated_at,
			:degree_type,
			:profession,
			:course,
			:year_uni,
			:publish_contact_info,
			:nsfw,
			:avatar,
			:background,
			:toc,
			:benchmark,
			:getting_started,
	        :cv_description,
	        :cv_doc,
	        :cl_description,
	        :cl_doc,
			teaching_availabilities_attributes: [:id, :_destroy, :done , :start_time, :end_time, :user_id],
	        educations_attributes: [:id, :_destroy, :done, :guid, :school_instituition, :date_start, :date_finish, :city, :country, :degree, :field_study, :grade, :activities_societies, :description],
	        job_experiences_attributes: [:id, :_destroy, :done , :guid, :title, :company_name, :city, :country, :date_start, :date_finish, :current_working, :description],
	        skills_attributes: [:id, :_destroy, :done, :name])
	end

end