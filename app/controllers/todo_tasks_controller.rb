class TodoTasksController < ApplicationController
  before_action :authenticate_user!
  before_action :set_user
  before_action :set_todo, only: [:show, :edit, :update, :destroy]

  # GET /todos
  # GET /todos.json
  def index
    @new_todo = current_user.todo_tasks.new    
    @todo_tasks = current_user.todo_tasks.all
    # render json: @todos
  end

  # GET /todos/1
  # GET /todos/1.json
  def show
    render json: @todo_task
  end

  # GET /todos/new
  def new
    @todo_task = current_user.todo_tasks.new
  end

  # GET /todos/1/edit
  def edit
  end

  # POST /todos
  # POST /todos.json
  def create
    # binding.pry
    @todo_task = current_user.todo_tasks.create(todo_params)
    respond_to do |format|
      format.html { redirect_to todo_tasks_path }
      format.js {}
    end

    
    # if @todo_task.save
    #   render json: @todo_task, status: :created, location: @todo_task
    # else
    #   render json: @todo_task.errors, status: :unprocessable_entity
    # end
  end

  # PATCH/PUT /todos/1
  # PATCH/PUT /todos/1.json
  def update
    if @todo_task.update(todo_params)
      render json: @todo_task
    else
      render json: @todo_task.errors, status: :unprocessable_entity
    end
  end

  # DELETE /todos/1
  # DELETE /todos/1.json
  def destroy
    @todo_task.destroy

    respond_to do |format|
      format.html { redirect_to todo_tasks_path }
      format.js { }
    end
  end

  private

    # Use to call the user
    def set_user
      @user = User.find_by(username: params[:user_id])
    end

    # Use callbacks to share common setup or constraints between actions.
    def set_todo
      @todo_task = TodoTask.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def todo_params
      params.require(:todo_task).permit(:title, :completed, :row_order, :priority, :comment, :archived, :user_id)
    end
end
