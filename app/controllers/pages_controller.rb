class PagesController < ApplicationController
  def home
    if current_user
      if current_user.getting_started == true
        redirect_to edit_user_registration_path
      else
        if current_user.business?
          redirect_to user_path(current_user.username)
        else
          redirect_to activities_path
        end
      end
    end
  end

  def about
  end

  def community
  end

  def contact
  end

  def copyright
  end

  def privacy
  end

  def terms
  end

  def toc
  end  

  def cpolicy    
  end

  def find_tutor
   @classifieds = Classified.all
  end

  def find_university
    @classifieds = Classified.all
  end
end
