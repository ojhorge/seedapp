class YolosController < ApplicationController
  before_action :authenticate_user!
  before_action :set_yolo, only: [:show, :edit, :update, :destroy, :upvote, :downvote]

  # GET /yolos
  # GET /yolos.json
  def index
    @yolos = Yolo.all.order("created_at desc")
  end

  # GET /yolos/1
  # GET /yolos/1.json
  def show
  end

  # GET /yolos/new
  def new
    @yolo = current_user.yolos.new
  end

  # GET /yolos/1/edit
  def edit
  end

  # POST /yolos
  # POST /yolos.json
  def create
    @yolo = current_user.yolos.new(yolo_params)
    @yolo.guid = SecureRandom.base64[1..11]
    respond_to do |format|
      if @yolo.save
        format.html { redirect_to yolos_url, notice: 'Yolo was successfully created.' }
        format.json { render :show, status: :created, location: yolo_path(@yolo.guid) }
      else
        format.html { render :new }
        format.json { render json: @yolo.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /yolos/1
  # PATCH/PUT /yolos/1.json
  def update
    respond_to do |format|
      if @yolo.update(yolo_params)
        format.html { redirect_to yolo_path(@yolo.guid), notice: 'Yolo was successfully updated.' }
        format.json { render :show, status: :ok, location: yolo_path(@yolo.guid) }
      else
        format.html { render :edit }
        format.json { render json: @yolo.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /yolos/1
  # DELETE /yolos/1.json
  def destroy
    @yolo.destroy
    respond_to do |format|
      format.html { redirect_to yolos_url, notice: 'Yolo was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def upvote 
    current_user.like!(@yolo)
    @yolo.likees_count += 1
    @yolo.save
    redirect_to :back
  end  

  def downvote
    current_user.unlike!(@yolo)
    @yolo.likees_count -= 1
    @yolo.save
    redirect_to :back
  end


  private
    # Use callbacks to share common setup or constraints between actions.
    def set_yolo
      @yolo = Yolo.find_by_guid(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def yolo_params
      params.require(:yolo).permit(:user_id, :content, :content_html, :media, :guid, :likees_count)
    end
end
