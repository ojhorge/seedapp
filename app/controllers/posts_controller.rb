class PostsController < ApplicationController
  before_action :set_post, only: [:edit, :update, :destroy]
  # before_filter :load_postable

  def create
    # @post = @postable.posts.new(post_params)
    # @post.user_id = current_user.id
    @post = current_user.posts.new(post_params)
    if @post.save
      if @post.media != []
        @post.media.update_all(user_id: current_user.id, description: @post.content, guid: SecureRandom.hex[1..11])
      end
      @post.user.benchmark += @post.post_type.benchmark_value
      @post.user.save
      @post.create_activity key: 'post.created', owner: @post.user
      respond_to do |format|
        format.html { redirect_to user_path(@post.user.username), notice: "Post created!" }
      end
    else
      redirect_to user_path(@post.user.username), notice: "Something went wrong!"
    end
  end

  def create_from_activity
    # @post = @postable.posts.new(post_params)
    # @post.user_id = current_user.id
    @post = current_user.posts.new(post_params)
    if @post.save
      if @post.media != []
        @post.media.update_all(user_id: current_user.id, description: @post.content)
        @post.media.each do |media|
          media.guid = SecureRandom.hex[1..11]
          media.save
        end
      end
      @post.user.benchmark += @post.post_type.benchmark_value
      @post.user.save
      @post.create_activity key: 'post.created', owner: @post.user
      respond_to do |format|
        format.html { redirect_to activities_path, notice: "Post created!" }
      end
    else
      redirect_to activities_path, notice: "Something went wrong!"
    end
  end

  def edit
  end

  def update
    if @post.update(post_params)
      respond_to do |format|
        format.html { redirect_to user_path(@post.user.username), notice: "Post updated!" }
      end
    else
      redirect_to :back, notice: "Something went wrong!"
    end
  end

  def destroy
    @post.user.benchmark -= @post.post_type.benchmark_value
    @post.user.save
    @post.destroy    
    respond_to do |format|
      format.html { redirect_to :back, notice: "Post Removed!" }
    end
  end

  def upvote 
    @post = Post.find(params[:id])
    # @post.liked_by current_user
    current_user.like!(@post)
    @post.likees_count += 1
    @post.save
    redirect_to :back
  end  

  def downvote
    @post = Post.find(params[:id])
    # @post.disliked_by current_user
    current_user.unlike!(@post)
    @post.likees_count -= 1
    @post.save
    redirect_to :back
  end

  private

  def set_post
    @post = Post.find(params[:id])
  end

  def post_params
    params.require(:post).permit(
      :content,
      :post_type_id,
      :likees_count,
      media_attributes: [:id, :_destroy, :done, :guid, :user_id, :post_id, :media_attachment, :description]
      )
  end

  def load_postable
    resource, id = request.path.split('/')[1, 2]
    @postable = resource.singularize.classify.constantize.find(id)
  end

end