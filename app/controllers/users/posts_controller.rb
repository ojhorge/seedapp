class Users::PostsController < PostsController
	before_action :set_postable

	private

	def set_postable
		@postable = User.find(params[:user_id])
	end

end