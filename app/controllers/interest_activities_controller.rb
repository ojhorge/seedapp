class InterestActivitiesController < ApplicationController
  before_action :set_interest_activity, only: [:show, :edit, :update, :destroy]

  # GET /interest_activities
  # GET /interest_activities.json
  def index
    @interest_activities = InterestActivity.all
  end

  # GET /interest_activities/1
  # GET /interest_activities/1.json
  def show
  end

  # GET /interest_activities/new
  def new
    @interest_activity = InterestActivity.new
  end

  # GET /interest_activities/1/edit
  def edit
  end

  # POST /interest_activities
  # POST /interest_activities.json
  def create
    @interest_activity = InterestActivity.new(interest_activity_params)

    respond_to do |format|
      if @interest_activity.save
        format.html { redirect_to @interest_activity, notice: 'Interest activity was successfully created.' }
        format.json { render :show, status: :created, location: @interest_activity }
      else
        format.html { render :new }
        format.json { render json: @interest_activity.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /interest_activities/1
  # PATCH/PUT /interest_activities/1.json
  def update
    respond_to do |format|
      if @interest_activity.update(interest_activity_params)
        format.html { redirect_to @interest_activity, notice: 'Interest activity was successfully updated.' }
        format.json { render :show, status: :ok, location: @interest_activity }
      else
        format.html { render :edit }
        format.json { render json: @interest_activity.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /interest_activities/1
  # DELETE /interest_activities/1.json
  def destroy
    @interest_activity.destroy
    respond_to do |format|
      format.html { redirect_to interest_activities_url, notice: 'Interest activity was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_interest_activity
      @interest_activity = InterestActivity.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def interest_activity_params
      params.require(:interest_activity).permit(:name, :description, :user_id, :done, :media)
    end
end
