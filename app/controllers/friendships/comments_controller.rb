class Friendships::CommentsController < CommentsController
	before_action :set_commentable

	private

	def set_commentable
		@commentable = Friendship.find(params[:friendship_id])
	end

end