class OnlineLearningsController < ApplicationController
  before_action :set_online_learning, only: [:show, :edit, :update, :destroy]

  # GET /online_learnings
  # GET /online_learnings.json
  def index
    # @online_learnings = OnlineLearning.all
    @videos = Video.all
    @categories = Category.joins(:videos)
    @headline = Video.all.order("view_count DESC").first # .order("view_count DESC").first)
    @populars = Video.all.order("view_count DESC").limit(10).offset(1)
    @related = Video.all.order("view_count DESC").limit(3).offset(1)
    # @videoCategory = Videos.where(category: params[:cat])
  end

  # GET /online_learnings/1
  # GET /online_learnings/1.json
  def show
  end

  # GET /online_learnings/new
  def new
    @online_learning = OnlineLearning.new
  end

  # GET /online_learnings/1/edit
  def edit
  end

  # POST /online_learnings
  # POST /online_learnings.json
  def create
    @online_learning = OnlineLearning.new(online_learning_params)

    respond_to do |format|
      if @online_learning.save
        format.html { redirect_to @online_learning, notice: 'Online learning was successfully created.' }
        format.json { render :show, status: :created, location: @online_learning }
      else
        format.html { render :new }
        format.json { render json: @online_learning.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /online_learnings/1
  # PATCH/PUT /online_learnings/1.json
  def update
    respond_to do |format|
      if @online_learning.update(online_learning_params)
        format.html { redirect_to @online_learning, notice: 'Online learning was successfully updated.' }
        format.json { render :show, status: :ok, location: @online_learning }
      else
        format.html { render :edit }
        format.json { render json: @online_learning.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /online_learnings/1
  # DELETE /online_learnings/1.json
  def destroy
    @online_learning.destroy
    respond_to do |format|
      format.html { redirect_to online_learnings_url, notice: 'Online learning was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_online_learning
      @online_learning = OnlineLearning.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def online_learning_params
      params[:online_learning]
    end
end
