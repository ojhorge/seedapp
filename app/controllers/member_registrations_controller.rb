class MemberRegistrationsController < Devise::RegistrationsController

	def create
		params[:member][:full_name] = params[:member][:membername]
		@member = Member.new(sign_up_params)
		respond_to do |format|
			if @member.save
				case @member.role when 's'
					Role.add_student(@member)
				when 't'
					Role.add_tutor(@member)
				when 'u'
					Role.add_university(@member)
				when 'b'
					Role.add_business(@member)
				else
					render :back
				end
				sign_in(@member)
				format.html { redirect_to edit_member_registration_path, notice: "Member created successfully!" }
				format.json { render :edit, status: :created, location: @member }
			else
				format.html { render :action => 'back', :member => params[:member] }
				format.json { render json: @member.errors, status: :unprocessable_entity }
			end
		end
	end

	def update
		if @member.student? || @member.tutor?
			params[:member][:full_name] = params[:member][:first_name] + ' ' + params[:member][:last_name]				
		elsif @member.university?
			params[:member][:full_name] = params[:member][:university]
		end
		super
		# respond_to do |format|

		# 	if @member.update(account_update_params)
		# 		format.html { redirect_to root_path, notice: 'Member was successfully updated.' }
		# 		format.json { render :show, status: :ok, location: @member }
		# 	else
		# 		format.html { render :edit }
		# 		format.json { render json: @member.errors, status: :unprocessable_entity }
		# 	end
		# end
	end


	private

	def sign_up_params
		params.require(:member).permit(
			:email, 
			:password, 
			:password_confirmation, 
			:membername,
			:role)
			# :toc)
	end

	def account_update_params
		params.require(:member).permit(
			:email, 
			:password, 
			:password_confirmation, 
			:membername, 
			:current_password, 
			:dob, 
			:bio, 
			:gender,
			:role,
			:first_name,
			:last_name,
			:bio,
			:dob,
			:gender,
			:full_name,
			:university,
			:country,
			:city,
			:ucas_code,
			:address1,
			:address2,
			:address3,
			:post_code,
			# :vat,
			:number_of_employees,
			:company_number,
			:industry,
			:info_email,
			:contact_email,
			:phone_number,
			:website_link,
			:years_experience,
			:uni_graduated_at,
			:degree_type,
			:profession,
			:course,
			:year_uni,
			:publish_contact_info,
			:nsfw,
			:avatar,
			:background,
			:toc,
			:benchmark,
			:getting_started,
			teaching_availabilities_attributes: [:id, :_destroy, :done , :start_time, :end_time, :member_id],
	        educations_attributes: [:id, :_destroy, :done, :guid, :school_instituition, :date_start, :date_finish, :city, :country, :degree, :field_study, :grade, :activities_societies, :description],
	        job_experiences_attributes: [:id, :_destroy, :done , :guid, :title, :company_name, :city, :country, :date_start, :date_finish, :current_working, :description],
	        skills_attributes: [:id, :_destroy, :done, :name])
	end
end