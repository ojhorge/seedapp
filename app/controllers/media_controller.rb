class MediaController < ApplicationController
  before_action :set_user, only: [:index, :show]
  before_action :set_medium, only: [:show, :edit, :update, :destroy]
  before_action :set_media, only: [:show, :index]
  # GET /media
  # GET /media.json
  def index
  end

  # GET /media/1
  # GET /media/1.json
  def show
    @pics = @media.where("media_attachment_content_type like ?", "%image%")
    @vids = @media.where("media_attachment_content_type like ?", "%video%")
  end

  # GET /media/new
  def new
    @medium = Medium.new
  end

  # GET /media/1/edit
  def edit
  end

  # POST /media
  # POST /media.json
  def create
    @medium = current_user.media.new(medium_params)
    # @medium.user_id = current_user.id
    @medium.guid = SecureRandom.base64[1..11]
    if @medium.post_id != nil && @medium.post_id != ""
      @medium.description = @medium.post.description
    end
    respond_to do |format|
      if @medium.save
        format.html { redirect_to media_path(@medium.user.username), notice: 'Medium was successfully created.' }
        format.json { render :show, status: :created, location: media_path(@medium.user.username) }
      else
        format.html { render :new }
        format.json { render json: @medium.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /media/1
  # PATCH/PUT /media/1.json
  def update
    respond_to do |format|
      if @medium.update(medium_params)
        format.html { redirect_to @medium, notice: 'Medium was successfully updated.' }
        format.json { render :show, status: :ok, location: @medium }
      else
        format.html { render :edit }
        format.json { render json: @medium.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /media/1
  # DELETE /media/1.json
  def destroy
    @medium.destroy
    respond_to do |format|
      format.html { redirect_to media_url, notice: 'Medium was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_medium
      # @medium = Medium.find(params[:id])
    end

    def set_media
      @media = Medium.all.where(user_id: @user.id)
    end

    def set_user
      @user = User.find_by_username(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def medium_params
      params.require(:medium).permit(:user_id, :description, :guid, :post_id, :media_attachment)
    end
end
