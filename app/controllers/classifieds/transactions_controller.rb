class Classifieds::TransactionsController < TransactionsController
	before_action :set_sellable

	private

	def set_sellable
		@sellable = Classified.friendly.find(params[:classified_id])
	end

end