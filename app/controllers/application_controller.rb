class ApplicationController < ActionController::Base
	before_filter :configure_permitted_parameters, if: :devise_controller?
	# before_filter :check_for_video_crunch

	helper_method :request_count
	helper_method :notification_count
	# helper_method :chat_friends
	helper_method :todo_tasks
	helper_method :mailbox, :conversation
	# Prevent CSRF attacks by raising an exception.
	# For APIs, you may want to use :null_session instead.
	protect_from_forgery with: :exception
	# before_filter :redirect_to_https

	# def redirect_to_https
	#  	redirect_to :protocol => "https://" unless request.ssl?
	# end


	# def check_for_video_crunch
	#   if Model.where(:video_processing => true).each do |s|
	#     flash[:video_messages] = Array.new
	#     flash[:video_messages] << "Video file is currently proccessing for #{s.title}."
	#   end.empty?
	#   end
	# end

	def todo_tasks
		current_user.tasks.all
	end

	def chat_friends
		current_user.active_friends
	end

	def request_count
		current_user.pending_friend_requests_from.map(&:user).size  	
	end

	def notification_count
		PublicActivity::Activity.where(owner_id: @users).order('created_at DESC').where(:read => false).count
	end
  
	private

	def mailbox
		@mailbox ||= current_user.mailbox
	end

	def conversation
		@conversation ||= mailbox.conversations.find(params[:id])
	end

	protected
	def configure_permitted_parameters
	    devise_parameter_sanitizer.for(:sign_up) { |u| u.permit(:username, :email, :password, :password_confirmation, :remember_me) }
	    devise_parameter_sanitizer.for(:sign_in) { |u| u.permit(:login, :username, :email, :password, :remember_me) }
	end


end
