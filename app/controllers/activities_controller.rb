class ActivitiesController < ApplicationController
	before_action :authenticate_user!, only: [:index]

	def index
		@post = Post.new
		@user = current_user
		@users = @user.active_friends
		@users.push(current_user.followees(User))
		@users.push(current_user)
		# @activities = []
		case params[:content] when 'posts'
			@activities = PublicActivity::Activity.where(owner_id: @users, trackable_type: "Post").order('created_at DESC')
		else
			@activities = PublicActivity::Activity.where(owner_id: @users).order('created_at DESC')
		end
  	@notifications = PublicActivity::Activity.where(owner_id: @user.id).order('created_at DESC').limit(8)

    	# @activities.each_with_index do |activity|
    	# 	if activity.trackable != nil
	    # 		@commentable = activity.trackable
	    # 		@comments = @commentable.comments
	    # 	end
    	# end

		@quote = get_quote
		if !Poll.none == false
			@poll = Poll.last
		end
		@groups = Group.all
		@events = Event.where("end_time >= ?", Time.now).order("start_time asc").limit(4)
		@exchange = current_user.ex_files.all

		# Exchange Files Categories
		@categories = []
		@exchange.each do |exchange|
			@categories.push(exchange.category)
		end

		@categories = @categories.uniq

		# @comment = current_user.comments.new
		@post.media.build unless @post.media.any?
		@yolos = Yolo.all.limit(6)
	end

	private
	def get_quote
		quotes = [
				"Education is not preparation for life; education <i>is</i> life itself.<br> – John Dewey",
				"We have all much to learn: Let us teach one another as <i>kindly</i> as we can.<br> – Reade",
				"Few of us know what we are capable of doing . . .; <i>We have never pushed ourselves</i> hard enough to find out.<br> – Alfred A. Montapert",
				"Success seems to be largely a matter of <i>hanging on</i> after others have let go.<br> – William Feather",
				"If you want learning, you must <i>work</i> for it.<br> – Josiah Gilbert Holland",
				"Achievements are the accomplishment of <i>persistent</i> individuals.<br> – James Jones",
				"Failing to prepare is preparing to fail.<br> – Anonymous"
			]
		index = Random.rand(quotes.size - 1)

		return quotes[index]
	end
end
