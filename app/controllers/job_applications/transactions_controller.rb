class JobApplications::TransactionsController < TransactionsController
	before_action :set_sellable

	private

	def set_sellable
		@sellable = JobApplication.friendly.find(params[:job_application_id])
	end

end