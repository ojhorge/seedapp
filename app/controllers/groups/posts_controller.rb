class Groups::PostsController < PostsController
	before_action :set_postable

	private

	def set_postable
		@postable = Group.find(params[:group_id])
	end

end