class ClassifiedsController < ApplicationController
  before_action :set_classified, only: [:show, :edit, :update, :destroy, :purchase]

  # GET /classifieds
  # GET /classifieds.json
  def index
    @classifieds = (current_user.classifieds + Classified.where(availability: false).joins(:sale)).uniq
    if params[:type] == "boo"
      @classifieds = (current_user.classifieds + Classified.where(availability: false, cat: "book").joins(:sale)).uniq
    elsif params[:type] == "acco"
      @classifieds = (current_user.classifieds + Classified.where(availability: false, cat: "accommodation").joins(:sale)).uniq
    end
  end

  # GET /classifieds/1
  # GET /classifieds/1.json
  def show
  end

  def purchase
    @sellable = @classified
  end

  # GET /classifieds/new
  def new
    @classified = current_user.classifieds.new
    # @classified.build_sale
  end

  # GET /classifieds/1/edit
  def edit
  end

  # POST /classifieds
  # POST /classifieds.json
  def create
    @classified = current_user.classifieds.new(classified_params)
  

    # if @classified.save
    respond_to do |format|
      if @classified.save
        format.html { redirect_to classified_path(@classified.slug), notice: 'Classified was created, now need to purchase the placement.' }
        # format.json { render :show, status: :ok, location: @classified }
      else
        format.html { render :new }
        # format.json { render json: @classified.errors, status: :unprocessable_entity }
      end
    end
    #   redirect_to @classified.paypal_url(classified_path(@classified))
    #   render show_buy_path(@classified.slug)
    # else
    #   render :new
    # end
  end

  # PATCH/PUT /classifieds/1
  # PATCH/PUT /classifieds/1.json
  def update
    respond_to do |format|
      if @classified.update(classified_params)
        format.html { redirect_to @classified, notice: 'Classified was successfully updated.' }
        format.json { render :show, status: :ok, location: @classified }
      else
        format.html { render :edit }
        format.json { render json: @classified.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /classifieds/1
  # DELETE /classifieds/1.json
  def destroy
    @classified.destroy
    respond_to do |format|
      format.html { redirect_to classifieds_url, notice: 'Classified was successfully removed.' }
      format.json { head :no_content }
    end
  end

  # protect_from_forgery except: [:hook]
  def hook
    params.permit! # Permit all Paypal input params
    status = params[:payment_status]
    if status == "Completed"
      @classified = Classified.find params[:invoice]
      @classified.update_attributes notification_params: params, status: status, transaction_id: params[:txn_id], purchased_at: Time.now
    end
    render nothing: true
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_classified
      @classified = Classified.friendly.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def classified_params
      params.require(:classified).permit(:title, :user_id, :description, :price, :availability, :notification_status, :status, :purchased_at, :cat, :pic, :payment_price)
    end
end
