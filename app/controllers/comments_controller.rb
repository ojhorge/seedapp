class CommentsController < ApplicationController
	before_action :authenticate_user!
	# before_filter :load_commentable
	# before_action :set_community_post, only: [:show, :edit, :update, :destroy]

	def new
		@comment = @commentable.comments.new
	end

	def create
		@comment = @commentable.comments.new comment_params
		# @comment = @commentable.comments.new(comment_params)
		@comment.user = current_user
		# @comment = Comment.build_from(@commentable, @user_who_commented, params[:body])
		if @comment.save
	      redirect_to :back, notice: "Comment created."
	    else
	      redirect_to :back, notice: "Something went wrong"
	    end
	end

	def edit
	end

	def delete
	end

	def upvote 
	  @comment = Comment.find(params[:id])
	  @comment.upvote_by current_user
	  redirect_to :back
	end  

	def downvote
	  @comment = Comment.find(params[:id])
	  @comment.downvote_by current_user
	  redirect_to :back
	end

	private

	# def load_commentable
	#     resource, id = request.path.split('/')[1, 2]
	#     @commentable = resource.singularize.classify.constantize.find(id)
	# end

	private
    def comment_params
      	params.require(:comment).permit(:user_id, :content)
    end

   #  def find_commentable
   #    	params.each do |name, value|
   #      	if name =~ /(.+)_id$/
   #        		return $1.classify.constantize.find(value)
   #      	end
   #  	end
  	# end

end
