// // This is shorthand for $( document ).ready(function() { })
// $(function(){
//   $("form").submit(function(event){
//     event.preventDefault();

//     var action = $(this).attr('action');
//     var method = $(this).attr('method');

//     // var title = $(this).find('#todo_task_title').val();
//     // var priority = $(this).find('#todo_task_priority').val();

//     // .serializeArray() can be called on any form element (and here, $(this) is our form)
//     var data = $(this).serializeArray();

//     $.ajax({
//       method: method,
//       url: action,
//       data: data,

//       // this line makes the response format JavaScript and not html.
//       dataType: 'script'
//     });

//     // this debugger should be hit when you click the submit button!
//     // debugger;
//   });
// });