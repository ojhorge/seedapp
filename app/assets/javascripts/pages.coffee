# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

# $ ->
#   $('[data-toggle="tooltip"]').tooltip()
#   return

$(document).ready ->
  $('.member_register').hide().removeClass("hidden")
  $('.business_register').hide().removeClass("hidden")

  @showLogin = ->
    login = $('.login')
    $('#memberReg').on 'click', ->
      $('.member_register').fadeOut 1000, ->
        login.fadeIn 1000
        return
      return
    $('#businessReg').on 'click', ->
      $('.business_register').fadeOut 1000, ->
        login.fadeIn 1000
        return
      return

  @showMemberRegistration = ->
    $('.business_register').fadeOut 1000, ->
      $('.member_register').fadeIn 1000
      return
    $('.login').fadeOut 1000, ->
      $('.member_register').fadeIn 1000
      return
    return

  @showBusinessRegistration = ->
    $('.login').fadeOut 1000, ->
      $('.business_register').fadeIn 1000
      return
    $('.member_register').fadeOut 1000, ->
      $('.business_register').fadeIn 1000
      return
    return

  return
