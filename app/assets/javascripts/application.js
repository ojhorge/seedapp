// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery.turbolinks
//= require jquery_ujs
//= require jquery-ui.min
//= require moment
//= require pickers
//= require video
//= require fullcalendar
//= require bootstrap-datetimepicker
//= require chat
//= require private_pub
//= require html.sortable
//= require jquery.soulmate
//= require chosen-jquery
//= require Chart
//= require excanvas
//= require jasny-bootstrap
//= require toolkit
//= require cocoon
//= require_tree .

// $(document).ready(ready);
$(document).on('ready page:load', function(){
  $('#calendar').fullCalendar({
    events: '/events.json'
  });


  var ready = function(){
    var render, select;

    render = function(term, data, type) {
      return term;
    }

    select = function(term, data, type){
      // populate our search form with the autocomplete result
      $('#search').val(term);
     
      // hide our autocomplete results
      $('ul#soulmate').hide();
      // This can go to the top if needed however there is always conflits * = require twitter/bootstrap

      // then redirect to the result's link 
      // remember we have the link in the 'data' metadata
      return window.location.href = data.link
    }

    $('#search').soulmate({
      url: '/autocomplete/search',
      types: ['Users','Groups','Classifieds', 'Events', 'Opportunities'],
      renderCallback : render,
      selectCallback : select,
      minQueryLength : 2,
      maxResults     : 5
    });

  }

  $(window).scroll(function(){
      if ($(this).scrollTop() > 600) {
          $(".scrollToTop").fadeIn(1000);
      } else {
          $(".scrollToTop").fadeOut(1000);
      }
  });

  //Click event to scroll to top
  $(".scrollToTop").click(function(){
      $('html, body').animate({scrollTop : 0},500);
      return false;
  });
});
// when our document is ready, call our ready function
$(document).ready(ready);

// if using turbolinks, listen to the page:load event and fire our ready function
$(document).on('page:load', ready);

// $(document).on('page:change', function(){
//   $('[data-toggle="popover"]').popover();
// });