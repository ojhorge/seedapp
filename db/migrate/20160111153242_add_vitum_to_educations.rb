class AddVitumToEducations < ActiveRecord::Migration
  def change
    add_reference :educations, :vitum, index: true, foreign_key: true
  end
end
