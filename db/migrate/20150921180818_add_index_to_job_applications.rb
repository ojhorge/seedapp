class AddIndexToJobApplications < ActiveRecord::Migration
  def change
    add_index :job_applications, :guid, unique: true
  end
end
