class AddBenchmarkToUsers < ActiveRecord::Migration
  def change
    add_column :users, :benchmark, :integer, default: 40
  end
end
