class CreateVita < ActiveRecord::Migration
  def change
    create_table :vita do |t|
      t.belongs_to :user, index: true, foreign_key: true
      t.string :title
      t.string :guid

      t.timestamps null: false
    end
  end
end
