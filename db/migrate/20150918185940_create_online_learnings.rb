class CreateOnlineLearnings < ActiveRecord::Migration
  def change
    create_table :online_learnings do |t|

      t.timestamps null: false
    end
  end
end
