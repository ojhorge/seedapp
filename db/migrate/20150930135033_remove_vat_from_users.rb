class RemoveVatFromUsers < ActiveRecord::Migration
  def change
    remove_column :users, :vat, :string
  end
end
