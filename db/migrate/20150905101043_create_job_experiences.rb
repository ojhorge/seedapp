class CreateJobExperiences < ActiveRecord::Migration
  def change
    create_table :job_experiences do |t|
      t.string :guid
      t.string :title
      t.string :company_name
      t.string :city
      t.string :country
      t.date :date_start
      t.date :date_finish
      t.boolean :current_working
      t.text :description
      t.boolean :done
      t.belongs_to :detailbook, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
