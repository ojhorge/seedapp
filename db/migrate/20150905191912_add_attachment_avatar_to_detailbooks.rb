class AddAttachmentAvatarToDetailbooks < ActiveRecord::Migration
  def self.up
    change_table :detailbooks do |t|
      t.attachment :avatar
    end
  end

  def self.down
    remove_attachment :detailbooks, :avatar
  end
end
