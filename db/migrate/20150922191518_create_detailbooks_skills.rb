class CreateDetailbooksSkills < ActiveRecord::Migration
  def change
    create_table :detailbooks_skills do |t|
      t.integer :detailbook_id
      t.integer :skill_id
    end
  end
end
