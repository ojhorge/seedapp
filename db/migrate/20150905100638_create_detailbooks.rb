class CreateDetailbooks < ActiveRecord::Migration
  def change
    create_table :detailbooks do |t|
      t.belongs_to :user, index: true, foreign_key: true
      t.string :name
      t.string :last_name
      t.date :dob
      t.string :gender
      t.string :city
      t.string :country
      t.integer :benchmark

      t.timestamps null: false
    end
  end
end
