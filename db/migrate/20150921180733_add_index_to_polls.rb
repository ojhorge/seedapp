class AddIndexToPolls < ActiveRecord::Migration
  def change
    add_column :polls, :guid, :string
    add_index :polls, :guid, unique: true
  end
end
