class AddAttachmentClDocToApplicationForms < ActiveRecord::Migration
  def self.up
    change_table :application_forms do |t|
      t.attachment :cl_doc
    end
  end

  def self.down
    remove_attachment :application_forms, :cl_doc
  end
end
