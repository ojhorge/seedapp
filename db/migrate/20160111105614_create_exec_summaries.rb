class CreateExecSummaries < ActiveRecord::Migration
  def change
    create_table :exec_summaries do |t|
      t.text :description
      # t.belongs_to :vita, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
