class CreateSkills < ActiveRecord::Migration
  def change
    create_table :skills do |t|
      t.string :name
      t.boolean :done
      t.belongs_to :detailbook, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
