class CreatePollVotes < ActiveRecord::Migration
  def change
    create_table :poll_votes do |t|
      t.references :user, index: true, foreign_key: true
      t.references :vote_option, index: true, foreign_key: true

      t.timestamps null: false
    end

    add_index :poll_votes, [:vote_option_id, :user_id], unique: true
  end
end
