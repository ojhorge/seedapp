class CreateTeachingAvailabilities < ActiveRecord::Migration
  def change
    create_table :teaching_availabilities do |t|
      t.belongs_to :user, index: true, foreign_key: true
      t.string :guid
      t.datetime :start_time
      t.datetime :end_time

      t.timestamps null: false
    end
  end
end
