class AddAttachmentSkillFormatToSkills < ActiveRecord::Migration
  def self.up
    change_table :skills do |t|
      t.attachment :skill_format
    end
  end

  def self.down
    remove_attachment :skills, :skill_format
  end
end
