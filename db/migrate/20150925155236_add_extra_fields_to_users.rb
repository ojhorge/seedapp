class AddExtraFieldsToUsers < ActiveRecord::Migration
  def change
    add_column :users, :teach_online, :boolean
    add_column :users, :skype, :string
  end
end
