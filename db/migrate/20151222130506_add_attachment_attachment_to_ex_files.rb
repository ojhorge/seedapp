class AddAttachmentAttachmentToExFiles < ActiveRecord::Migration
  def self.up
    change_table :ex_files do |t|
      t.attachment :attachment
    end
  end

  def self.down
    remove_attachment :ex_files, :attachment
  end
end
