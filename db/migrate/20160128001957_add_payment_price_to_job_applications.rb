class AddPaymentPriceToJobApplications < ActiveRecord::Migration
  def change
    add_column :job_applications, :payment_price, :integer, default: 149
  end
end
