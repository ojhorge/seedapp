class AddAttachmentMediaToInterestActivities < ActiveRecord::Migration
  def self.up
    change_table :interest_activities do |t|
      t.attachment :media
    end
  end

  def self.down
    remove_attachment :interest_activities, :media
  end
end
