class AddAttachmentMediaAttachmentToExecSummaries < ActiveRecord::Migration
  def self.up
    change_table :exec_summaries do |t|
      t.attachment :media_attachment
    end
  end

  def self.down
    remove_attachment :exec_summaries, :media_attachment
  end
end
