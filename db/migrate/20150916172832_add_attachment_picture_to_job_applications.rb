class AddAttachmentPictureToJobApplications < ActiveRecord::Migration
  def self.up
    change_table :job_applications do |t|
      t.attachment :picture
    end
  end

  def self.down
    remove_attachment :job_applications, :picture
  end
end
