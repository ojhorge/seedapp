class AddAttachmentCvDocToApplicationForms < ActiveRecord::Migration
  def self.up
    change_table :application_forms do |t|
      t.attachment :cv_doc
    end
  end

  def self.down
    remove_attachment :application_forms, :cv_doc
  end
end
