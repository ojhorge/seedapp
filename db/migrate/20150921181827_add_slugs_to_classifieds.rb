class AddSlugsToClassifieds < ActiveRecord::Migration
  def change
    add_column :classifieds, :slug, :string
    add_index :classifieds, :slug, unique: true
  end
end
