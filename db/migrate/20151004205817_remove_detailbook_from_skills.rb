class RemoveDetailbookFromSkills < ActiveRecord::Migration
  def change
    remove_reference :skills, :detailbook, index: true, foreign_key: true
  end
end
