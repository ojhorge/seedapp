class CreateClassifieds < ActiveRecord::Migration
  def change
    create_table :classifieds do |t|
      t.string :title
      t.belongs_to :user, index: true, foreign_key: true
      t.text :description
      t.float :price
      t.boolean :availability
      t.text :notification_status
      t.string :transaction_id 
      t.string :status
      t.datetime :purchased_at

      t.timestamps null: false
    end
  end
end
