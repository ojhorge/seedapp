class AddDoneToInterestActivities < ActiveRecord::Migration
  def change
    add_column :interest_activities, :done, :boolean
  end
end
