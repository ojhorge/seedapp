class AddCVandClToUsers < ActiveRecord::Migration
  def change
    add_column :users, :cv_description, :text
    add_column :users, :cl_description, :text
  end
end
