class CreateComments < ActiveRecord::Migration
  def change
    create_table :comments, :force => true do |t|
      t.belongs_to :user, index: true, foreign_key: true
      t.belongs_to :commentable, index: true, polymorphic: true
      t.text :content
      t.text :content_html

      t.timestamps null: false
    end
  end
end
