class AddAttachmentBackgroundToDetailbooks < ActiveRecord::Migration
  def self.up
    change_table :detailbooks do |t|
      t.attachment :background
    end
  end

  def self.down
    remove_attachment :detailbooks, :background
  end
end
