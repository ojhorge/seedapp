class AddAttachmentMediaToEducations < ActiveRecord::Migration
  def self.up
    change_table :educations do |t|
      t.attachment :media
    end
  end

  def self.down
    remove_attachment :educations, :media
  end
end
