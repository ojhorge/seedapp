class AddDetailsToGroups < ActiveRecord::Migration
  def change
    add_column :groups, :isPublic, :boolean
    add_column :groups, :title, :string
    add_column :groups, :description, :text
    add_column :groups, :tagline, :string
    add_column :groups, :slug, :string, index: true
  end
end
