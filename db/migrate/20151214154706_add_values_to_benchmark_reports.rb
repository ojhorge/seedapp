class AddValuesToBenchmarkReports < ActiveRecord::Migration
  def change
    add_column :benchmark_reports, :average, :integer
    add_column :benchmark_reports, :maximum, :integer
  end
end
