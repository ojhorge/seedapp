class AddDoneToTeachingAvailabilities < ActiveRecord::Migration
  def change
    add_column :teaching_availabilities, :done, :boolean
  end
end
