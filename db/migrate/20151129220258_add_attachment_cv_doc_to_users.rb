class AddAttachmentCvDocToUsers < ActiveRecord::Migration
  def self.up
    change_table :users do |t|
      t.attachment :cv_doc
    end
  end

  def self.down
    remove_attachment :users, :cv_doc
  end
end
