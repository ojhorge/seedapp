class AddBmValueToPostType < ActiveRecord::Migration
  def change
    add_column :post_types, :benchmark_value, :integer
  end
end
