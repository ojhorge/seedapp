class AddIndexToVideos < ActiveRecord::Migration
  def change
    add_column :videos, :guid, :string
    add_index :videos, :guid, unique: true
  end
end
