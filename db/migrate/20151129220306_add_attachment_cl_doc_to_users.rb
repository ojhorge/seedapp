class AddAttachmentClDocToUsers < ActiveRecord::Migration
  def self.up
    change_table :users do |t|
      t.attachment :cl_doc
    end
  end

  def self.down
    remove_attachment :users, :cl_doc
  end
end
