class AddMissingFieldsToClassifieds < ActiveRecord::Migration
  def change
    add_column :classifieds, :type, :string
  end
end
