class RemoveDetailbookFromEducations < ActiveRecord::Migration
  def change
    remove_reference :educations, :detailbook, index: true, foreign_key: true
  end
end
