class CreateCommunityPosts < ActiveRecord::Migration
  def change
    create_table :community_posts do |t|
      t.string :name
      t.text :content

      t.timestamps null: false
    end
  end
end
