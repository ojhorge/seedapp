class AddReadToActivities < ActiveRecord::Migration
  def change
    add_column :activities, :read, :boolean, :polymorphic => true, default: false
  end
end
