class AddUsersToTodoTasks < ActiveRecord::Migration
  def change
    add_reference :todo_tasks, :user, index: true, foreign_key: true
  end
end
