class AddAttachmentMediaToJobExperiences < ActiveRecord::Migration
  def self.up
    change_table :job_experiences do |t|
      t.attachment :media
    end
  end

  def self.down
    remove_attachment :job_experiences, :media
  end
end
