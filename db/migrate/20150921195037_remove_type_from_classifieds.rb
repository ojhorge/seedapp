class RemoveTypeFromClassifieds < ActiveRecord::Migration
  def change
    remove_column :classifieds, :type, :string
  end
end
