class AddUsersToJobExperiences < ActiveRecord::Migration
  def change
    add_reference :job_experiences, :user, index: true, foreign_key: true
  end
end
