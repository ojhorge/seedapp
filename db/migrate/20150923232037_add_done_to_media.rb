class AddDoneToMedia < ActiveRecord::Migration
  def change
    add_column :media, :done, :boolean
  end
end
