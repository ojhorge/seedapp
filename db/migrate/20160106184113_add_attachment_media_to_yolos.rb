class AddAttachmentMediaToYolos < ActiveRecord::Migration
  def self.up
    change_table :yolos do |t|
      t.attachment :media
    end
  end

  def self.down
    remove_attachment :yolos, :media
  end
end
