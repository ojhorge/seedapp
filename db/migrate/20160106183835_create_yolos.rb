class CreateYolos < ActiveRecord::Migration
  def change
    create_table :yolos do |t|
      t.belongs_to :user, index: true, foreign_key: true
      t.text :content
      t.text :content_html

      t.timestamps null: false
    end
  end
end
