class AddGuidToBenchmarkReports < ActiveRecord::Migration
  def change
    add_column :benchmark_reports, :guid, :string
  end
end
