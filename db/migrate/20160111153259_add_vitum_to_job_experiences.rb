class AddVitumToJobExperiences < ActiveRecord::Migration
  def change
    add_reference :job_experiences, :vitum, index: true, foreign_key: true
  end
end
