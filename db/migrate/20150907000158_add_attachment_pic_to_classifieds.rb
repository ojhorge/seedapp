class AddAttachmentPicToClassifieds < ActiveRecord::Migration
  def self.up
    change_table :classifieds do |t|
      t.attachment :pic
    end
  end

  def self.down
    remove_attachment :classifieds, :pic
  end
end
