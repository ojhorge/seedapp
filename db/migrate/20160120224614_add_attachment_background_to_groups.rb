class AddAttachmentBackgroundToGroups < ActiveRecord::Migration
  def self.up
    change_table :groups do |t|
      t.attachment :background
    end
  end

  def self.down
    remove_attachment :groups, :background
  end
end
