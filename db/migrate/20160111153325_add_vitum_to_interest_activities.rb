class AddVitumToInterestActivities < ActiveRecord::Migration
  def change
    add_reference :interest_activities, :vitum, index: true, foreign_key: true
  end
end
