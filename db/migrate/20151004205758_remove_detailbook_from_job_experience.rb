class RemoveDetailbookFromJobExperience < ActiveRecord::Migration
  def change
    remove_reference :job_experiences, :detailbook, index: true, foreign_key: true
  end
end
