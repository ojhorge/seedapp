class CreateExFiles < ActiveRecord::Migration
  def change
    create_table :ex_files do |t|
      t.belongs_to :user, index: true, foreign_key: true
      t.string :name
      t.text :description
      t.belongs_to :category, index: true, foreign_key: true
      t.string :guid

      t.timestamps null: false
    end
  end
end
