class AddAdditionalFieldsToUsers < ActiveRecord::Migration
  def change
    add_column :users, :getting_started, :boolean, default: true
    add_column :users, :role, :string
    add_column :users, :first_name, :string
    add_column :users, :last_name, :string
    add_column :users, :bio, :text
    add_column :users, :dob, :date
    add_column :users, :gender, :string
    add_column :users, :full_name, :string
    add_column :users, :university, :string
    add_column :users, :country, :string
    add_column :users, :city, :string
    add_column :users, :ucas_code, :string
    add_column :users, :address1, :string
    add_column :users, :address2, :string
    add_column :users, :address3, :string
    add_column :users, :post_code, :string
    add_column :users, :vat, :string
    add_column :users, :number_of_employees, :integer
    add_column :users, :company_number, :string
    add_column :users, :industry, :string
    add_column :users, :info_email, :string
    add_column :users, :contact_email, :string
    add_column :users, :phone_number, :string
    add_column :users, :website_link, :string
    add_column :users, :years_experience, :integer
    add_column :users, :uni_graduated_at, :string
    add_column :users, :degree_type, :string
    add_column :users, :profession, :string
    add_column :users, :course, :string
    add_column :users, :year_uni, :string
    add_column :users, :publish_contact_info, :boolean
    add_column :users, :nfsw, :string
  end
end
