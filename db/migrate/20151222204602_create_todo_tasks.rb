class CreateTodoTasks < ActiveRecord::Migration
  def change
    create_table :todo_tasks do |t|
      t.string :title
      t.boolean :completed
      t.integer :row_order
      t.integer :priority
      t.text :comment
      t.boolean :archived, null: false, default: false

      t.timestamps null: false
    end
  end
end
