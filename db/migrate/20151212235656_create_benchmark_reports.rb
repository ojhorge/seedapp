class CreateBenchmarkReports < ActiveRecord::Migration
  def change
    create_table :benchmark_reports do |t|
      t.belongs_to :user, index: true, foreign_key: true
      t.integer :points
      t.integer :month
      t.integer :year

      t.timestamps null: false
    end
  end
end
