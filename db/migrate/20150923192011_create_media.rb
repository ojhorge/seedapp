class CreateMedia < ActiveRecord::Migration
  def change
    create_table :media do |t|
      t.belongs_to :user, index: true, foreign_key: true
      t.text :description
      # t.string :guid
      t.belongs_to :post, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
