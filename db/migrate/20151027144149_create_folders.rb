class CreateFolders < ActiveRecord::Migration
  def change
    create_table :folders do |t|
      t.string :guid
      t.belongs_to :user, index: true, foreign_key: true
      t.string :name
      t.text :description
      t.text :sharable_to
      t.string :category

      t.timestamps null: false
    end
    add_index :folders, :guid, unique: true
  end
end
