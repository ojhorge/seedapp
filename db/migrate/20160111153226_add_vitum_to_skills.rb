class AddVitumToSkills < ActiveRecord::Migration
  def change
    add_reference :skills, :vitum, index: true, foreign_key: true
  end
end
