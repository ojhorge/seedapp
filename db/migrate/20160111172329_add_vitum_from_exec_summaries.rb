class AddVitumFromExecSummaries < ActiveRecord::Migration
  def change
    add_reference :exec_summaries, :vitum, index: true, foreign_key: true
  end
end
