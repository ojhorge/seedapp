class AddSharingToApplicationForms < ActiveRecord::Migration
  def change
    add_column :application_forms, :sharing, :boolean
  end
end
