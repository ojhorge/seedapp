class AddDeadlineToJobApplications < ActiveRecord::Migration
  def change
    add_column :job_applications, :deadline, :date
  end
end
