class AddLikeesCountToFriendships < ActiveRecord::Migration
  def change
    add_column :friendships, :likees_count, :integer, default: 0
  end
end
