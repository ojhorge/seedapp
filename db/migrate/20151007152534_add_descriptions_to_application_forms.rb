class AddDescriptionsToApplicationForms < ActiveRecord::Migration
  def change
    add_column :application_forms, :cv_description, :text
    add_column :application_forms, :cl_description, :text
  end
end
