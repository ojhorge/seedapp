class AddPaymentPriceToClassifieds < ActiveRecord::Migration
  def change
    add_column :classifieds, :payment_price, :integer, default: 149
  end
end
