class RemoveNfswFromUsers < ActiveRecord::Migration
  def change
    remove_column :users, :nfsw, :string
  end
end
