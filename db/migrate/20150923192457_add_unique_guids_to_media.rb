class AddUniqueGuidsToMedia < ActiveRecord::Migration
  def change
    add_column :media, :guid, :string
    add_index :media, :guid, unique: true
  end
end
