class CreateEducations < ActiveRecord::Migration
  def change
    create_table :educations do |t|
      t.string :guid
      t.string :school_institution
      t.date :date_start
      t.date :date_finish
      t.string :city
      t.string :country
      t.string :degree
      t.string :field_study
      t.string :grade
      t.string :activities_societies, array:true
      t.text :description
      t.boolean :done
      t.belongs_to :detailbook, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
