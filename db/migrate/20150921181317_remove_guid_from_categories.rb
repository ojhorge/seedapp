class RemoveGuidFromCategories < ActiveRecord::Migration
  def change
  	remove_column :categories, :guid, :string
  end
end
