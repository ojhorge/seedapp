class AddSlugsToSyllabuses < ActiveRecord::Migration
  def change
    add_column :syllabuses, :slug, :string
    add_index :syllabuses, :slug, unique: true
  end
end
