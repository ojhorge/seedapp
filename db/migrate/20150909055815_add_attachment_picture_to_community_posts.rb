class AddAttachmentPictureToCommunityPosts < ActiveRecord::Migration
  def self.up
    change_table :community_posts do |t|
      t.attachment :picture
    end
  end

  def self.down
    remove_attachment :community_posts, :picture
  end
end
