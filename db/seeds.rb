# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

Category.create(title: "Arts and Hummanities")
Category.create(title: "Business")
Category.create(title: "Computer Science")
Category.create(title: "Data Science")
Category.create(title: "Life Sciences")
Category.create(title: "Math and Logic")
Category.create(title: "Personal Development")
Category.create(title: "Physical Science and Engineering")
Category.create(title: "Social Sciences")

p "Categories created successfully!"

PostType.create(name: "--- Benchmark ---", benchmark_value: 0)
PostType.create(name: "Photo", benchmark_value: 1)
PostType.create(name: "Video", benchmark_value: 2)
PostType.create(name: "Presentation", benchmark_value: 3)
PostType.create(name: "Video Presentation", benchmark_value: 5)
PostType.create(name: "Group Work", benchmark_value: 5)
PostType.create(name: "Group Presentation", benchmark_value: 5)
PostType.create(name: "Public Speaking", benchmark_value: 8)
PostType.create(name: "Award", benchmark_value: 10)

p "Post Types created successfully!"