# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160128171602) do

  create_table "activities", force: :cascade do |t|
    t.integer  "trackable_id"
    t.string   "trackable_type"
    t.integer  "owner_id"
    t.string   "owner_type"
    t.string   "key"
    t.text     "parameters"
    t.integer  "recipient_id"
    t.string   "recipient_type"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "read",           default: false
  end

  add_index "activities", ["owner_id", "owner_type"], name: "index_activities_on_owner_id_and_owner_type"
  add_index "activities", ["recipient_id", "recipient_type"], name: "index_activities_on_recipient_id_and_recipient_type"
  add_index "activities", ["trackable_id", "trackable_type"], name: "index_activities_on_trackable_id_and_trackable_type"

  create_table "application_forms", force: :cascade do |t|
    t.string   "guid",                default: "3b03520-1767-4e64-8d"
    t.integer  "job_application_id"
    t.integer  "user_id"
    t.datetime "created_at",                                           null: false
    t.datetime "updated_at",                                           null: false
    t.string   "cv_doc_file_name"
    t.string   "cv_doc_content_type"
    t.integer  "cv_doc_file_size"
    t.datetime "cv_doc_updated_at"
    t.string   "cl_doc_file_name"
    t.string   "cl_doc_content_type"
    t.integer  "cl_doc_file_size"
    t.datetime "cl_doc_updated_at"
    t.text     "cv_description"
    t.text     "cl_description"
    t.boolean  "sharing"
    t.text     "about_yourself"
    t.text     "wswhy"
    t.text     "wmyqftp"
    t.text     "woothdyehtm"
    t.text     "wdydtsapitc"
    t.text     "wtottamityiyj"
    t.text     "hdywup"
  end

  add_index "application_forms", ["guid"], name: "index_application_forms_on_guid"
  add_index "application_forms", ["job_application_id"], name: "index_application_forms_on_job_application_id"
  add_index "application_forms", ["user_id"], name: "index_application_forms_on_user_id"

  create_table "benchmark_reports", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "points"
    t.integer  "month"
    t.integer  "year"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer  "average"
    t.integer  "maximum"
    t.string   "guid"
  end

  add_index "benchmark_reports", ["user_id"], name: "index_benchmark_reports_on_user_id"

  create_table "categories", force: :cascade do |t|
    t.string   "title"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string   "slug"
  end

  add_index "categories", ["slug"], name: "index_categories_on_slug", unique: true

  create_table "classifieds", force: :cascade do |t|
    t.string   "title"
    t.integer  "user_id"
    t.text     "description"
    t.float    "price"
    t.boolean  "availability"
    t.text     "notification_status"
    t.string   "transaction_id"
    t.string   "status"
    t.datetime "purchased_at"
    t.datetime "created_at",                        null: false
    t.datetime "updated_at",                        null: false
    t.string   "pic_file_name"
    t.string   "pic_content_type"
    t.integer  "pic_file_size"
    t.datetime "pic_updated_at"
    t.string   "slug"
    t.string   "cat"
    t.integer  "payment_price",       default: 149
  end

  add_index "classifieds", ["slug"], name: "index_classifieds_on_slug", unique: true
  add_index "classifieds", ["user_id"], name: "index_classifieds_on_user_id"

  create_table "comments", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "commentable_id"
    t.string   "commentable_type"
    t.text     "content"
    t.text     "content_html"
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
  end

  add_index "comments", ["commentable_type", "commentable_id"], name: "index_comments_on_commentable_type_and_commentable_id"
  add_index "comments", ["user_id"], name: "index_comments_on_user_id"

  create_table "community_posts", force: :cascade do |t|
    t.string   "name"
    t.text     "content"
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
    t.string   "picture_file_name"
    t.string   "picture_content_type"
    t.integer  "picture_file_size"
    t.datetime "picture_updated_at"
  end

  create_table "conversations", force: :cascade do |t|
    t.integer  "sender_id"
    t.integer  "recipient_id"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
  end

  add_index "conversations", ["recipient_id"], name: "index_conversations_on_recipient_id"
  add_index "conversations", ["sender_id"], name: "index_conversations_on_sender_id"

  create_table "course_syllabuses", force: :cascade do |t|
    t.integer  "course_id"
    t.integer  "syllabus_id"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "courses", force: :cascade do |t|
    t.string   "title"
    t.text     "description"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.string   "slug"
  end

  add_index "courses", ["slug"], name: "index_courses_on_slug", unique: true

  create_table "detailbooks", force: :cascade do |t|
    t.integer  "user_id"
    t.string   "name"
    t.string   "last_name"
    t.date     "dob"
    t.string   "gender"
    t.string   "city"
    t.string   "country"
    t.integer  "benchmark"
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
    t.string   "background_file_name"
    t.string   "background_content_type"
    t.integer  "background_file_size"
    t.datetime "background_updated_at"
    t.string   "avatar_file_name"
    t.string   "avatar_content_type"
    t.integer  "avatar_file_size"
    t.datetime "avatar_updated_at"
    t.string   "username"
  end

  add_index "detailbooks", ["user_id"], name: "index_detailbooks_on_user_id"

  create_table "detailbooks_skills", force: :cascade do |t|
    t.integer "detailbook_id"
    t.integer "skill_id"
  end

  create_table "educations", force: :cascade do |t|
    t.string   "guid"
    t.string   "school_institution"
    t.date     "date_start"
    t.date     "date_finish"
    t.string   "city"
    t.string   "country"
    t.string   "degree"
    t.string   "field_study"
    t.string   "grade"
    t.string   "activities_societies"
    t.text     "description"
    t.boolean  "done"
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
    t.integer  "user_id"
    t.string   "media_file_name"
    t.string   "media_content_type"
    t.integer  "media_file_size"
    t.datetime "media_updated_at"
    t.integer  "vitum_id"
  end

  add_index "educations", ["user_id"], name: "index_educations_on_user_id"
  add_index "educations", ["vitum_id"], name: "index_educations_on_vitum_id"

  create_table "events", force: :cascade do |t|
    t.string   "title"
    t.text     "description"
    t.datetime "start_time"
    t.datetime "end_time"
    t.string   "location"
    t.string   "city"
    t.string   "country"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.integer  "user_id"
    t.string   "category"
    t.string   "slug"
  end

  add_index "events", ["slug"], name: "index_events_on_slug", unique: true
  add_index "events", ["user_id"], name: "index_events_on_user_id"

  create_table "ex_files", force: :cascade do |t|
    t.integer  "user_id"
    t.string   "name"
    t.text     "description"
    t.integer  "category_id"
    t.string   "guid"
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
    t.string   "attachment_file_name"
    t.string   "attachment_content_type"
    t.integer  "attachment_file_size"
    t.datetime "attachment_updated_at"
  end

  add_index "ex_files", ["category_id"], name: "index_ex_files_on_category_id"
  add_index "ex_files", ["user_id"], name: "index_ex_files_on_user_id"

  create_table "exec_summaries", force: :cascade do |t|
    t.text     "description"
    t.datetime "created_at",                     null: false
    t.datetime "updated_at",                     null: false
    t.string   "media_attachement_file_name"
    t.string   "media_attachement_content_type"
    t.integer  "media_attachement_file_size"
    t.datetime "media_attachement_updated_at"
    t.integer  "vitum_id"
    t.string   "media_attachment_file_name"
    t.string   "media_attachment_content_type"
    t.integer  "media_attachment_file_size"
    t.datetime "media_attachment_updated_at"
  end

  add_index "exec_summaries", ["vitum_id"], name: "index_exec_summaries_on_vitum_id"

  create_table "folders", force: :cascade do |t|
    t.string   "guid"
    t.integer  "user_id"
    t.string   "name"
    t.text     "description"
    t.text     "sharable_to"
    t.string   "category"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  add_index "folders", ["guid"], name: "index_folders_on_guid", unique: true
  add_index "folders", ["user_id"], name: "index_folders_on_user_id"

  create_table "follows", force: :cascade do |t|
    t.integer  "followable_id",                   null: false
    t.string   "followable_type",                 null: false
    t.integer  "follower_id",                     null: false
    t.string   "follower_type",                   null: false
    t.boolean  "blocked",         default: false, null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "follows", ["followable_id", "followable_type"], name: "fk_followables"
  add_index "follows", ["follower_id", "follower_type"], name: "fk_follows"

  create_table "friendships", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "friend_id"
    t.string   "state",        default: "pending"
    t.datetime "friended_at"
    t.datetime "created_at",                       null: false
    t.datetime "updated_at",                       null: false
    t.integer  "likees_count", default: 0
  end

  add_index "friendships", ["user_id"], name: "index_friendships_on_user_id"

  create_table "group_memberships", force: :cascade do |t|
    t.integer  "member_id",       null: false
    t.string   "member_type",     null: false
    t.integer  "group_id"
    t.string   "group_type"
    t.string   "group_name"
    t.string   "membership_type"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "group_memberships", ["group_name"], name: "index_group_memberships_on_group_name"
  add_index "group_memberships", ["group_type", "group_id"], name: "index_group_memberships_on_group_type_and_group_id"
  add_index "group_memberships", ["member_type", "member_id"], name: "index_group_memberships_on_member_type_and_member_id"

  create_table "groups", force: :cascade do |t|
    t.string   "type"
    t.boolean  "isPublic"
    t.string   "title"
    t.text     "description"
    t.string   "tagline"
    t.string   "slug"
    t.string   "avatar_file_name"
    t.string   "avatar_content_type"
    t.integer  "avatar_file_size"
    t.datetime "avatar_updated_at"
    t.string   "background_file_name"
    t.string   "background_content_type"
    t.integer  "background_file_size"
    t.datetime "background_updated_at"
  end

  create_table "interest_activities", force: :cascade do |t|
    t.string   "name"
    t.text     "description"
    t.integer  "user_id"
    t.datetime "created_at",         null: false
    t.datetime "updated_at",         null: false
    t.string   "media_file_name"
    t.string   "media_content_type"
    t.integer  "media_file_size"
    t.datetime "media_updated_at"
    t.boolean  "done"
    t.integer  "vitum_id"
  end

  add_index "interest_activities", ["user_id"], name: "index_interest_activities_on_user_id"
  add_index "interest_activities", ["vitum_id"], name: "index_interest_activities_on_vitum_id"

  create_table "job_applications", force: :cascade do |t|
    t.string   "guid"
    t.string   "company_name"
    t.string   "industry"
    t.string   "job_title"
    t.string   "experience"
    t.string   "job_function"
    t.string   "employment_type"
    t.text     "job_description"
    t.string   "city"
    t.string   "country"
    t.boolean  "public"
    t.integer  "user_id"
    t.datetime "created_at",                         null: false
    t.datetime "updated_at",                         null: false
    t.string   "picture_file_name"
    t.string   "picture_content_type"
    t.integer  "picture_file_size"
    t.datetime "picture_updated_at"
    t.string   "cat"
    t.date     "deadline"
    t.string   "slug"
    t.integer  "payment_price",        default: 149
  end

  add_index "job_applications", ["guid"], name: "index_job_applications_on_guid", unique: true
  add_index "job_applications", ["slug"], name: "index_job_applications_on_slug", unique: true
  add_index "job_applications", ["user_id"], name: "index_job_applications_on_user_id"

  create_table "job_experiences", force: :cascade do |t|
    t.string   "guid"
    t.string   "title"
    t.string   "company_name"
    t.string   "city"
    t.string   "country"
    t.date     "date_start"
    t.date     "date_finish"
    t.boolean  "current_working"
    t.text     "description"
    t.boolean  "done"
    t.datetime "created_at",         null: false
    t.datetime "updated_at",         null: false
    t.integer  "user_id"
    t.string   "media_file_name"
    t.string   "media_content_type"
    t.integer  "media_file_size"
    t.datetime "media_updated_at"
    t.integer  "vitum_id"
  end

  add_index "job_experiences", ["user_id"], name: "index_job_experiences_on_user_id"
  add_index "job_experiences", ["vitum_id"], name: "index_job_experiences_on_vitum_id"

  create_table "lectures", force: :cascade do |t|
    t.string   "title"
    t.text     "description"
    t.integer  "syllabus_id"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  add_index "lectures", ["syllabus_id"], name: "index_lectures_on_syllabus_id"

  create_table "likes", force: :cascade do |t|
    t.string   "liker_type"
    t.integer  "liker_id"
    t.string   "likeable_type"
    t.integer  "likeable_id"
    t.datetime "created_at"
  end

  add_index "likes", ["likeable_id", "likeable_type"], name: "fk_likeables"
  add_index "likes", ["liker_id", "liker_type"], name: "fk_likes"

  create_table "mailboxer_conversation_opt_outs", force: :cascade do |t|
    t.integer "unsubscriber_id"
    t.string  "unsubscriber_type"
    t.integer "conversation_id"
  end

  add_index "mailboxer_conversation_opt_outs", ["conversation_id"], name: "index_mailboxer_conversation_opt_outs_on_conversation_id"
  add_index "mailboxer_conversation_opt_outs", ["unsubscriber_id", "unsubscriber_type"], name: "index_mailboxer_conversation_opt_outs_on_unsubscriber_id_type"

  create_table "mailboxer_conversations", force: :cascade do |t|
    t.string   "subject",    default: ""
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
  end

  create_table "mailboxer_notifications", force: :cascade do |t|
    t.string   "type"
    t.text     "body"
    t.string   "subject",              default: ""
    t.integer  "sender_id"
    t.string   "sender_type"
    t.integer  "conversation_id"
    t.boolean  "draft",                default: false
    t.string   "notification_code"
    t.integer  "notified_object_id"
    t.string   "notified_object_type"
    t.string   "attachment"
    t.datetime "updated_at",                           null: false
    t.datetime "created_at",                           null: false
    t.boolean  "global",               default: false
    t.datetime "expires"
  end

  add_index "mailboxer_notifications", ["conversation_id"], name: "index_mailboxer_notifications_on_conversation_id"
  add_index "mailboxer_notifications", ["notified_object_id", "notified_object_type"], name: "index_mailboxer_notifications_on_notified_object_id_and_type"
  add_index "mailboxer_notifications", ["sender_id", "sender_type"], name: "index_mailboxer_notifications_on_sender_id_and_sender_type"
  add_index "mailboxer_notifications", ["type"], name: "index_mailboxer_notifications_on_type"

  create_table "mailboxer_receipts", force: :cascade do |t|
    t.integer  "receiver_id"
    t.string   "receiver_type"
    t.integer  "notification_id",                            null: false
    t.boolean  "is_read",                    default: false
    t.boolean  "trashed",                    default: false
    t.boolean  "deleted",                    default: false
    t.string   "mailbox_type",    limit: 25
    t.datetime "created_at",                                 null: false
    t.datetime "updated_at",                                 null: false
  end

  add_index "mailboxer_receipts", ["notification_id"], name: "index_mailboxer_receipts_on_notification_id"
  add_index "mailboxer_receipts", ["receiver_id", "receiver_type"], name: "index_mailboxer_receipts_on_receiver_id_and_receiver_type"

  create_table "media", force: :cascade do |t|
    t.integer  "user_id"
    t.text     "description"
    t.integer  "post_id"
    t.datetime "created_at",                    null: false
    t.datetime "updated_at",                    null: false
    t.string   "media_attachment_file_name"
    t.string   "media_attachment_content_type"
    t.integer  "media_attachment_file_size"
    t.datetime "media_attachment_updated_at"
    t.string   "guid"
    t.boolean  "done"
  end

  add_index "media", ["guid"], name: "index_media_on_guid", unique: true
  add_index "media", ["post_id"], name: "index_media_on_post_id"
  add_index "media", ["user_id"], name: "index_media_on_user_id"

  create_table "members", force: :cascade do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "members", ["email"], name: "index_members_on_email", unique: true
  add_index "members", ["reset_password_token"], name: "index_members_on_reset_password_token", unique: true

  create_table "mentions", force: :cascade do |t|
    t.string   "mentioner_type"
    t.integer  "mentioner_id"
    t.string   "mentionable_type"
    t.integer  "mentionable_id"
    t.datetime "created_at"
  end

  add_index "mentions", ["mentionable_id", "mentionable_type"], name: "fk_mentionables"
  add_index "mentions", ["mentioner_id", "mentioner_type"], name: "fk_mentions"

  create_table "messages", force: :cascade do |t|
    t.text     "body"
    t.integer  "conversation_id"
    t.integer  "user_id"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
  end

  add_index "messages", ["conversation_id"], name: "index_messages_on_conversation_id"
  add_index "messages", ["user_id"], name: "index_messages_on_user_id"

  create_table "online_learnings", force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "poll_votes", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "vote_option_id"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
  end

  add_index "poll_votes", ["user_id"], name: "index_poll_votes_on_user_id"
  add_index "poll_votes", ["vote_option_id", "user_id"], name: "index_poll_votes_on_vote_option_id_and_user_id", unique: true
  add_index "poll_votes", ["vote_option_id"], name: "index_poll_votes_on_vote_option_id"

  create_table "polls", force: :cascade do |t|
    t.text     "topic"
    t.integer  "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string   "guid"
  end

  add_index "polls", ["guid"], name: "index_polls_on_guid", unique: true
  add_index "polls", ["user_id"], name: "index_polls_on_user_id"

  create_table "post_types", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
    t.integer  "benchmark_value"
  end

  create_table "posts", force: :cascade do |t|
    t.integer  "user_id"
    t.text     "content"
    t.text     "content_html"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
    t.string   "attachment_file_name"
    t.string   "attachment_content_type"
    t.integer  "attachment_file_size"
    t.datetime "attachment_updated_at"
    t.string   "select_option"
    t.integer  "post_type_id"
    t.integer  "likees_count",            default: 0
    t.integer  "postable_id"
    t.string   "postable_type"
  end

  add_index "posts", ["postable_type", "postable_id"], name: "index_posts_on_postable_type_and_postable_id"

  create_table "roles", force: :cascade do |t|
    t.integer  "user_id"
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "roles", ["user_id"], name: "index_roles_on_user_id"

  create_table "sales", force: :cascade do |t|
    t.string   "buyer_email"
    t.string   "seller_email"
    t.integer  "amount"
    t.string   "guid"
    t.integer  "sellable_id"
    t.string   "sellable_type"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
  end

  add_index "sales", ["sellable_type", "sellable_id"], name: "index_sales_on_sellable_type_and_sellable_id"

  create_table "simple_hashtag_hashtaggings", force: :cascade do |t|
    t.integer "hashtag_id"
    t.integer "hashtaggable_id"
    t.string  "hashtaggable_type"
  end

  add_index "simple_hashtag_hashtaggings", ["hashtag_id"], name: "index_simple_hashtag_hashtaggings_on_hashtag_id"
  add_index "simple_hashtag_hashtaggings", ["hashtaggable_id", "hashtaggable_type"], name: "index_hashtaggings_hashtaggable_id_hashtaggable_type"

  create_table "simple_hashtag_hashtags", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "simple_hashtag_hashtags", ["name"], name: "index_simple_hashtag_hashtags_on_name"

  create_table "skills", force: :cascade do |t|
    t.string   "name"
    t.boolean  "done"
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
    t.integer  "user_id"
    t.string   "skill_format_file_name"
    t.string   "skill_format_content_type"
    t.integer  "skill_format_file_size"
    t.datetime "skill_format_updated_at"
    t.integer  "vitum_id"
  end

  add_index "skills", ["user_id"], name: "index_skills_on_user_id"
  add_index "skills", ["vitum_id"], name: "index_skills_on_vitum_id"

  create_table "skills_users", force: :cascade do |t|
    t.integer "skill_id"
    t.integer "user_id"
  end

  create_table "syllabuses", force: :cascade do |t|
    t.string   "title"
    t.text     "description"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.string   "slug"
  end

  add_index "syllabuses", ["slug"], name: "index_syllabuses_on_slug", unique: true

  create_table "tasks", force: :cascade do |t|
    t.string   "title"
    t.integer  "priority"
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
    t.integer  "user_id"
    t.boolean  "checked",    default: false
  end

  add_index "tasks", ["user_id"], name: "index_tasks_on_user_id"

  create_table "teaching_availabilities", force: :cascade do |t|
    t.integer  "user_id"
    t.string   "guid"
    t.datetime "start_time"
    t.datetime "end_time"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.boolean  "done"
  end

  add_index "teaching_availabilities", ["user_id"], name: "index_teaching_availabilities_on_user_id"

  create_table "todo_rails_tasks", force: :cascade do |t|
    t.string   "title"
    t.boolean  "completed"
    t.integer  "row_order"
    t.integer  "priority"
    t.text     "comment"
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
    t.boolean  "archieved",  default: false, null: false
  end

  create_table "todo_tasks", force: :cascade do |t|
    t.string   "title"
    t.boolean  "completed"
    t.integer  "row_order"
    t.integer  "priority"
    t.text     "comment"
    t.boolean  "archived",   default: false, null: false
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
    t.integer  "user_id"
  end

  add_index "todo_tasks", ["user_id"], name: "index_todo_tasks_on_user_id"

  create_table "users", force: :cascade do |t|
    t.string   "username",                default: "",   null: false
    t.string   "email",                   default: "",   null: false
    t.string   "encrypted_password",      default: "",   null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",           default: 0,    null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.string   "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string   "unconfirmed_email"
    t.integer  "failed_attempts",         default: 0,    null: false
    t.string   "unlock_token"
    t.datetime "locked_at"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "getting_started",         default: true
    t.string   "role"
    t.string   "first_name"
    t.string   "last_name"
    t.text     "bio"
    t.date     "dob"
    t.string   "gender"
    t.string   "full_name"
    t.string   "university"
    t.string   "country"
    t.string   "city"
    t.string   "ucas_code"
    t.string   "address1"
    t.string   "address2"
    t.string   "address3"
    t.string   "post_code"
    t.integer  "number_of_employees"
    t.string   "company_number"
    t.string   "industry"
    t.string   "info_email"
    t.string   "contact_email"
    t.string   "phone_number"
    t.string   "website_link"
    t.integer  "years_experience"
    t.string   "uni_graduated_at"
    t.string   "degree_type"
    t.string   "profession"
    t.string   "course"
    t.string   "year_uni"
    t.boolean  "publish_contact_info"
    t.string   "avatar_file_name"
    t.string   "avatar_content_type"
    t.integer  "avatar_file_size"
    t.datetime "avatar_updated_at"
    t.string   "background_file_name"
    t.string   "background_content_type"
    t.integer  "background_file_size"
    t.datetime "background_updated_at"
    t.boolean  "toc"
    t.boolean  "nsfw"
    t.boolean  "teach_online"
    t.string   "skype"
    t.integer  "benchmark",               default: 40
    t.text     "cv_description"
    t.text     "cl_description"
    t.string   "cv_doc_file_name"
    t.string   "cv_doc_content_type"
    t.integer  "cv_doc_file_size"
    t.datetime "cv_doc_updated_at"
    t.string   "cl_doc_file_name"
    t.string   "cl_doc_content_type"
    t.integer  "cl_doc_file_size"
    t.datetime "cl_doc_updated_at"
  end

  add_index "users", ["confirmation_token"], name: "index_users_on_confirmation_token", unique: true
  add_index "users", ["email"], name: "index_users_on_email", unique: true
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  add_index "users", ["unlock_token"], name: "index_users_on_unlock_token", unique: true
  add_index "users", ["username"], name: "index_users_on_username", unique: true

  create_table "videos", force: :cascade do |t|
    t.string   "title"
    t.text     "description"
    t.integer  "lecture_id"
    t.datetime "created_at",                       null: false
    t.datetime "updated_at",                       null: false
    t.integer  "category_id"
    t.string   "content_file_name"
    t.string   "content_content_type"
    t.integer  "content_file_size"
    t.datetime "content_updated_at"
    t.integer  "view_count",           default: 0
    t.string   "guid"
  end

  add_index "videos", ["category_id"], name: "index_videos_on_category_id"
  add_index "videos", ["guid"], name: "index_videos_on_guid", unique: true
  add_index "videos", ["lecture_id"], name: "index_videos_on_lecture_id"

  create_table "vita", force: :cascade do |t|
    t.integer  "user_id"
    t.string   "title"
    t.string   "guid"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "vita", ["user_id"], name: "index_vita_on_user_id"

  create_table "vote_options", force: :cascade do |t|
    t.string   "title"
    t.integer  "poll_id"
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
    t.integer  "votes_count", default: 0, null: false
  end

  add_index "vote_options", ["poll_id"], name: "index_vote_options_on_poll_id"

  create_table "votes", force: :cascade do |t|
    t.integer  "votable_id"
    t.string   "votable_type"
    t.integer  "voter_id"
    t.string   "voter_type"
    t.boolean  "vote_flag"
    t.string   "vote_scope"
    t.integer  "vote_weight"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "votes", ["votable_id", "votable_type", "vote_scope"], name: "index_votes_on_votable_id_and_votable_type_and_vote_scope"
  add_index "votes", ["voter_id", "voter_type", "vote_scope"], name: "index_votes_on_voter_id_and_voter_type_and_vote_scope"

  create_table "yolos", force: :cascade do |t|
    t.integer  "user_id"
    t.text     "content"
    t.text     "content_html"
    t.datetime "created_at",                     null: false
    t.datetime "updated_at",                     null: false
    t.string   "media_file_name"
    t.string   "media_content_type"
    t.integer  "media_file_size"
    t.datetime "media_updated_at"
    t.string   "guid"
    t.integer  "likees_count",       default: 0
  end

  add_index "yolos", ["user_id"], name: "index_yolos_on_user_id"

end
