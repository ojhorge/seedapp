source 'https://rubygems.org'

ruby '2.2.2'
# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem 'rails', '4.2.4'
# Use SCSS for stylesheets
gem 'sass-rails', '~> 5.0'
gem 'compass-rails', '~> 2.0.4'
# Use Uglifier as compressor for JavaScript assets
gem 'uglifier', '>= 1.3.0'
# Use CoffeeScript for .coffee assets and views
gem 'coffee-rails', '~> 4.1.0'
# See https://github.com/rails/execjs#readme for more supported runtimes
gem 'therubyracer', platforms: :ruby

# Extra stuff
gem 'eventmachine'
# Use jquery as the JavaScript library
gem 'jquery-rails'
# Turbolinks makes following links in your web application faster. Read more: https://github.com/rails/turbolinks
gem 'turbolinks'
gem 'jquery-turbolinks'
# Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
gem 'jbuilder', '~> 2.0'
# bundle exec rake doc:rails generates the API under doc/api.
gem 'sdoc', '~> 0.4.0', group: :doc

# Use ActiveModel has_secure_password
gem 'bcrypt', '~> 3.1.7'

# Bootstrap
# gem 'bootstrap-sass', '~> 3.3.5'
# gem 'sass-rails', '>= 3.2'
gem 'twitter-bootstrap-rails', :git => 'git://github.com/seyhunak/twitter-bootstrap-rails.git'

# HAML
gem "haml"
gem "haml-rails", "~> 0.9"

# Useful Gems
gem 'simple_form'
gem "country_select"

# gem "refile", require: "refile/rails"
# gem "refile-mini_magick"
# gem 'carrierwave', github: 'carrierwaveuploader/carrierwave'
gem 'paperclip', :git => "git://github.com/thoughtbot/paperclip.git"
gem 'paperclip-av-transcoder'
# gem 'letter_opener'
gem 'devise'
# gem 'cancancan'

gem 'gravatarify', '~> 3.0.0'
gem 'cocoon'

# GUID generation
gem "uuid", "2.3.7"

# Icons
gem "entypo-rails"

# gem "gritter", "1.2.0" (Not working at the moment)

gem 'better_errors'

gem 'public_activity'

gem 'auto_html', '~> 1.6.4'

gem 'rack-timeout'

# Video players
# gem "mediaelement_rails"
gem 'videojs_rails'

# gem "stream_rails", '~> 2.2'

# gem 'cells', '~> 4.0.2'

# Admin Stuff
gem "rails_admin"

# Events
gem 'fullcalendar-rails'
gem 'momentjs-rails'

gem 'datetimepicker-rails', github: 'zpaulovics/datetimepicker-rails', branch: 'master', submodules: true

# Use Capistrano for deployment
# gem 'capistrano-rails', group: :development

# Beautiful URLs
gem 'friendly_id', '~> 5.1.0'

# Social Network Gems
gem "socialization"
# Comments
# gem 'acts_as_commentable'
# gem 'acts_as_commentable_with_threading'
# gem 'awesome_nested_set'

# gem 'acts_as_nested_set'
# Likes
gem 'acts_as_votable', '~> 0.10.0'

# Chat
gem 'private_pub'
gem 'thin'

# Todo list
gem 'pry'

# Sidebar for Chat and ToDoLists
# gem 'less-rails-semantic_ui', '~> 2.1.4'
# gem 'autoprefixer-rails'
# gem 'semantic-ui-sass', github: 'doabit/semantic-ui-sass'


# Advanced Search
# gem 'searchkick'

gem 'rack-contrib'
gem 'soulmate', :require => 'soulmate/server'

# Chart for Benchmark
# gem 'chart-js-rails'
gem 'chartjs-ror'

# Hashtags for Posts
gem 'simple_hashtag'


#Poll Caching (Performance)
gem 'counter_culture', '~> 0.1.23'

# PDF Viewer
gem 'pdfjs_rails'

# OmniAuth and Social Networks for Community Members
gem 'omniauth'
# gem 'omniauth-twitter'
# gem 'omniauth-facebook'
# gem 'omniauth-instagram'
# gem 'twitter'
# gem 'instagram'
gem 'omniauth-google-oauth2'
gem 'google-api-client', require: 'google/api_client'

# Process Scheduling for Benchmark and others
gem 'whenever', require: false

# Rails <=> Javascript
gem 'gon'
gem 'rabl'
# Also add either `oj` or `yajl-ruby` as the JSON parser
gem 'oj'

# Font Awesome
gem 'font-awesome-rails'

# gem 'todo_rails'

# extracting dependency gems from the 'todo_rails' gem
gem 'bower-rails', "~> 0.10.0"

# Payments
# gem 'braintree'
gem 'stripe'

# Private Inbox System
gem 'mailboxer'
# w/ enhancements
gem 'chosen-rails'

# Following System
# gem "acts_as_follower"

# Modernizr
gem 'modernizr-rails'

# For groups and memberships
gem 'groupify'

# Rename the application
gem 'rename'

# gem 'redis-server'

group :development, :test do
  # Call 'byebug' anywhere in the code to stop execution and get a debugger console
  gem 'byebug'

  # Testing
  gem 'rspec-rails'
  gem 'factory_girl_rails'

  # Debugging
  gem 'pry-rails'

  # Access an IRB console on exception pages or by using <%= console %> in views
  gem 'web-console', '~> 2.0'
  # Use sqlite3 as the database for Active Record
  gem 'sqlite3'
  # Spring speeds up development by keeping your application running in the background. Read more: https://github.com/rails/spring
  gem 'spring'

  # Environment Variable Manager
  gem 'dotenv-rails'
  # gem 'dotenv-rails', groups: [:development]

 end

group :production do
  gem 'pg'
  gem 'rails_12factor'
  # Use Unicorn as the app server
  gem 'unicorn'

end

group :development do
  gem 'capistrano', '~> 3.4.0'
  gem 'capistrano-bundler', '~> 1.1.3'
  gem 'capistrano-rails', '~> 1.1.1'

  # Add this if you're using rbenv
  gem 'capistrano-rbenv', github: "capistrano/rbenv"
  gem 'capistrano-passenger'
  # Add this if you're using rvm
  # gem 'capistrano-rvm', github: "capistrano/rvm"


end
