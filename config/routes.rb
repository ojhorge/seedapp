Rails.application.routes.draw do
  resources :vita
  resources :yolos do
    resources :comments, module: :yolos
    member do
      put "like", to: "yolos#upvote"
      put "dislike", to: "yolos#downvote"
    end
  end
  # get 'follows/follow'

  # get 'follows/unfollow'

  # devise_for :members, controllers: {registrations: 'member_registrations'}
  get "hashtags/:hashtag",   to: "hashtags#show",      as: :hashtag
  get "hashtags",            to: "hashtags#index",     as: :hashtags
  # get 'benchmark_reports/index'

  # get 'benchmark_reports/show'

  # get 'benchmark_reports/create'

  # get 'benchmark_reports/new'

  mount Soulmate::Server, :at => "/autocomplete"

  # mount TodoRails::Engine => "/todos", as: 'todo'

  devise_for :users, controllers: {registrations: 'user_registrations'}

  get 'conversations/show'

  get 'conversations/create'

  resources :application_forms
  resources :media
  resources :groups do
    resources :posts, module: :groups
    member do
      get :join
      get :unfollow
    end
  end


  # get  '/buy/:slug', to: 'transactions#new', as: :show_buy
  post '/buy/:slug', to: 'transactions#create', as: :buy
  # get '/pickup/:guid', to: 'transactions#pickup', as: :pickup

  # resources :groups, only: [:show], shallow: true do
  #   resources :memberships, only: [:new, :create] #-> domain.com/2/memberships/new
  # end
  resources :online_learnings
  resources :categories
  resources :videos
  resources :lectures
  resources :syllabuses
  resources :courses
  resources :polls
  resources :poll_votes, only: [:create]
  resources :community_posts

  mount RailsAdmin::Engine => '/admin', as: 'rails_admin'
  
  resources :events do
    resources :posts, module: :events
  end

  get 'notifications/' => "notifications#index"

  resources :classifieds do
    resources :transactions, only: [:purchase, :create, :pickup], module: :classifieds
  end

  post "/hook" => "classifieds#hook"
  
  get "/classifieds/:id/purchase", to: "classifieds#purchase", as: :purchase_classified


  resources :posts, only: [:create, :edit, :update, :destroy] do
    resources :comments, module: :posts
    member do
      put "like", to: "posts#upvote"
      put "dislike", to: "posts#downvote"
    end
  end

  resources :activities, only: [:index] do
    # resources :posts, module: :activities
    # resources :comments, module: :activities
    member do
      put "like", to: "activities#upvote"
      put "dislike", to: "activities#downvote"
    end
  end

  resources :job_applications do
    resources :transactions, only: [:purchase, :create, :pickup], module: :job_applications
  end
  
  root 'pages#home'

  post "/activity_create" => "posts#create_from_activity"


  get "/about" => "pages#about"
  get "/community" => "pages#community"
  get "/contact" => "pages#contact"
  get "/copyright" => "pages#copyright"
  get "/privacy" => "pages#privacy"
  get "/terms" => "pages#terms"
  get "/toc" => "pages#toc"
  get "/cookies" => "pages#cpolicy"

  get "/find_university" => "pages#find_university"
  get "/find_tutor" => "pages#find_tutor"

  get '/users/:id/job_applications', to: 'job_applications#job_applications', as: 'user_job_applications'

  
  resources :users, only: [:show, :index]

  # resources :tasks do
  #   put :sort, on: :collection
  # end

  resources :detailbooks do
    resources :educations
    resources :job_experiences
  end
  resources :users do
    resources :posts, module: :users
    resources :educations
    resources :job_experiences
    resources :benchmark_reports, only: [:index, :show, :create, :new]
    resources :ex_files
    resources :skills
    resources :todo_tasks 
    resources :vita, only: [:index]
    resources :interest_activities
  end
  resources :skills
  resources :todo_tasks

  
  resources :friendships, only: [:create, :destroy, :accept] do 
    resources :comments, module: :friendships
    member do 
      put :accept
      put "like", to: "friendships#upvote"
      put "dislike", to: "friendships#downvote"
    end
  end

  resources :follows, only: [:follow, :unfollow] do
    member do
      get "follow"
      get "unfollow"
    end
  end

  resources :comments do
    member do
      put "like", to: "comments#upvote"
      put "dislike", to: "comments#downvote"
    end
  end

  # mailbox folder routes
  get "mailbox/inbox" => "mailbox#inbox", as: :mailbox_inbox
  get "mailbox/sent" => "mailbox#sent", as: :mailbox_sent
  get "mailbox/trash" => "mailbox#trash", as: :mailbox_trash

  # conversations
  resources :conversations do
    member do
      post :reply
      post :trash
      post :untrash
    end
  end

  # get 'pages/about'
  # get 'pages/community'
  # get 'pages/contact'
  # get 'pages/copyright'
  # get 'pages/privacy'
  # get 'pages/terms'

  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  # root 'welcome#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
