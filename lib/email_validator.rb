# class for the email validation
class EmailValidator <  ActiveModel::Validator
  # email validation for student, university and tutor  
  def validate(value)
    if value.role == 's'
      value.errors[:email] << "This is not your students' email" unless
      value.email =~ /^(\S+)@(\S+).ac.uk$/
    elsif value.role == 't'
      value.errors[:email] << "This email not a tutors' email" unless
      value.email =~ /^(\S+)@(\S+).ac.uk$/
    elsif value.role == 'u'
      value.errors[:email] << "This is not a university email" unless
      value.email =~ /^(\S+)@(\S+).ac.uk$/                      
    elsif value.role == 'b'
      value.errors[:email] << "This is not email" unless
      value.email =~  /.+@.+\..+/i
    end
  end
end