desc "make_benchmark_reports"
task make_benchmark_reports: :environment do
	# every month on the last day at 23:59:59
	students = User.all.where(role: "s") # Gets all students
	max = students.order("benchmark desc").maximum(:benchmark).to_i # Gets the highest benchmark user
	avg = students.order("benchmark desc").average(:benchmark).to_i # Gets the average benchmark from all students

	students.each do |student| # For every student ...
		BenchmarkReport.create(
			user_id: student.id, 
			points: student.benchmark, 
			month: Time.now().month, 
			year: Time.now().year,
			average: avg,
			maximum: max
		) # ... create a benchmark report!
	end
end
