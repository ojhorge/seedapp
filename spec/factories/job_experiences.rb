FactoryGirl.define do
  factory :job_experience do
    guid "MyString"
title "MyString"
company_name "MyString"
city "MyString"
country "MyString"
date_start "2015-09-05"
date_finish "2015-09-05"
current_working false
description "MyText"
done false
detailbook nil
  end

end
