FactoryGirl.define do
  factory :job_application do
    guid "MyString"
company_name "MyString"
industry "MyString"
job_title "MyString"
experience "MyString"
job_function "MyString"
employment_type "MyString"
job_description "MyText"
city "MyString"
country "MyString"
public false
user nil
  end

end
