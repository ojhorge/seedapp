FactoryGirl.define do
  factory :education do
    guid "MyString"
school_institution "MyString"
date_start "2015-09-05"
date_finish "2015-09-05"
city "MyString"
country "MyString"
degree "MyString"
field_study "MyString"
grade "MyString"
activities_societies "MyString"
description "MyText"
done false
detailbook nil
  end

end
