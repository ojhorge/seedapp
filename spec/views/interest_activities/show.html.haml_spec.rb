require 'rails_helper'

RSpec.describe "interest_activities/show", type: :view do
  before(:each) do
    @interest_activity = assign(:interest_activity, InterestActivity.create!(
      :name => "Name",
      :description => "MyText",
      :user => nil
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/Name/)
    expect(rendered).to match(/MyText/)
    expect(rendered).to match(//)
  end
end
