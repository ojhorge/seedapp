require 'rails_helper'

RSpec.describe "interest_activities/edit", type: :view do
  before(:each) do
    @interest_activity = assign(:interest_activity, InterestActivity.create!(
      :name => "MyString",
      :description => "MyText",
      :user => nil
    ))
  end

  it "renders the edit interest_activity form" do
    render

    assert_select "form[action=?][method=?]", interest_activity_path(@interest_activity), "post" do

      assert_select "input#interest_activity_name[name=?]", "interest_activity[name]"

      assert_select "textarea#interest_activity_description[name=?]", "interest_activity[description]"

      assert_select "input#interest_activity_user_id[name=?]", "interest_activity[user_id]"
    end
  end
end
