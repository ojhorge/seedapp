require 'rails_helper'

RSpec.describe "interest_activities/index", type: :view do
  before(:each) do
    assign(:interest_activities, [
      InterestActivity.create!(
        :name => "Name",
        :description => "MyText",
        :user => nil
      ),
      InterestActivity.create!(
        :name => "Name",
        :description => "MyText",
        :user => nil
      )
    ])
  end

  it "renders a list of interest_activities" do
    render
    assert_select "tr>td", :text => "Name".to_s, :count => 2
    assert_select "tr>td", :text => "MyText".to_s, :count => 2
    assert_select "tr>td", :text => nil.to_s, :count => 2
  end
end
