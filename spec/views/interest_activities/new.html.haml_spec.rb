require 'rails_helper'

RSpec.describe "interest_activities/new", type: :view do
  before(:each) do
    assign(:interest_activity, InterestActivity.new(
      :name => "MyString",
      :description => "MyText",
      :user => nil
    ))
  end

  it "renders new interest_activity form" do
    render

    assert_select "form[action=?][method=?]", interest_activities_path, "post" do

      assert_select "input#interest_activity_name[name=?]", "interest_activity[name]"

      assert_select "textarea#interest_activity_description[name=?]", "interest_activity[description]"

      assert_select "input#interest_activity_user_id[name=?]", "interest_activity[user_id]"
    end
  end
end
