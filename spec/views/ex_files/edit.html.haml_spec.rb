require 'rails_helper'

RSpec.describe "ex_files/edit", type: :view do
  before(:each) do
    @ex_file = assign(:ex_file, ExFile.create!(
      :user => nil,
      :name => "MyString",
      :description => "MyText",
      :category => nil,
      :guid => "MyString"
    ))
  end

  it "renders the edit ex_file form" do
    render

    assert_select "form[action=?][method=?]", user_ex_file_path(@user.username, @ex_file.guid), "post" do

      assert_select "input#ex_file_user_id[name=?]", "ex_file[user_id]"

      assert_select "input#ex_file_name[name=?]", "ex_file[name]"

      assert_select "textarea#ex_file_description[name=?]", "ex_file[description]"

      assert_select "input#ex_file_category_id[name=?]", "ex_file[category_id]"

      assert_select "input#ex_file_guid[name=?]", "ex_file[guid]"
    end
  end
end
