require 'rails_helper'

RSpec.describe "ex_files/show", type: :view do
  before(:each) do
    @ex_file = assign(:ex_file, ExFile.create!(
      :user => nil,
      :name => "Name",
      :description => "MyText",
      :category => nil,
      :guid => "Guid"
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(//)
    expect(rendered).to match(/Name/)
    expect(rendered).to match(/MyText/)
    expect(rendered).to match(//)
    expect(rendered).to match(/Guid/)
  end
end
