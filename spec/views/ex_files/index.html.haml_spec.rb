require 'rails_helper'

RSpec.describe "ex_files/index", type: :view do
  before(:each) do
    assign(:ex_files, [
      ExFile.create!(
        :user => nil,
        :name => "Name",
        :description => "MyText",
        :category => nil,
        :guid => "Guid"
      ),
      ExFile.create!(
        :user => nil,
        :name => "Name",
        :description => "MyText",
        :category => nil,
        :guid => "Guid"
      )
    ])
  end

  it "renders a list of ex_files" do
    render
    assert_select "tr>td", :text => nil.to_s, :count => 2
    assert_select "tr>td", :text => "Name".to_s, :count => 2
    assert_select "tr>td", :text => "MyText".to_s, :count => 2
    assert_select "tr>td", :text => nil.to_s, :count => 2
    assert_select "tr>td", :text => "Guid".to_s, :count => 2
  end
end
