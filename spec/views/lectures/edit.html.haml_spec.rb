require 'rails_helper'

RSpec.describe "lectures/edit", type: :view do
  before(:each) do
    @lecture = assign(:lecture, Lecture.create!(
      :title => "MyString",
      :description => "MyText",
      :syllabus => nil
    ))
  end

  it "renders the edit lecture form" do
    render

    assert_select "form[action=?][method=?]", lecture_path(@lecture), "post" do

      assert_select "input#lecture_title[name=?]", "lecture[title]"

      assert_select "textarea#lecture_description[name=?]", "lecture[description]"

      assert_select "input#lecture_syllabus_id[name=?]", "lecture[syllabus_id]"
    end
  end
end
