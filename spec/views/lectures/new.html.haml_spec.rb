require 'rails_helper'

RSpec.describe "lectures/new", type: :view do
  before(:each) do
    assign(:lecture, Lecture.new(
      :title => "MyString",
      :description => "MyText",
      :syllabus => nil
    ))
  end

  it "renders new lecture form" do
    render

    assert_select "form[action=?][method=?]", lectures_path, "post" do

      assert_select "input#lecture_title[name=?]", "lecture[title]"

      assert_select "textarea#lecture_description[name=?]", "lecture[description]"

      assert_select "input#lecture_syllabus_id[name=?]", "lecture[syllabus_id]"
    end
  end
end
