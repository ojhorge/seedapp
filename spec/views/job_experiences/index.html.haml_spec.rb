require 'rails_helper'

RSpec.describe "job_experiences/index", type: :view do
  before(:each) do
    assign(:job_experiences, [
      JobExperience.create!(
        :guid => "Guid",
        :title => "Title",
        :company_name => "Company Name",
        :city => "City",
        :country => "Country",
        :current_working => false,
        :description => "MyText",
        :done => false,
        :detailbook => nil
      ),
      JobExperience.create!(
        :guid => "Guid",
        :title => "Title",
        :company_name => "Company Name",
        :city => "City",
        :country => "Country",
        :current_working => false,
        :description => "MyText",
        :done => false,
        :detailbook => nil
      )
    ])
  end

  it "renders a list of job_experiences" do
    render
    assert_select "tr>td", :text => "Guid".to_s, :count => 2
    assert_select "tr>td", :text => "Title".to_s, :count => 2
    assert_select "tr>td", :text => "Company Name".to_s, :count => 2
    assert_select "tr>td", :text => "City".to_s, :count => 2
    assert_select "tr>td", :text => "Country".to_s, :count => 2
    assert_select "tr>td", :text => false.to_s, :count => 2
    assert_select "tr>td", :text => "MyText".to_s, :count => 2
    assert_select "tr>td", :text => false.to_s, :count => 2
    assert_select "tr>td", :text => nil.to_s, :count => 2
  end
end
