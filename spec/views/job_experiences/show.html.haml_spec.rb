require 'rails_helper'

RSpec.describe "job_experiences/show", type: :view do
  before(:each) do
    @job_experience = assign(:job_experience, JobExperience.create!(
      :guid => "Guid",
      :title => "Title",
      :company_name => "Company Name",
      :city => "City",
      :country => "Country",
      :current_working => false,
      :description => "MyText",
      :done => false,
      :detailbook => nil
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/Guid/)
    expect(rendered).to match(/Title/)
    expect(rendered).to match(/Company Name/)
    expect(rendered).to match(/City/)
    expect(rendered).to match(/Country/)
    expect(rendered).to match(/false/)
    expect(rendered).to match(/MyText/)
    expect(rendered).to match(/false/)
    expect(rendered).to match(//)
  end
end
