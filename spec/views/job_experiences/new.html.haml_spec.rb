require 'rails_helper'

RSpec.describe "job_experiences/new", type: :view do
  before(:each) do
    assign(:job_experience, JobExperience.new(
      :guid => "MyString",
      :title => "MyString",
      :company_name => "MyString",
      :city => "MyString",
      :country => "MyString",
      :current_working => false,
      :description => "MyText",
      :done => false,
      :detailbook => nil
    ))
  end

  it "renders new job_experience form" do
    render

    assert_select "form[action=?][method=?]", job_experiences_path, "post" do

      assert_select "input#job_experience_guid[name=?]", "job_experience[guid]"

      assert_select "input#job_experience_title[name=?]", "job_experience[title]"

      assert_select "input#job_experience_company_name[name=?]", "job_experience[company_name]"

      assert_select "input#job_experience_city[name=?]", "job_experience[city]"

      assert_select "input#job_experience_country[name=?]", "job_experience[country]"

      assert_select "input#job_experience_current_working[name=?]", "job_experience[current_working]"

      assert_select "textarea#job_experience_description[name=?]", "job_experience[description]"

      assert_select "input#job_experience_done[name=?]", "job_experience[done]"

      assert_select "input#job_experience_detailbook_id[name=?]", "job_experience[detailbook_id]"
    end
  end
end
