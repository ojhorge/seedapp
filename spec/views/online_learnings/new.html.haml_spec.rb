require 'rails_helper'

RSpec.describe "online_learnings/new", type: :view do
  before(:each) do
    assign(:online_learning, OnlineLearning.new())
  end

  it "renders new online_learning form" do
    render

    assert_select "form[action=?][method=?]", online_learnings_path, "post" do
    end
  end
end
