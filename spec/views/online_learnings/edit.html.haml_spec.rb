require 'rails_helper'

RSpec.describe "online_learnings/edit", type: :view do
  before(:each) do
    @online_learning = assign(:online_learning, OnlineLearning.create!())
  end

  it "renders the edit online_learning form" do
    render

    assert_select "form[action=?][method=?]", online_learning_path(@online_learning), "post" do
    end
  end
end
