require 'rails_helper'

RSpec.describe "todo_tasks/show", type: :view do
  before(:each) do
    @todo_task = assign(:todo_task, TodoTask.create!(
      :title => "Title",
      :completed => false,
      :row_order => 1,
      :priority => 2,
      :comment => "MyText",
      :archived => false
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/Title/)
    expect(rendered).to match(/false/)
    expect(rendered).to match(/1/)
    expect(rendered).to match(/2/)
    expect(rendered).to match(/MyText/)
    expect(rendered).to match(/false/)
  end
end
