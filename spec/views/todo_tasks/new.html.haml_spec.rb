require 'rails_helper'

RSpec.describe "todo_tasks/new", type: :view do
  before(:each) do
    assign(:todo_task, TodoTask.new(
      :title => "MyString",
      :completed => false,
      :row_order => 1,
      :priority => 1,
      :comment => "MyText",
      :archived => false
    ))
  end

  it "renders new todo_task form" do
    render

    assert_select "form[action=?][method=?]", todo_tasks_path, "post" do

      assert_select "input#todo_task_title[name=?]", "todo_task[title]"

      assert_select "input#todo_task_completed[name=?]", "todo_task[completed]"

      assert_select "input#todo_task_row_order[name=?]", "todo_task[row_order]"

      assert_select "input#todo_task_priority[name=?]", "todo_task[priority]"

      assert_select "textarea#todo_task_comment[name=?]", "todo_task[comment]"

      assert_select "input#todo_task_archived[name=?]", "todo_task[archived]"
    end
  end
end
