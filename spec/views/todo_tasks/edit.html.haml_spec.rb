require 'rails_helper'

RSpec.describe "todo_tasks/edit", type: :view do
  before(:each) do
    @todo_task = assign(:todo_task, TodoTask.create!(
      :title => "MyString",
      :completed => false,
      :row_order => 1,
      :priority => 1,
      :comment => "MyText",
      :archived => false
    ))
  end

  it "renders the edit todo_task form" do
    render

    assert_select "form[action=?][method=?]", todo_task_path(@todo_task), "post" do

      assert_select "input#todo_task_title[name=?]", "todo_task[title]"

      assert_select "input#todo_task_completed[name=?]", "todo_task[completed]"

      assert_select "input#todo_task_row_order[name=?]", "todo_task[row_order]"

      assert_select "input#todo_task_priority[name=?]", "todo_task[priority]"

      assert_select "textarea#todo_task_comment[name=?]", "todo_task[comment]"

      assert_select "input#todo_task_archived[name=?]", "todo_task[archived]"
    end
  end
end
