require 'rails_helper'

RSpec.describe "todo_tasks/index", type: :view do
  before(:each) do
    assign(:todo_tasks, [
      TodoTask.create!(
        :title => "Title",
        :completed => false,
        :row_order => 1,
        :priority => 2,
        :comment => "MyText",
        :archived => false
      ),
      TodoTask.create!(
        :title => "Title",
        :completed => false,
        :row_order => 1,
        :priority => 2,
        :comment => "MyText",
        :archived => false
      )
    ])
  end

  it "renders a list of todo_tasks" do
    render
    assert_select "tr>td", :text => "Title".to_s, :count => 2
    assert_select "tr>td", :text => false.to_s, :count => 2
    assert_select "tr>td", :text => 1.to_s, :count => 2
    assert_select "tr>td", :text => 2.to_s, :count => 2
    assert_select "tr>td", :text => "MyText".to_s, :count => 2
    assert_select "tr>td", :text => false.to_s, :count => 2
  end
end
