require 'rails_helper'

RSpec.describe "media/edit", type: :view do
  before(:each) do
    @medium = assign(:medium, Medium.create!(
      :user => nil,
      :description => "MyText",
      :guid => "MyString",
      :post => nil
    ))
  end

  it "renders the edit medium form" do
    render

    assert_select "form[action=?][method=?]", medium_path(@medium), "post" do

      assert_select "input#medium_user_id[name=?]", "medium[user_id]"

      assert_select "textarea#medium_description[name=?]", "medium[description]"

      assert_select "input#medium_guid[name=?]", "medium[guid]"

      assert_select "input#medium_post_id[name=?]", "medium[post_id]"
    end
  end
end
