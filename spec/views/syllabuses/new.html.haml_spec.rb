require 'rails_helper'

RSpec.describe "syllabuses/new", type: :view do
  before(:each) do
    assign(:syllabus, Syllabus.new(
      :title => "MyString",
      :description => "MyText"
    ))
  end

  it "renders new syllabus form" do
    render

    assert_select "form[action=?][method=?]", syllabuses_path, "post" do

      assert_select "input#syllabus_title[name=?]", "syllabus[title]"

      assert_select "textarea#syllabus_description[name=?]", "syllabus[description]"
    end
  end
end
