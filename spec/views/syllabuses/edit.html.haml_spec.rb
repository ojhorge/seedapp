require 'rails_helper'

RSpec.describe "syllabuses/edit", type: :view do
  before(:each) do
    @syllabus = assign(:syllabus, Syllabus.create!(
      :title => "MyString",
      :description => "MyText"
    ))
  end

  it "renders the edit syllabus form" do
    render

    assert_select "form[action=?][method=?]", syllabus_path(@syllabus), "post" do

      assert_select "input#syllabus_title[name=?]", "syllabus[title]"

      assert_select "textarea#syllabus_description[name=?]", "syllabus[description]"
    end
  end
end
