require 'rails_helper'

RSpec.describe "syllabuses/index", type: :view do
  before(:each) do
    assign(:syllabuses, [
      Syllabus.create!(
        :title => "Title",
        :description => "MyText"
      ),
      Syllabus.create!(
        :title => "Title",
        :description => "MyText"
      )
    ])
  end

  it "renders a list of syllabuses" do
    render
    assert_select "tr>td", :text => "Title".to_s, :count => 2
    assert_select "tr>td", :text => "MyText".to_s, :count => 2
  end
end
