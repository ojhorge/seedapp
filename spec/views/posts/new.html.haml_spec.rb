require 'rails_helper'

RSpec.describe "posts/new", type: :view do
  before(:each) do
    assign(:post, Post.new(
      :user_id => 1,
      :content => "MyText",
      :content_html => "MyText"
    ))
  end

  it "renders new post form" do
    render

    assert_select "form[action=?][method=?]", posts_path, "post" do

      assert_select "input#post_user_id[name=?]", "post[user_id]"

      assert_select "textarea#post_content[name=?]", "post[content]"

      assert_select "textarea#post_content_html[name=?]", "post[content_html]"
    end
  end
end
