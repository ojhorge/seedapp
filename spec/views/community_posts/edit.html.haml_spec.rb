require 'rails_helper'

RSpec.describe "community_posts/edit", type: :view do
  before(:each) do
    @community_post = assign(:community_post, CommunityPost.create!(
      :name => "MyString",
      :content => "MyText"
    ))
  end

  it "renders the edit community_post form" do
    render

    assert_select "form[action=?][method=?]", community_post_path(@community_post), "post" do

      assert_select "input#community_post_name[name=?]", "community_post[name]"

      assert_select "textarea#community_post_content[name=?]", "community_post[content]"
    end
  end
end
