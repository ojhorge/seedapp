require 'rails_helper'

RSpec.describe "community_posts/index", type: :view do
  before(:each) do
    assign(:community_posts, [
      CommunityPost.create!(
        :name => "Name",
        :content => "MyText"
      ),
      CommunityPost.create!(
        :name => "Name",
        :content => "MyText"
      )
    ])
  end

  it "renders a list of community_posts" do
    render
    assert_select "tr>td", :text => "Name".to_s, :count => 2
    assert_select "tr>td", :text => "MyText".to_s, :count => 2
  end
end
