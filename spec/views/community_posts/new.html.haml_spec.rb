require 'rails_helper'

RSpec.describe "community_posts/new", type: :view do
  before(:each) do
    assign(:community_post, CommunityPost.new(
      :name => "MyString",
      :content => "MyText"
    ))
  end

  it "renders new community_post form" do
    render

    assert_select "form[action=?][method=?]", community_posts_path, "post" do

      assert_select "input#community_post_name[name=?]", "community_post[name]"

      assert_select "textarea#community_post_content[name=?]", "community_post[content]"
    end
  end
end
