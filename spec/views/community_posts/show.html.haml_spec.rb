require 'rails_helper'

RSpec.describe "community_posts/show", type: :view do
  before(:each) do
    @community_post = assign(:community_post, CommunityPost.create!(
      :name => "Name",
      :content => "MyText"
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/Name/)
    expect(rendered).to match(/MyText/)
  end
end
