require 'rails_helper'

RSpec.describe "detailbooks/show", type: :view do
  before(:each) do
    @detailbook = assign(:detailbook, Detailbook.create!(
      :user => nil,
      :name => "Name",
      :last_name => "Last Name",
      :gender => "Gender",
      :city => "City",
      :country => "Country",
      :benchmark => 1
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(//)
    expect(rendered).to match(/Name/)
    expect(rendered).to match(/Last Name/)
    expect(rendered).to match(/Gender/)
    expect(rendered).to match(/City/)
    expect(rendered).to match(/Country/)
    expect(rendered).to match(/1/)
  end
end
