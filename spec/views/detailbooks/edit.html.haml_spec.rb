require 'rails_helper'

RSpec.describe "detailbooks/edit", type: :view do
  before(:each) do
    @detailbook = assign(:detailbook, Detailbook.create!(
      :user => nil,
      :name => "MyString",
      :last_name => "MyString",
      :gender => "MyString",
      :city => "MyString",
      :country => "MyString",
      :benchmark => 1
    ))
  end

  it "renders the edit detailbook form" do
    render

    assert_select "form[action=?][method=?]", detailbook_path(@detailbook), "post" do

      assert_select "input#detailbook_user_id[name=?]", "detailbook[user_id]"

      assert_select "input#detailbook_name[name=?]", "detailbook[name]"

      assert_select "input#detailbook_last_name[name=?]", "detailbook[last_name]"

      assert_select "input#detailbook_gender[name=?]", "detailbook[gender]"

      assert_select "input#detailbook_city[name=?]", "detailbook[city]"

      assert_select "input#detailbook_country[name=?]", "detailbook[country]"

      assert_select "input#detailbook_benchmark[name=?]", "detailbook[benchmark]"
    end
  end
end
