require 'rails_helper'

RSpec.describe "detailbooks/index", type: :view do
  before(:each) do
    assign(:detailbooks, [
      Detailbook.create!(
        :user => nil,
        :name => "Name",
        :last_name => "Last Name",
        :gender => "Gender",
        :city => "City",
        :country => "Country",
        :benchmark => 1
      ),
      Detailbook.create!(
        :user => nil,
        :name => "Name",
        :last_name => "Last Name",
        :gender => "Gender",
        :city => "City",
        :country => "Country",
        :benchmark => 1
      )
    ])
  end

  it "renders a list of detailbooks" do
    render
    assert_select "tr>td", :text => nil.to_s, :count => 2
    assert_select "tr>td", :text => "Name".to_s, :count => 2
    assert_select "tr>td", :text => "Last Name".to_s, :count => 2
    assert_select "tr>td", :text => "Gender".to_s, :count => 2
    assert_select "tr>td", :text => "City".to_s, :count => 2
    assert_select "tr>td", :text => "Country".to_s, :count => 2
    assert_select "tr>td", :text => 1.to_s, :count => 2
  end
end
