require 'rails_helper'

RSpec.describe "yolos/index", type: :view do
  before(:each) do
    assign(:yolos, [
      Yolo.create!(
        :user => nil,
        :content => "MyText",
        :content_html => "MyText"
      ),
      Yolo.create!(
        :user => nil,
        :content => "MyText",
        :content_html => "MyText"
      )
    ])
  end

  it "renders a list of yolos" do
    render
    assert_select "tr>td", :text => nil.to_s, :count => 2
    assert_select "tr>td", :text => "MyText".to_s, :count => 2
    assert_select "tr>td", :text => "MyText".to_s, :count => 2
  end
end
