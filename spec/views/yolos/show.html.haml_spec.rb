require 'rails_helper'

RSpec.describe "yolos/show", type: :view do
  before(:each) do
    @yolo = assign(:yolo, Yolo.create!(
      :user => nil,
      :content => "MyText",
      :content_html => "MyText"
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(//)
    expect(rendered).to match(/MyText/)
    expect(rendered).to match(/MyText/)
  end
end
