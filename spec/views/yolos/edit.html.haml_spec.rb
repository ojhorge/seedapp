require 'rails_helper'

RSpec.describe "yolos/edit", type: :view do
  before(:each) do
    @yolo = assign(:yolo, Yolo.create!(
      :user => nil,
      :content => "MyText",
      :content_html => "MyText"
    ))
  end

  it "renders the edit yolo form" do
    render

    assert_select "form[action=?][method=?]", yolo_path(@yolo), "post" do

      assert_select "input#yolo_user_id[name=?]", "yolo[user_id]"

      assert_select "textarea#yolo_content[name=?]", "yolo[content]"

      assert_select "textarea#yolo_content_html[name=?]", "yolo[content_html]"
    end
  end
end
