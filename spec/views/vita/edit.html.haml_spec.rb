require 'rails_helper'

RSpec.describe "vita/edit", type: :view do
  before(:each) do
    @vitum = assign(:vitum, Vitum.create!(
      :user => nil,
      :title => "MyString",
      :guid => "MyString"
    ))
  end

  it "renders the edit vitum form" do
    render

    assert_select "form[action=?][method=?]", vitum_path(@vitum), "post" do

      assert_select "input#vitum_user_id[name=?]", "vitum[user_id]"

      assert_select "input#vitum_title[name=?]", "vitum[title]"

      assert_select "input#vitum_guid[name=?]", "vitum[guid]"
    end
  end
end
