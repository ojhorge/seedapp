require 'rails_helper'

RSpec.describe "vita/new", type: :view do
  before(:each) do
    assign(:vitum, Vitum.new(
      :user => nil,
      :title => "MyString",
      :guid => "MyString"
    ))
  end

  it "renders new vitum form" do
    render

    assert_select "form[action=?][method=?]", vita_path, "post" do

      assert_select "input#vitum_user_id[name=?]", "vitum[user_id]"

      assert_select "input#vitum_title[name=?]", "vitum[title]"

      assert_select "input#vitum_guid[name=?]", "vitum[guid]"
    end
  end
end
