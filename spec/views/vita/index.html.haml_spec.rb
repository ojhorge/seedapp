require 'rails_helper'

RSpec.describe "vita/index", type: :view do
  before(:each) do
    assign(:vita, [
      Vitum.create!(
        :user => nil,
        :title => "Title",
        :guid => "Guid"
      ),
      Vitum.create!(
        :user => nil,
        :title => "Title",
        :guid => "Guid"
      )
    ])
  end

  it "renders a list of vita" do
    render
    assert_select "tr>td", :text => nil.to_s, :count => 2
    assert_select "tr>td", :text => "Title".to_s, :count => 2
    assert_select "tr>td", :text => "Guid".to_s, :count => 2
  end
end
