require 'rails_helper'

RSpec.describe "vita/show", type: :view do
  before(:each) do
    @vitum = assign(:vitum, Vitum.create!(
      :user => nil,
      :title => "Title",
      :guid => "Guid"
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(//)
    expect(rendered).to match(/Title/)
    expect(rendered).to match(/Guid/)
  end
end
