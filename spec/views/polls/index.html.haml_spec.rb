require 'rails_helper'

RSpec.describe "polls/index", type: :view do
  before(:each) do
    assign(:polls, [
      Poll.create!(
        :topic => "MyText",
        :user => nil
      ),
      Poll.create!(
        :topic => "MyText",
        :user => nil
      )
    ])
  end

  it "renders a list of polls" do
    render
    assert_select "tr>td", :text => "MyText".to_s, :count => 2
    assert_select "tr>td", :text => nil.to_s, :count => 2
  end
end
