require 'rails_helper'

RSpec.describe "polls/edit", type: :view do
  before(:each) do
    @poll = assign(:poll, Poll.create!(
      :topic => "MyText",
      :user => nil
    ))
  end

  it "renders the edit poll form" do
    render

    assert_select "form[action=?][method=?]", poll_path(@poll), "post" do

      assert_select "textarea#poll_topic[name=?]", "poll[topic]"

      assert_select "input#poll_user_id[name=?]", "poll[user_id]"
    end
  end
end
