require 'rails_helper'

RSpec.describe "events/new", type: :view do
  before(:each) do
    assign(:event, Event.new(
      :title => "MyString",
      :description => "MyText",
      :location => "MyString",
      :city => "MyString",
      :country => "MyString"
    ))
  end

  it "renders new event form" do
    render

    assert_select "form[action=?][method=?]", events_path, "post" do

      assert_select "input#event_title[name=?]", "event[title]"

      assert_select "textarea#event_description[name=?]", "event[description]"

      assert_select "input#event_location[name=?]", "event[location]"

      assert_select "input#event_city[name=?]", "event[city]"

      assert_select "input#event_country[name=?]", "event[country]"
    end
  end
end
