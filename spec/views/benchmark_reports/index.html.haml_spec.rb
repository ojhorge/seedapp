require 'rails_helper'

RSpec.describe "benchmark_reports/index", type: :view do
  before(:each) do
    assign(:benchmark_reports, [
      BenchmarkReport.create!(
        :user => nil,
        :points => 1,
        :month => 2,
        :year => 3
      ),
      BenchmarkReport.create!(
        :user => nil,
        :points => 1,
        :month => 2,
        :year => 3
      )
    ])
  end

  it "renders a list of benchmark_reports" do
    render
    assert_select "tr>td", :text => nil.to_s, :count => 2
    assert_select "tr>td", :text => 1.to_s, :count => 2
    assert_select "tr>td", :text => 2.to_s, :count => 2
    assert_select "tr>td", :text => 3.to_s, :count => 2
  end
end
