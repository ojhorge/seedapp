require 'rails_helper'

RSpec.describe "benchmark_reports/show", type: :view do
  before(:each) do
    @benchmark_report = assign(:benchmark_report, BenchmarkReport.create!(
      :user => nil,
      :points => 1,
      :month => 2,
      :year => 3
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(//)
    expect(rendered).to match(/1/)
    expect(rendered).to match(/2/)
    expect(rendered).to match(/3/)
  end
end
