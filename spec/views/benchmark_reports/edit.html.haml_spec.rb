require 'rails_helper'

RSpec.describe "benchmark_reports/edit", type: :view do
  before(:each) do
    @benchmark_report = assign(:benchmark_report, BenchmarkReport.create!(
      :user => nil,
      :points => 1,
      :month => 1,
      :year => 1
    ))
  end

  it "renders the edit benchmark_report form" do
    render

    assert_select "form[action=?][method=?]", benchmark_report_path(@benchmark_report), "post" do

      assert_select "input#benchmark_report_user_id[name=?]", "benchmark_report[user_id]"

      assert_select "input#benchmark_report_points[name=?]", "benchmark_report[points]"

      assert_select "input#benchmark_report_month[name=?]", "benchmark_report[month]"

      assert_select "input#benchmark_report_year[name=?]", "benchmark_report[year]"
    end
  end
end
