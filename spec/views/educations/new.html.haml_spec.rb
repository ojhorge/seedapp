require 'rails_helper'

RSpec.describe "educations/new", type: :view do
  before(:each) do
    assign(:education, Education.new(
      :guid => "MyString",
      :school_institution => "MyString",
      :city => "MyString",
      :country => "MyString",
      :degree => "MyString",
      :field_study => "MyString",
      :grade => "MyString",
      :activities_societies => "MyString",
      :description => "MyText",
      :done => false,
      :detailbook => nil
    ))
  end

  it "renders new education form" do
    render

    assert_select "form[action=?][method=?]", educations_path, "post" do

      assert_select "input#education_guid[name=?]", "education[guid]"

      assert_select "input#education_school_institution[name=?]", "education[school_institution]"

      assert_select "input#education_city[name=?]", "education[city]"

      assert_select "input#education_country[name=?]", "education[country]"

      assert_select "input#education_degree[name=?]", "education[degree]"

      assert_select "input#education_field_study[name=?]", "education[field_study]"

      assert_select "input#education_grade[name=?]", "education[grade]"

      assert_select "input#education_activities_societies[name=?]", "education[activities_societies]"

      assert_select "textarea#education_description[name=?]", "education[description]"

      assert_select "input#education_done[name=?]", "education[done]"

      assert_select "input#education_detailbook_id[name=?]", "education[detailbook_id]"
    end
  end
end
