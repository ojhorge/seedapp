require 'rails_helper'

RSpec.describe "educations/show", type: :view do
  before(:each) do
    @education = assign(:education, Education.create!(
      :guid => "Guid",
      :school_institution => "School Institution",
      :city => "City",
      :country => "Country",
      :degree => "Degree",
      :field_study => "Field Study",
      :grade => "Grade",
      :activities_societies => "Activities Societies",
      :description => "MyText",
      :done => false,
      :detailbook => nil
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/Guid/)
    expect(rendered).to match(/School Institution/)
    expect(rendered).to match(/City/)
    expect(rendered).to match(/Country/)
    expect(rendered).to match(/Degree/)
    expect(rendered).to match(/Field Study/)
    expect(rendered).to match(/Grade/)
    expect(rendered).to match(/Activities Societies/)
    expect(rendered).to match(/MyText/)
    expect(rendered).to match(/false/)
    expect(rendered).to match(//)
  end
end
