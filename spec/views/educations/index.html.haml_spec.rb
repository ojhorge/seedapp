require 'rails_helper'

RSpec.describe "educations/index", type: :view do
  before(:each) do
    assign(:educations, [
      Education.create!(
        :guid => "Guid",
        :school_institution => "School Institution",
        :city => "City",
        :country => "Country",
        :degree => "Degree",
        :field_study => "Field Study",
        :grade => "Grade",
        :activities_societies => "Activities Societies",
        :description => "MyText",
        :done => false,
        :detailbook => nil
      ),
      Education.create!(
        :guid => "Guid",
        :school_institution => "School Institution",
        :city => "City",
        :country => "Country",
        :degree => "Degree",
        :field_study => "Field Study",
        :grade => "Grade",
        :activities_societies => "Activities Societies",
        :description => "MyText",
        :done => false,
        :detailbook => nil
      )
    ])
  end

  it "renders a list of educations" do
    render
    assert_select "tr>td", :text => "Guid".to_s, :count => 2
    assert_select "tr>td", :text => "School Institution".to_s, :count => 2
    assert_select "tr>td", :text => "City".to_s, :count => 2
    assert_select "tr>td", :text => "Country".to_s, :count => 2
    assert_select "tr>td", :text => "Degree".to_s, :count => 2
    assert_select "tr>td", :text => "Field Study".to_s, :count => 2
    assert_select "tr>td", :text => "Grade".to_s, :count => 2
    assert_select "tr>td", :text => "Activities Societies".to_s, :count => 2
    assert_select "tr>td", :text => "MyText".to_s, :count => 2
    assert_select "tr>td", :text => false.to_s, :count => 2
    assert_select "tr>td", :text => nil.to_s, :count => 2
  end
end
