require 'rails_helper'

RSpec.describe "folders/index", type: :view do
  before(:each) do
    assign(:folders, [
      Folder.create!(
        :guid => "Guid",
        :user => nil,
        :name => "Name",
        :description => "MyText",
        :sharable_to => "MyText",
        :category => "Category"
      ),
      Folder.create!(
        :guid => "Guid",
        :user => nil,
        :name => "Name",
        :description => "MyText",
        :sharable_to => "MyText",
        :category => "Category"
      )
    ])
  end

  it "renders a list of folders" do
    render
    assert_select "tr>td", :text => "Guid".to_s, :count => 2
    assert_select "tr>td", :text => nil.to_s, :count => 2
    assert_select "tr>td", :text => "Name".to_s, :count => 2
    assert_select "tr>td", :text => "MyText".to_s, :count => 2
    assert_select "tr>td", :text => "MyText".to_s, :count => 2
    assert_select "tr>td", :text => "Category".to_s, :count => 2
  end
end
