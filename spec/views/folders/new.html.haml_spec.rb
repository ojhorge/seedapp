require 'rails_helper'

RSpec.describe "folders/new", type: :view do
  before(:each) do
    assign(:folder, Folder.new(
      :guid => "MyString",
      :user => nil,
      :name => "MyString",
      :description => "MyText",
      :sharable_to => "MyText",
      :category => "MyString"
    ))
  end

  it "renders new folder form" do
    render

    assert_select "form[action=?][method=?]", folders_path, "post" do

      assert_select "input#folder_guid[name=?]", "folder[guid]"

      assert_select "input#folder_user_id[name=?]", "folder[user_id]"

      assert_select "input#folder_name[name=?]", "folder[name]"

      assert_select "textarea#folder_description[name=?]", "folder[description]"

      assert_select "textarea#folder_sharable_to[name=?]", "folder[sharable_to]"

      assert_select "input#folder_category[name=?]", "folder[category]"
    end
  end
end
