require 'rails_helper'

RSpec.describe "skills/new", type: :view do
  before(:each) do
    assign(:skill, Skill.new(
      :name => "MyString",
      :done => false,
      :detailbook => nil
    ))
  end

  it "renders new skill form" do
    render

    assert_select "form[action=?][method=?]", skills_path, "post" do

      assert_select "input#skill_name[name=?]", "skill[name]"

      assert_select "input#skill_done[name=?]", "skill[done]"

      assert_select "input#skill_detailbook_id[name=?]", "skill[detailbook_id]"
    end
  end
end
