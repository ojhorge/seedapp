require 'rails_helper'

RSpec.describe "job_applications/show", type: :view do
  before(:each) do
    @job_application = assign(:job_application, JobApplication.create!(
      :guid => "Guid",
      :company_name => "Company Name",
      :industry => "Industry",
      :job_title => "Job Title",
      :experience => "Experience",
      :job_function => "Job Function",
      :employment_type => "Employment Type",
      :job_description => "MyText",
      :city => "City",
      :country => "Country",
      :public => false,
      :user => nil
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/Guid/)
    expect(rendered).to match(/Company Name/)
    expect(rendered).to match(/Industry/)
    expect(rendered).to match(/Job Title/)
    expect(rendered).to match(/Experience/)
    expect(rendered).to match(/Job Function/)
    expect(rendered).to match(/Employment Type/)
    expect(rendered).to match(/MyText/)
    expect(rendered).to match(/City/)
    expect(rendered).to match(/Country/)
    expect(rendered).to match(/false/)
    expect(rendered).to match(//)
  end
end
