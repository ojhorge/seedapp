require 'rails_helper'

RSpec.describe "job_applications/new", type: :view do
  before(:each) do
    assign(:job_application, JobApplication.new(
      :guid => "MyString",
      :company_name => "MyString",
      :industry => "MyString",
      :job_title => "MyString",
      :experience => "MyString",
      :job_function => "MyString",
      :employment_type => "MyString",
      :job_description => "MyText",
      :city => "MyString",
      :country => "MyString",
      :public => false,
      :user => nil
    ))
  end

  it "renders new job_application form" do
    render

    assert_select "form[action=?][method=?]", job_applications_path, "post" do

      assert_select "input#job_application_guid[name=?]", "job_application[guid]"

      assert_select "input#job_application_company_name[name=?]", "job_application[company_name]"

      assert_select "input#job_application_industry[name=?]", "job_application[industry]"

      assert_select "input#job_application_job_title[name=?]", "job_application[job_title]"

      assert_select "input#job_application_experience[name=?]", "job_application[experience]"

      assert_select "input#job_application_job_function[name=?]", "job_application[job_function]"

      assert_select "input#job_application_employment_type[name=?]", "job_application[employment_type]"

      assert_select "textarea#job_application_job_description[name=?]", "job_application[job_description]"

      assert_select "input#job_application_city[name=?]", "job_application[city]"

      assert_select "input#job_application_country[name=?]", "job_application[country]"

      assert_select "input#job_application_public[name=?]", "job_application[public]"

      assert_select "input#job_application_user_id[name=?]", "job_application[user_id]"
    end
  end
end
