require 'rails_helper'

RSpec.describe "job_applications/index", type: :view do
  before(:each) do
    assign(:job_applications, [
      JobApplication.create!(
        :guid => "Guid",
        :company_name => "Company Name",
        :industry => "Industry",
        :job_title => "Job Title",
        :experience => "Experience",
        :job_function => "Job Function",
        :employment_type => "Employment Type",
        :job_description => "MyText",
        :city => "City",
        :country => "Country",
        :public => false,
        :user => nil
      ),
      JobApplication.create!(
        :guid => "Guid",
        :company_name => "Company Name",
        :industry => "Industry",
        :job_title => "Job Title",
        :experience => "Experience",
        :job_function => "Job Function",
        :employment_type => "Employment Type",
        :job_description => "MyText",
        :city => "City",
        :country => "Country",
        :public => false,
        :user => nil
      )
    ])
  end

  it "renders a list of job_applications" do
    render
    assert_select "tr>td", :text => "Guid".to_s, :count => 2
    assert_select "tr>td", :text => "Company Name".to_s, :count => 2
    assert_select "tr>td", :text => "Industry".to_s, :count => 2
    assert_select "tr>td", :text => "Job Title".to_s, :count => 2
    assert_select "tr>td", :text => "Experience".to_s, :count => 2
    assert_select "tr>td", :text => "Job Function".to_s, :count => 2
    assert_select "tr>td", :text => "Employment Type".to_s, :count => 2
    assert_select "tr>td", :text => "MyText".to_s, :count => 2
    assert_select "tr>td", :text => "City".to_s, :count => 2
    assert_select "tr>td", :text => "Country".to_s, :count => 2
    assert_select "tr>td", :text => false.to_s, :count => 2
    assert_select "tr>td", :text => nil.to_s, :count => 2
  end
end
