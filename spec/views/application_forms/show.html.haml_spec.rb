require 'rails_helper'

RSpec.describe "application_forms/show", type: :view do
  before(:each) do
    @application_form = assign(:application_form, ApplicationForm.create!(
      :guid => "Guid",
      :job_application => nil,
      :user => nil
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/Guid/)
    expect(rendered).to match(//)
    expect(rendered).to match(//)
  end
end
