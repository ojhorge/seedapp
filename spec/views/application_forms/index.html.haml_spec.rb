require 'rails_helper'

RSpec.describe "application_forms/index", type: :view do
  before(:each) do
    assign(:application_forms, [
      ApplicationForm.create!(
        :guid => "Guid",
        :job_application => nil,
        :user => nil
      ),
      ApplicationForm.create!(
        :guid => "Guid",
        :job_application => nil,
        :user => nil
      )
    ])
  end

  it "renders a list of application_forms" do
    render
    assert_select "tr>td", :text => "Guid".to_s, :count => 2
    assert_select "tr>td", :text => nil.to_s, :count => 2
    assert_select "tr>td", :text => nil.to_s, :count => 2
  end
end
