require 'rails_helper'

RSpec.describe "application_forms/new", type: :view do
  before(:each) do
    assign(:application_form, ApplicationForm.new(
      :guid => "MyString",
      :job_application => nil,
      :user => nil
    ))
  end

  it "renders new application_form form" do
    render

    assert_select "form[action=?][method=?]", application_forms_path, "post" do

      assert_select "input#application_form_guid[name=?]", "application_form[guid]"

      assert_select "input#application_form_job_application_id[name=?]", "application_form[job_application_id]"

      assert_select "input#application_form_user_id[name=?]", "application_form[user_id]"
    end
  end
end
