require 'rails_helper'

RSpec.describe "application_forms/edit", type: :view do
  before(:each) do
    @application_form = assign(:application_form, ApplicationForm.create!(
      :guid => "MyString",
      :job_application => nil,
      :user => nil
    ))
  end

  it "renders the edit application_form form" do
    render

    assert_select "form[action=?][method=?]", application_form_path(@application_form), "post" do

      assert_select "input#application_form_guid[name=?]", "application_form[guid]"

      assert_select "input#application_form_job_application_id[name=?]", "application_form[job_application_id]"

      assert_select "input#application_form_user_id[name=?]", "application_form[user_id]"
    end
  end
end
