require 'rails_helper'

RSpec.describe "classifieds/index", type: :view do
  before(:each) do
    assign(:classifieds, [
      Classified.create!(
        :title => "",
        :user => nil,
        :description => "MyText",
        :price => 1.5,
        :availability => false,
        :notification_status => "MyText",
        :status => ""
      ),
      Classified.create!(
        :title => "",
        :user => nil,
        :description => "MyText",
        :price => 1.5,
        :availability => false,
        :notification_status => "MyText",
        :status => ""
      )
    ])
  end

  it "renders a list of classifieds" do
    render
    assert_select "tr>td", :text => "".to_s, :count => 2
    assert_select "tr>td", :text => nil.to_s, :count => 2
    assert_select "tr>td", :text => "MyText".to_s, :count => 2
    assert_select "tr>td", :text => 1.5.to_s, :count => 2
    assert_select "tr>td", :text => false.to_s, :count => 2
    assert_select "tr>td", :text => "MyText".to_s, :count => 2
    assert_select "tr>td", :text => "".to_s, :count => 2
  end
end
