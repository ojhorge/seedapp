require 'rails_helper'

RSpec.describe "classifieds/show", type: :view do
  before(:each) do
    @classified = assign(:classified, Classified.create!(
      :title => "",
      :user => nil,
      :description => "MyText",
      :price => 1.5,
      :availability => false,
      :notification_status => "MyText",
      :status => ""
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(//)
    expect(rendered).to match(//)
    expect(rendered).to match(/MyText/)
    expect(rendered).to match(/1.5/)
    expect(rendered).to match(/false/)
    expect(rendered).to match(/MyText/)
    expect(rendered).to match(//)
  end
end
