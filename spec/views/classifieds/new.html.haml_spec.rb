require 'rails_helper'

RSpec.describe "classifieds/new", type: :view do
  before(:each) do
    assign(:classified, Classified.new(
      :title => "",
      :user => nil,
      :description => "MyText",
      :price => 1.5,
      :availability => false,
      :notification_status => "MyText",
      :status => ""
    ))
  end

  it "renders new classified form" do
    render

    assert_select "form[action=?][method=?]", classifieds_path, "post" do

      assert_select "input#classified_title[name=?]", "classified[title]"

      assert_select "input#classified_user_id[name=?]", "classified[user_id]"

      assert_select "textarea#classified_description[name=?]", "classified[description]"

      assert_select "input#classified_price[name=?]", "classified[price]"

      assert_select "input#classified_availability[name=?]", "classified[availability]"

      assert_select "textarea#classified_notification_status[name=?]", "classified[notification_status]"

      assert_select "input#classified_status[name=?]", "classified[status]"
    end
  end
end
