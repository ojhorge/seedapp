require 'rails_helper'

RSpec.describe "classifieds/edit", type: :view do
  before(:each) do
    @classified = assign(:classified, Classified.create!(
      :title => "",
      :user => nil,
      :description => "MyText",
      :price => 1.5,
      :availability => false,
      :notification_status => "MyText",
      :status => ""
    ))
  end

  it "renders the edit classified form" do
    render

    assert_select "form[action=?][method=?]", classified_path(@classified), "post" do

      assert_select "input#classified_title[name=?]", "classified[title]"

      assert_select "input#classified_user_id[name=?]", "classified[user_id]"

      assert_select "textarea#classified_description[name=?]", "classified[description]"

      assert_select "input#classified_price[name=?]", "classified[price]"

      assert_select "input#classified_availability[name=?]", "classified[availability]"

      assert_select "textarea#classified_notification_status[name=?]", "classified[notification_status]"

      assert_select "input#classified_status[name=?]", "classified[status]"
    end
  end
end
