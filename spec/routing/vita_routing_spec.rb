require "rails_helper"

RSpec.describe VitaController, type: :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/vita").to route_to("vita#index")
    end

    it "routes to #new" do
      expect(:get => "/vita/new").to route_to("vita#new")
    end

    it "routes to #show" do
      expect(:get => "/vita/1").to route_to("vita#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/vita/1/edit").to route_to("vita#edit", :id => "1")
    end

    it "routes to #create" do
      expect(:post => "/vita").to route_to("vita#create")
    end

    it "routes to #update via PUT" do
      expect(:put => "/vita/1").to route_to("vita#update", :id => "1")
    end

    it "routes to #update via PATCH" do
      expect(:patch => "/vita/1").to route_to("vita#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/vita/1").to route_to("vita#destroy", :id => "1")
    end

  end
end
