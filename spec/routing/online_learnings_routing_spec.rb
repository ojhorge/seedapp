require "rails_helper"

RSpec.describe OnlineLearningsController, type: :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/online_learnings").to route_to("online_learnings#index")
    end

    it "routes to #new" do
      expect(:get => "/online_learnings/new").to route_to("online_learnings#new")
    end

    it "routes to #show" do
      expect(:get => "/online_learnings/1").to route_to("online_learnings#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/online_learnings/1/edit").to route_to("online_learnings#edit", :id => "1")
    end

    it "routes to #create" do
      expect(:post => "/online_learnings").to route_to("online_learnings#create")
    end

    it "routes to #update via PUT" do
      expect(:put => "/online_learnings/1").to route_to("online_learnings#update", :id => "1")
    end

    it "routes to #update via PATCH" do
      expect(:patch => "/online_learnings/1").to route_to("online_learnings#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/online_learnings/1").to route_to("online_learnings#destroy", :id => "1")
    end

  end
end
