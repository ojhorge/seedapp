require "rails_helper"

RSpec.describe DetailbooksController, type: :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/detailbooks").to route_to("detailbooks#index")
    end

    it "routes to #new" do
      expect(:get => "/detailbooks/new").to route_to("detailbooks#new")
    end

    it "routes to #show" do
      expect(:get => "/detailbooks/1").to route_to("detailbooks#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/detailbooks/1/edit").to route_to("detailbooks#edit", :id => "1")
    end

    it "routes to #create" do
      expect(:post => "/detailbooks").to route_to("detailbooks#create")
    end

    it "routes to #update via PUT" do
      expect(:put => "/detailbooks/1").to route_to("detailbooks#update", :id => "1")
    end

    it "routes to #update via PATCH" do
      expect(:patch => "/detailbooks/1").to route_to("detailbooks#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/detailbooks/1").to route_to("detailbooks#destroy", :id => "1")
    end

  end
end
