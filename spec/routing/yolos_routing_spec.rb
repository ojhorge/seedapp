require "rails_helper"

RSpec.describe YolosController, type: :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/yolos").to route_to("yolos#index")
    end

    it "routes to #new" do
      expect(:get => "/yolos/new").to route_to("yolos#new")
    end

    it "routes to #show" do
      expect(:get => "/yolos/1").to route_to("yolos#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/yolos/1/edit").to route_to("yolos#edit", :id => "1")
    end

    it "routes to #create" do
      expect(:post => "/yolos").to route_to("yolos#create")
    end

    it "routes to #update via PUT" do
      expect(:put => "/yolos/1").to route_to("yolos#update", :id => "1")
    end

    it "routes to #update via PATCH" do
      expect(:patch => "/yolos/1").to route_to("yolos#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/yolos/1").to route_to("yolos#destroy", :id => "1")
    end

  end
end
