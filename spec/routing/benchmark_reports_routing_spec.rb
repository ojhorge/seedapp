require "rails_helper"

RSpec.describe BenchmarkReportsController, type: :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/benchmark_reports").to route_to("benchmark_reports#index")
    end

    it "routes to #new" do
      expect(:get => "/benchmark_reports/new").to route_to("benchmark_reports#new")
    end

    it "routes to #show" do
      expect(:get => "/benchmark_reports/1").to route_to("benchmark_reports#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/benchmark_reports/1/edit").to route_to("benchmark_reports#edit", :id => "1")
    end

    it "routes to #create" do
      expect(:post => "/benchmark_reports").to route_to("benchmark_reports#create")
    end

    it "routes to #update via PUT" do
      expect(:put => "/benchmark_reports/1").to route_to("benchmark_reports#update", :id => "1")
    end

    it "routes to #update via PATCH" do
      expect(:patch => "/benchmark_reports/1").to route_to("benchmark_reports#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/benchmark_reports/1").to route_to("benchmark_reports#destroy", :id => "1")
    end

  end
end
