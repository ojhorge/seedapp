require "rails_helper"

RSpec.describe JobExperiencesController, type: :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/job_experiences").to route_to("job_experiences#index")
    end

    it "routes to #new" do
      expect(:get => "/job_experiences/new").to route_to("job_experiences#new")
    end

    it "routes to #show" do
      expect(:get => "/job_experiences/1").to route_to("job_experiences#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/job_experiences/1/edit").to route_to("job_experiences#edit", :id => "1")
    end

    it "routes to #create" do
      expect(:post => "/job_experiences").to route_to("job_experiences#create")
    end

    it "routes to #update via PUT" do
      expect(:put => "/job_experiences/1").to route_to("job_experiences#update", :id => "1")
    end

    it "routes to #update via PATCH" do
      expect(:patch => "/job_experiences/1").to route_to("job_experiences#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/job_experiences/1").to route_to("job_experiences#destroy", :id => "1")
    end

  end
end
