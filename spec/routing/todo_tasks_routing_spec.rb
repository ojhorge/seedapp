require "rails_helper"

RSpec.describe TodoTasksController, type: :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/todo_tasks").to route_to("todo_tasks#index")
    end

    it "routes to #new" do
      expect(:get => "/todo_tasks/new").to route_to("todo_tasks#new")
    end

    it "routes to #show" do
      expect(:get => "/todo_tasks/1").to route_to("todo_tasks#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/todo_tasks/1/edit").to route_to("todo_tasks#edit", :id => "1")
    end

    it "routes to #create" do
      expect(:post => "/todo_tasks").to route_to("todo_tasks#create")
    end

    it "routes to #update via PUT" do
      expect(:put => "/todo_tasks/1").to route_to("todo_tasks#update", :id => "1")
    end

    it "routes to #update via PATCH" do
      expect(:patch => "/todo_tasks/1").to route_to("todo_tasks#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/todo_tasks/1").to route_to("todo_tasks#destroy", :id => "1")
    end

  end
end
