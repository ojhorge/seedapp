require "rails_helper"

RSpec.describe CommunityPostsController, type: :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/community_posts").to route_to("community_posts#index")
    end

    it "routes to #new" do
      expect(:get => "/community_posts/new").to route_to("community_posts#new")
    end

    it "routes to #show" do
      expect(:get => "/community_posts/1").to route_to("community_posts#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/community_posts/1/edit").to route_to("community_posts#edit", :id => "1")
    end

    it "routes to #create" do
      expect(:post => "/community_posts").to route_to("community_posts#create")
    end

    it "routes to #update via PUT" do
      expect(:put => "/community_posts/1").to route_to("community_posts#update", :id => "1")
    end

    it "routes to #update via PATCH" do
      expect(:patch => "/community_posts/1").to route_to("community_posts#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/community_posts/1").to route_to("community_posts#destroy", :id => "1")
    end

  end
end
